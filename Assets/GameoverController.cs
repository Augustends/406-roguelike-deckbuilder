using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameoverController : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        FadesManager.Instance.FadeIn();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void HandleMainMenuClick()
    {
        FadesManager.Instance.FadeOutThen(delegate { GoMainMenu(); });
    }

    private void GoMainMenu()
    {
        GameController.Instance.RestartGame();
    }
}
