using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherManager : MonoBehaviour
{
    [SerializeField]
    GameObject defaultWeather = null;
    [SerializeField]
    GameObject fireWeather = null;
    [SerializeField]
    GameObject windWeather = null;
    [SerializeField]
    GameObject waterWeather = null;
    [SerializeField]
    GameObject earthWeather = null;

    [SerializeField]
    int excessWeatherThreshold = 10;

    GameObject currentWeather = null;
    Energy_Type currentWeatherType = Energy_Type.NONE;

    [SerializeField]
    EnergyManager energyManager = null;

    int lastTurnEffectApplied = 0;

    // Start is called before the first frame update
    void Start()
    {
        currentWeather = GameObject.Instantiate(defaultWeather);
        currentWeather.transform.SetParent(this.transform);
        currentWeatherType = Energy_Type.NONE;

        // Get the EnergyManager in the scene.
        var energy_managers = FindObjectsOfType<EnergyManager>();
        if (energy_managers.Length < 1)
        {
            Debug.LogError("No energy manager exists in the scene!\n");
            return;
        }
        energyManager = energy_managers[0];

        // We want to call WeatherCheck energy second.
        InvokeRepeating("WeatherCheck", 0f, 1f);
    }

    public void OnTurnStart(CombatCharacters currentCharacter)
    {
        

        switch (currentWeatherType)
        {
            case Energy_Type.WATER:
                if (currentCharacter.character_type != Character_Type.PLAYER) return;
                GainAP gain = new GainAP();
                gain.setTargets(new CombatCharacters[] {currentCharacter});
                gain.setAmount(1);
                gain.activate();
                break;
            case Energy_Type.WIND:
                if (currentCharacter.character_type != Character_Type.PLAYER) return;
                DrawCards draw = new DrawCards();
                draw.setTargets(new CombatCharacters[] {currentCharacter});
                draw.setAmount(1);
                draw.activate();
                break;
            case Energy_Type.FIRE:
                if (currentCharacter.character_type == Character_Type.PLAYER) return;
                Damage dmg = new Damage();
                dmg.setTargets(new CombatCharacters[] {currentCharacter});
                // Random.Range(int,int) is max exclusive, so (1,5) returns 1-4, aka 1d4
                dmg.setAmount(UnityEngine.Random.Range(1,5));
                dmg.activate();
                break;
            case Energy_Type.EARTH:
                if (currentCharacter.character_type != Character_Type.PLAYER) return;
                ApplyArmour arm = new ApplyArmour();
                arm.setTargets(new CombatCharacters[] {currentCharacter});
                arm.setAmount(UnityEngine.Random.Range(1,4));
                arm.activate();
                break;
            default: break;
        }
    }

    void OnDestroy()
    {
        CancelInvoke();
    }

    void WeatherCheck()
    {
        if (energyManager == null) return;

        // Figure out which type has the greatest energy amount, and how by how much.
        Energy_Type greatest_type = Energy_Type.NONE;
        int greatest_amount = 0;
        for (Energy_Type e = Energy_Type.WATER; e <= Energy_Type.EARTH; ++e)
        {
            int amount = energyManager.getEnergyAmount(e);
            if (amount > greatest_amount)
            {
                greatest_amount = amount;
                greatest_type = e;
            }
        }

        if (greatest_type == currentWeatherType) return;
        if (greatest_amount < excessWeatherThreshold)
        {
            if (currentWeatherType == Energy_Type.NONE) return;

            Destroy(currentWeather);
            currentWeather = GameObject.Instantiate(defaultWeather);
            currentWeather.transform.SetParent(this.transform);
            currentWeatherType = Energy_Type.NONE;
            return;
        }

        Destroy(currentWeather);
        switch (greatest_type)
        {
            case Energy_Type.WATER: if (waterWeather != null) { currentWeather = GameObject.Instantiate(waterWeather); } break;
            case Energy_Type.WIND:  if (windWeather != null)  { currentWeather = GameObject.Instantiate(windWeather); }  break;
            case Energy_Type.FIRE:  if (fireWeather != null)  { currentWeather = GameObject.Instantiate(fireWeather); }  break;
            case Energy_Type.EARTH: if (earthWeather != null) { currentWeather = GameObject.Instantiate(earthWeather); } break;
            default: break;
        }
        currentWeather.transform.SetParent(this.transform);
        currentWeatherType = greatest_type;
    }
}
