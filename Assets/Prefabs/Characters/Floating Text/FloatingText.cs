using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingText : MonoBehaviour
    //@author Tanner Skomar
{
    public float destroy_time = 2f;//How long the text should last before being destroyed
    public Vector3 text_offset = new Vector3(0,0,0); //amount to offset the text. //Right now if is behind the sprites it will not be shown
    public Vector3 randomize_intensity = new Vector3(1, 0.5f, 0);//Intensity of the random spawn  differences 

    // Start is called before the first frame update
    void Start()
    {
        var mesh = GetComponent<MeshRenderer>();
        mesh.sortingOrder = 100; // Put it in front of other things
        transform.parent.localPosition += text_offset; //add offset to parent so that animation is relative
        transform.parent.localPosition += new Vector3(Random.Range(-randomize_intensity.x, randomize_intensity.x), Random.Range(-randomize_intensity.y, randomize_intensity.y));

        Destroy(gameObject, destroy_time);//Destroy the text after time
    }
}
