using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnToken : MonoBehaviour
{
    public int order_in_round;
    float size = 100;
    RectTransform rectTransform;
    public CombatCharacters owner;

    public float TargetXPos { get { return order_in_round ==0 ? order_in_round * size -20 : order_in_round* size; } }
    //The first element is seperated a bit
    public int OrderInRound { get => order_in_round; set => order_in_round = value; }

    private void Awake()
    {
        //Debug.Log("HI");
    }

    public void Init(CombatCharacters owner)
    {
        this.owner = owner;
    }
    // Start is called before the first frame update
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        rectTransform.localPosition = new Vector3(Mathf.Lerp(rectTransform.localPosition.x, TargetXPos, 0.1f),
            0, 0);
    }
    private void OnDestroy()
    {
    }

    public void PromoteToFirst()
    {
        StartCoroutine(biggify());
    }
    IEnumerator biggify()
    {
        float tick = 0;
        while(tick <= 0.4f)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(1.53f, 1.53f, 1.53f), 0.1f);
            tick += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
}
