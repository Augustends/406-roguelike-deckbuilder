using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

/// <summary>
/// Updates when the turn order when told to. Looks at an injected turn order of combatant
/// Adds new turn token for new arrivals
/// Deletes new turn tokens if a character died
/// 
/// </summary>
public class TurnDisplay : MonoBehaviour
{
    [SerializeField]Transform holderTr;
    public List<TurnToken> TokenOrder = new List<TurnToken>();

    private void Awake()
    {
        TokenOrder = new List<TurnToken>();
        if (holderTr == null)
        {
            Debug.LogError("Need to specify turn order root in canvas UI");
        }
    }

    // Turn order is determined by the order in the hierarchy
    void Start()
    {
        
    }

    // If you wanna customize how the turn order tokens are spawne. Here is the place
    TurnToken AddToken(CombatCharacters character, List<TurnToken> round, int index_order) {
        if (character == null)
        {
            Debug.LogError("Warning: Tried to add null character");
            return null;
        }
        GameObject newTurnToken = new GameObject(character.transform.name);
        newTurnToken.transform.SetParent(holderTr);
        newTurnToken.transform.SetAsLastSibling();
        newTurnToken.AddComponent<RectTransform>();
        newTurnToken.GetComponent<RectTransform>().sizeDelta = Vector2.one * 100;
        newTurnToken.GetComponent<RectTransform>().localPosition += Vector3.right * 3000;
        newTurnToken.AddComponent<Image>();
        newTurnToken.GetComponent<Image>().sprite = character.UITurnToken;
        newTurnToken.AddComponent<TurnToken>();
        newTurnToken.GetComponent<TurnToken>().Init(character);
        newTurnToken.GetComponent<TurnToken>().order_in_round = index_order;
        round.Add(newTurnToken.GetComponent<TurnToken>());
        return newTurnToken.GetComponent<TurnToken>();
    }

    void RemoveToken(TurnToken token)
    {
        if (TokenOrder.Contains(token))
        {
            TokenOrder.Remove(token);
        }
        Destroy(token.gameObject);
    }

    // Add new ones to display. Delete old ones
    public void Display(List<CombatCharacters> order)
    {
        //Debug.Log(TokenOrder.Count + "   " + TokenOrder.Contains(null));
        List<TurnToken> seen = new List<TurnToken>();

        // If we have a different first person, delete old first token
        if (TokenOrder.Count>0 && order.Count > 0 && order[0] != TokenOrder[0]?.owner)
        {
            RemoveToken(TokenOrder[0]);
            // Update the leading token to be the one in the turn order
            TurnToken newFirstToken = null;
            foreach (var token in TokenOrder)
            {
                if (token.owner == order[0])
                {
                    newFirstToken = token;
                    newFirstToken.PromoteToFirst();
                    break;
                }
            }
            if (newFirstToken == null)
            {
                Debug.LogError("Cannot find the first character in the order: order[0], in the current list of turn tokens");
            }
            TokenOrder.Remove(newFirstToken);
            TokenOrder.Insert(0,newFirstToken);
            return;
        }

        for (int i = 0; i < order.Count; i++)
        {
            bool found = false;
            CombatCharacters turnOwner = order[i];

            // Update ongoing
            foreach (var token in TokenOrder)
            {
                if ( token.owner== turnOwner && !seen.Contains(token))
                {
                    found = true;
                    token.order_in_round = i;
                    seen.Add(token);
                    break;
                }
            }
            // Add new tokens for new arrivals
            if (!found)
            {
                var newToken = AddToken(turnOwner, TokenOrder, i);
                seen.Add(newToken);
            }
        }
        // Delete unused tokens this turnorder
        for (int j = 0; j < TokenOrder.Count; j++)
        {
            if (!seen.Contains(TokenOrder[j]))
            {
                RemoveToken(TokenOrder[j]);
            }
        }
    }

    public void NewTurn()
    {
        
    }

    /*
public IEnumerator Shrink(Transform t)
{
   float tick = 0;
   while(tick < 0.2f)
   {
       tick += Time.deltaTime;
       t.localScale = Vector3.one * (0.2f - tick) / 0.2f;
       yield return new WaitForEndOfFrame();
   }
   Destroy(t.gameObject);
}
*/
}
