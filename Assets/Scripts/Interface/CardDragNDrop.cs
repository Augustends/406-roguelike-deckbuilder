using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class CardDragNDrop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
	[SerializeField]
	private Canvas canvas;

    private RectTransform rectTransform;

	private CanvasGroup canvasGroup;

	private GameObject gameController;

	private CardController cardController;

	// Events
	private UnityEvent cardDragging;
	private UnityEvent cardDropped;

	private void Start()
	{
		// get and store the game and card controllers
		gameController = GameObject.FindWithTag("GameController");
		cardController = gameController.GetComponent<CardController>();

		rectTransform = GetComponent<RectTransform>();
		canvasGroup = GetComponent<CanvasGroup>();

		// get the card events from the card controller
		cardDragging = cardController.cardDragging;
		cardDropped = cardController.cardDropped;
	}

	public void OnPointerDown(PointerEventData eventData)
    {
		Debug.Log("Pointer down");
	}

    public void OnBeginDrag(PointerEventData eventData)
    {
		canvasGroup.blocksRaycasts = false;

		// broadcast that the card has been picked up
		if (cardDragging != null)
		{
			cardDragging.Invoke();
			Debug.Log("Dragging invoked");
		}
	}

    public void OnEndDrag(PointerEventData eventData) 
    {
		canvasGroup.blocksRaycasts = true;

		RaycastHit2D[] hits = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
		Debug.Log(hits.Length);

		// broadcast that the card has been dropped
		if (cardDropped != null)
		{

			cardDropped.Invoke();
		}
	}

    public void OnDrag(PointerEventData eventData)
    {
        rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
	}
}
