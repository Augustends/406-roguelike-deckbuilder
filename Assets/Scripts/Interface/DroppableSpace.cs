using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class DroppableSpace : MonoBehaviour, IDropHandler
{

	// the card controller for this scene
	[SerializeField]
	public GameObject objectWithCardController;

	protected CardController cardController;

	protected CanvasGroup canvasGroup;

	// events
	protected UnityEvent cardDragging;
	protected UnityEvent cardDropped;


	protected void Start()
	{
		// make sure that the mesh is culled when space is invisible
		GetComponent<CanvasRenderer>().cullTransparentMesh = true;

		// get the card controller from the game controller
		cardController = objectWithCardController.GetComponent<CardController>();

		canvasGroup = GetComponent<CanvasGroup>();

		// get events from card controller
		cardDragging = cardController.cardDragging;
		cardDropped = cardController.cardDropped;

		// show the droppable space when a card is picked up
		cardDragging.AddListener(show);
		// hide when a card is dropped
		cardDropped.AddListener(hide);

		// set to invisible by default
		hide();
	}

	public virtual void OnDrop(PointerEventData eventData)
	{
		Debug.Log("Item Dropped");
		if (eventData.pointerDrag != null)
		{
			eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = GetComponent<RectTransform>().anchoredPosition;
		}
	}


	// hides this UI element
	protected void hide()
	{
		// set alpha to 0 to make area invisible
		canvasGroup.alpha = 0;
		// disable ray casts
		canvasGroup.blocksRaycasts = false;
	}

	// shows this UI element
	protected void show()
	{
		// set alpha to 0.6 to make area visible but transparent
		canvasGroup.alpha = 0.6f;
		// enable ray casts
		canvasGroup.blocksRaycasts = true;
	}

}
