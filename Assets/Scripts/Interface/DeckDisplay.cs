using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class DeckDisplay : MonoBehaviour
{
    [SerializeField] protected PlayerData playerData;
    //An array of 3 lists. Each list contains an instantiation of a card from the character's deck
    protected List<Card>[] decks = new List<Card>[3];
    //parent object for each deck
    [SerializeField] protected GameObject[] deckDisplay = new GameObject[3];
    //lists is either the combat deck or discard deck in a combat scene
    protected CardContainer[] lists = new CardContainer[3];
    GameObject characterTabs;
    protected enum CONTAINER_TYPE {FULL_DECK, COMBAT_DECK, DISCARD_PILE}

    protected enum DECK_DISPLAY_STATE { DECKA, DECKB, DECKC, CLOSED }

    protected DECK_DISPLAY_STATE state = DECK_DISPLAY_STATE.CLOSED;

    [SerializeField] protected CONTAINER_TYPE containerType = CONTAINER_TYPE.FULL_DECK;

    public virtual void Awake()
    {
        characterTabs = GameObject.Find("Character Tabs");
        GenerateTabs();
    }
    //Fills out the display
    public virtual void CreateDisplay()
    {
        switch (containerType)
        {
            case CONTAINER_TYPE.FULL_DECK:
                for (int i = 0; i < playerData.party_members.Count; i++)
                {
                    decks[i] = GenerateDeck(playerData.party_members[i].character_deck, deckDisplay[i].transform.Find("Mask").transform.Find("Deck").transform);
                }
                break;
            case CONTAINER_TYPE.COMBAT_DECK:
                for (int i = 0; i < playerData.party_members.Count; i++)
                {
                    lists[i] = GameObject.Find("CombatManager").GetComponent<CombatController>().Party[i].gameObject.GetComponent<PlayerCharacter>().PlayerDeck;
                    decks[i] = GenerateDeck(lists[i].Card_list, deckDisplay[i].transform.Find("Mask").transform.Find("Deck").transform);
                }
                break;
            case CONTAINER_TYPE.DISCARD_PILE:
                for (int i = 0; i < playerData.party_members.Count; i++)
                {
                    lists[i] = GameObject.Find("CombatManager").GetComponent<CombatController>().Party[i].gameObject.GetComponent<PlayerCharacter>().PlayerDiscard;
                    decks[i] = GenerateDeck(lists[i].Card_list, deckDisplay[i].transform.Find("Mask").transform.Find("Deck").transform);
                }
                break;
        }
    }
    /*
     * @list: A list of cards that will be displayed
     * @parent: The parent transform that will contain this list of cards
     * @return: a list of instantiated cards
     */
    protected List<Card> GenerateDeck(List<Card> list, Transform parent)
    {
        List<Card> deck = new List<Card>();
        foreach(Card c in list)
        {
            Card card = GameObject.Instantiate(c, parent);
            deck.Add(card);
            card.GetComponent<RectTransform>().localScale = new Vector3(.7f, .7f, 1);
            card.SetState(Card.CardState.DISPLAYED);
        }
        return deck;
    }
    //overloads the GenerateDeck function and uses a LinkedList instead of a List
    protected List<Card> GenerateDeck(LinkedList<Card> list, Transform parent)
    {
        //create a copy of the given list so it will show up sorted
        Card[] listCopy = new Card[list.Count];
        list.CopyTo(listCopy, 0);
        listCopy = listCopy.OrderBy(x => x.Card_name).ToArray();
        //create a deck to store the instantiated objects
        List<Card> deck = new List<Card>();
        foreach (Card c in listCopy)
        {
            Card card = GameObject.Instantiate(c, parent);
            deck.Add(card);
            card.GetComponent<RectTransform>().localScale = new Vector3(.7f, .7f, 1);
            card.SetState(Card.CardState.DISPLAYED);
        }
        return deck;
    }
    /*
     * Generates tabs with the party member's portraits so you can switch which cardlist you want to view
     */
    protected void GenerateTabs()
    {
        for (int i = 0; i < playerData.party_members.Count; i++)
        {
            //Generate the portraits of the party members
            GameObject portrait = new GameObject();
            Image portraitImage = portrait.AddComponent<Image>();
            portrait.GetComponent<Image>().sprite = GameObject.Instantiate(playerData.party_members[i].tokenSprite);
            portrait.GetComponent<RectTransform>().sizeDelta = new Vector2(170, 170);
            portrait.transform.SetParent(characterTabs.transform);
            Button button = portrait.AddComponent<Button>();
            //Adds the appropriate button component to the tab
            if(i == 0)
            button.GetComponent<Button>().onClick.AddListener(delegate { DisplayDeckA(); });
            if(i == 1)
            button.GetComponent<Button>().onClick.AddListener(delegate { DisplayDeckB(); });
            if(i == 2)
            button.GetComponent<Button>().onClick.AddListener(delegate { DisplayDeckC(); });
        }
    }
    //refreshes the deck displays
    public void RefreshDisplay()
    {
        foreach(GameObject obj in deckDisplay)
        {
            Debug.Log(obj.name);
            foreach(Transform child in obj.transform.Find("Mask").transform.Find("Deck").transform)
            {
                Destroy(child.gameObject);
            }
        }
        CreateDisplay();
    }

    //functions below are for switching character tabs
    public void DisplayDeckA()
    {
        state = DECK_DISPLAY_STATE.DECKA;
        deckDisplay[0].SetActive(true);
        deckDisplay[1].SetActive(false);
        deckDisplay[2].SetActive(false);
    }
    public void DisplayDeckB()
    {
        state = DECK_DISPLAY_STATE.DECKB;
        deckDisplay[0].SetActive(false);
        deckDisplay[1].SetActive(true);
        deckDisplay[2].SetActive(false);
    }

    public void DisplayDeckC()
    {
        state = DECK_DISPLAY_STATE.DECKC;
        deckDisplay[0].SetActive(false);
        deckDisplay[1].SetActive(false);
        deckDisplay[2].SetActive(true);
    }
    //functions below are fore opening/closing the entire display
    public void CloseDisplay()
    {
        state = DECK_DISPLAY_STATE.CLOSED;
        this.gameObject.SetActive(false);
    }
    //opens general display
    private void OpenDisplay()
    {
        RefreshDisplay();
        this.gameObject.SetActive(true);
        DisplayDeckA();
    }
    //opens party's discard pile
    public void OpenDiscard()
    {
        containerType = CONTAINER_TYPE.DISCARD_PILE;
        OpenDisplay();
    }
    //opens party's current drawpile
    public void OpenCombatDeck()
    {
        Debug.Log("Open COMBAT DECK");
        containerType = CONTAINER_TYPE.COMBAT_DECK;
        OpenDisplay();
    }
    //displays party's full decklist
    public void OpenFullDeckList()
    {
        containerType = CONTAINER_TYPE.FULL_DECK;
        OpenDisplay();
    }
}
