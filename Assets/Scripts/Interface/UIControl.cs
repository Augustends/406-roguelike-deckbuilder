using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class UIControl : Singleton<UIControl>
{
    // Auth: Ryan K Larsen, rkl830, CMPT 406
    [SerializeField]
    Canvas canvas;

    public bool draggingCard = false;

	// the current active player. obtained from TurnMechanic.
	private PlayerCharacter currentPlayer
	{
		get { return (PlayerCharacter)TurnMechanic.Instance.ActiveCharacter; }
	}

	// Start is called before the first frame update
	void Start()
    {

    }

	protected override void Awake()
	{
		base.Awake();
	}

	protected override void OnDestroy()
	{
		base.OnDestroy();
	}

	// ------------------------- Some methods for utilization in the back-end. The other methods in this class ignore back-end and use their own structures.
	// ------------------------- Lachlan Muir

	public enum Destination { HAND, DECK, DISCARD };

	/**
	 * Moves the given card to the desired destination container.
	 * 
	 * @author: Lachlan Muir
	 * @param: card - the card to be moved.
	 * @param: dest - a Destination enum value used to determine where the card should be sent.
	 * @param: player - the player whose container the card should be sent to.
	 */
	public void moveCard(Card card, Destination dest, PlayerCharacter player)
	{
		if (card == null)
			Debug.LogError("Error in moveCard - card cannot be null");
		if (player == null)
			Debug.LogError("Error in moveCard - player cannot be null");
		// must start the transition animation as well as set the new card state for each destination
		switch (dest) 
		{
			case Destination.DECK:
				StartCoroutine(SmoothTransition(card.transform, player.PlayerDeck.transform));
				card.InDeck();
				break;
			case Destination.DISCARD:
				StartCoroutine(SmoothTransition(card.transform, player.PlayerDiscard.transform));
				card.InDiscard();
                card.transform.up = new Vector3(0, 1, 0);
                break;
			case Destination.HAND:
				StartCoroutine(SmoothTransition(card.transform, player.PlayerHand.transform));
				card.InHand();
				break;
			default:
				Debug.LogError("Error in moveCard - Invalid destination.");
				break;
		}
	}

	/**
	 * Moves multiple cards using the same behaviour as moveCard.
	 * 
	 * @author: Lachlan Muir
	 * @param: cards - an array of cards to be moved
	 * @param: dest - a Destination enum value used to determine where the card should be sent.
	 * @param: player - the player whose container the card should be sent to.
	 */
	public void moveMultiple(Card[] cards, Destination dest, PlayerCharacter player)
	{
		foreach (Card card in cards)
		{
			if (card != null)
			{
				moveCard(card, dest, player);
			}
			else
				Debug.LogError("Error in moveMultiple - cannot move a null card.");
		}
	}

	// ---------------------------------------------------------------------------------------

	/**
     * Given a card, will transition that card from the hand to the discard pile, either from playing the card or discarding it
     * @param card A card to be moved to the discard pile
     * @precond A card was played or needs to be discarded
     * @postcond card is now in the discard pile
     */
	public void CardInHandToDiscard(GameObject card)
    {
        StartCoroutine(SmoothTransition(card.transform, currentPlayer.PlayerDiscard.transform));
        card.GetComponent<Card>().InDiscard();
    }

    /**
     * Draws a random card from the deck and puts it into the hand
     */
    public void DrawCardFromDeck(Card card)
    {
		GameObject deck = currentPlayer.PlayerDeck.gameObject;
		GameObject hand = currentPlayer.PlayerHand.gameObject;

        void Draw() // Function to use for drawing a card
        {
            GameObject cardToDraw = card.gameObject;
            //StartCoroutine(SmoothTransition(cardToDraw.transform, hand.transform));
            cardToDraw.GetComponent<Card>().InHand();
            
        }
        
        Draw();
        UpdateCardHand();
    }

    /**
     * Sends all cards currently in the discard to the draw deck
     * @precond Cards in discard deck
     * @postcond All card gameobjects in discard pile are now in the draw deck
     */
    public void DiscardPileToDeck()
    {
		GameObject deck = currentPlayer.PlayerDeck.gameObject;
		GameObject discard = currentPlayer.PlayerDiscard.gameObject;

		foreach (Transform card in discard.transform.Cast<Transform>().ToList())
        {
            StartCoroutine(SmoothTransition(card, deck.transform));
            card.GetComponent<Card>().InDeck();
        }
    }

    /**
     * Used on the end of the characters turn, moves all cards currently in the hand to the discard pile
     * @precond Cards in hand to move to the discard pile
     * @postcond Hand is empty, cards are in the discard pile.
     */
    public void EndTurnDiscard()
    {
		GameObject hand = currentPlayer.PlayerHand.gameObject;
		GameObject discard = currentPlayer.PlayerDiscard.gameObject;

		foreach (Transform card in hand.transform.Cast<Transform>().ToList())
        {
            StartCoroutine(SmoothTransition(card.transform, discard.transform));
            card.GetComponent<Card>().InDiscard();
        }
    }

    /**
     * Given a card, will add it to the draw deck and set needed fields for interaction
     * @param card Card to add to the deck
     * @postcond Card added to draw deck
     */
    public void AddCardToDrawDeck(GameObject card)
    {
		GameObject deck = currentPlayer.PlayerDeck.gameObject;
		card.GetComponent<DragNDrop>().SetCanvas(canvas);
        card.transform.SetParent(deck.transform);
        card.transform.position = deck.transform.position;
    }

    /**
     * Offset is located at (960,-800), radius should be roughly 950.  This is sort of a magic number shitshow, but trust me, it works for 1920x1080. - Ryan
     * This should be called any time a card is attempted to play, played, drawn, discarded, or anything that can change the hand.
     * 
     */
    public void UpdateCardHand()
    {
        GameObject hand = currentPlayer.PlayerHand.gameObject;
        int cardCount = currentPlayer.PlayerHand.getCurrentHandSize();
        int i = 0;
        float sizeOfYOffset = 1.6f;
        float sizeOfXOffset = 1.6f;
        float xOffset = 960;
        float yOffset = -800;
        foreach (Card card in currentPlayer.PlayerHand.CurrentHand())
        {
            Vector3 newPosition;
            if (cardCount % 2 != 0)
            {
                newPosition = new Vector3(960 + 1000 * Mathf.Sin(0.1f * ((-cardCount / 2 + i) * sizeOfXOffset)), yOffset + 950 * Mathf.Cos(0.1f * ((-cardCount / 2 + i) * sizeOfYOffset)));
            }
            else // Add half an offset for even cases
            {
                newPosition = new Vector3(960 + 1000 * Mathf.Sin(0.1f * ((-cardCount / 2 + i) * sizeOfXOffset + sizeOfXOffset / 2)), yOffset + 950 * Mathf.Cos(0.1f * ((-cardCount / 2 + i) * sizeOfYOffset + sizeOfYOffset / 2)));
            }
            GameObject newObj = new GameObject();
            newObj.transform.position = newPosition;
            card.handPos = newPosition;
            StartCoroutine(SmoothTransition(card.transform, newObj.transform, false, 0.5f, false));
            Destroy(newObj, 0.6f);
            // I actually hate the way I've done this but it works TODO: overhaul SmoothTransition to not use transforms, because that was a poor idea in retrospect
            card.transform.up = new Vector3(newPosition.x - 960, newPosition.y + 800); // Sets the rotation for the cards
            card.transform.SetSiblingIndex(i);
            i++;
        }

    }


    /**
     * Given two transforms, will move the first transform smoothly to the second one over smoothTime seconds.  Additionally
     * transformToMove will become a child of endpoint.
     * @param transformToMove Object to move
     * @param endpoint Point to move object to
     * @param setParent True by default, determines whether the parent is set for the first object to the second object
     * @param smoothTime The time to go through the animation, lower is faster, set to 0.7f by default
     * @param calledOutside If you are calling it inside the UpdateCardHand function, this must be false to prevent an infinite loop
     * @precond 2 transforms that can be moved
     * @postcond The first transform will be moved to the endpoint transform and will be a child of the endpoint
     */
    IEnumerator SmoothTransition(Transform transformToMove, Transform endpoint, bool setParent = true, float smoothTime = 0.4f, bool calledOutside = true)
    {
        float timeElapsed = 0;
        Vector3 velocity = Vector3.zero;
        float totalTime = smoothTime;

        while (timeElapsed < totalTime)
        {
            transformToMove.position = Vector3.SmoothDamp(transformToMove.position, endpoint.position, ref velocity, smoothTime);
            timeElapsed += Time.deltaTime;
            smoothTime -= Time.deltaTime;
            yield return null;
        }
        if (setParent)
        {
            //transformToMove.SetParent(endpoint);
        }
        transformToMove.position = endpoint.position;
        if (calledOutside)
        {
            //UpdateCardHand();
        }
    }

    IEnumerator HandFlourishWaitTimer()
    {
        float timeElapsed = 0;
        float totalTime = 0.44f;

        while (timeElapsed < totalTime)
        {
            timeElapsed += Time.deltaTime;
            yield return null;
        }

        UpdateCardHand();
    }

    public void HandWaitTimer()
    {
        StartCoroutine(HandFlourishWaitTimer());
    }
}
