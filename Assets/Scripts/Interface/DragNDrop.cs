using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragNDrop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler, IPointerEnterHandler, IPointerExitHandler
{
	[SerializeField]
	private Canvas canvas;

    private RectTransform rectTransform;

	private CanvasGroup canvasGroup;

	private Card myCard;

	private PlayArea playArea;

	private int siblingIndex;
	private bool hovered;
	private bool moving;
	private bool dragging;
	private Vector3 velocity = Vector3.zero;
	private Vector3 velo = Vector3.zero;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
		canvasGroup = GetComponent<CanvasGroup>();
		myCard = GetComponent<Card>();
		hovered = false;
		moving = false;
		dragging = false;
	}

	void Start()
	{
		if (canvas == null)
		{
			canvas = GameObject.FindGameObjectWithTag("CombatCanvas").GetComponent<Canvas>();
		}
		HidePlayArea();//make sure play area is initially hidden
	}

	void Update()
    {
		if (hovered && !dragging && !UIControl.Instance.draggingCard)
        {
			myCard.transform.localScale = Vector3.SmoothDamp(myCard.transform.localScale, new Vector3(1.1f, 1.1f, 1.1f), ref velocity, 0.1f);
        } else if (!hovered && myCard.transform.localScale.x > 0.8f)
        {
			myCard.transform.localScale = Vector3.SmoothDamp(myCard.transform.localScale, new Vector3(0.8f, 0.8f, 0.8f), ref velocity, 0.1f);
        }
        // Commented out for now, involves moving the card up
        if (hovered && !dragging && !UIControl.Instance.draggingCard)
        {
            myCard.transform.position = Vector3.SmoothDamp(myCard.transform.position, new Vector3(myCard.handPos.x, myCard.handPos.y + 60, myCard.handPos.z), ref velo, 0.1f);
            moving = true;
        }
        else if (!hovered && myCard.transform.position.y > myCard.handPos.y && moving && !dragging)
        {
            myCard.transform.position = Vector3.SmoothDamp(myCard.transform.position, myCard.handPos, ref velo, 0.1f);
        }
        else
        {
            moving = false;
        }

    }

	private void HidePlayArea()
	{
		PlayArea.Instance.GetComponent<SpriteRenderer>().color = new Color(0f, 0f, 0f, 0f);
		PlayArea.Instance.GetComponent<BoxCollider2D>().enabled = false;
	}

	private void ShowPlayArea()
	{
		PlayArea.Instance.GetComponent<SpriteRenderer>().color = new Color(0f, 0f, 0f, 0f);
		PlayArea.Instance.GetComponent<BoxCollider2D>().enabled = true;
	}
    
    public void OnPointerDown(PointerEventData eventData)
    {
		//Debug.Log("Pointer down");
	}

    public void OnBeginDrag(PointerEventData eventData)
    {
		dragging = true;
		UIControl.Instance.draggingCard = true;
		//Debug.Log("Begin drag");
		canvasGroup.blocksRaycasts = false;
		myCard.transform.up = new Vector3(0, 1, 0);
		// we only want the play area to show up if the card isn't targeted
		if (requirePlayArea(myCard.TargetType))
			ShowPlayArea();
	}

	/*
	 * Return true if the target type requires the play area to appear
	 * else return false
	 * @author Tanner Skomar
	*/
	public bool requirePlayArea(Card.CardTargetType targetType)
    {
		switch (targetType)
		{
			case Card.CardTargetType.ALL:
			case Card.CardTargetType.ALLIES:
			case Card.CardTargetType.ENEMIES:
			case Card.CardTargetType.SELF:
				return true;
			case Card.CardTargetType.TARGETED:
			case Card.CardTargetType.SINGLE_PLAYER:
			case Card.CardTargetType.SINGLE_ENEMY:
				return false;
			default:
				Debug.LogError("Card does not have a valid target type.");
				break;
		}
		return false;
    }

    public void OnEndDrag(PointerEventData eventData) 
    {
		StartCoroutine(CardInteractionDelay());
		UIControl.Instance.draggingCard = false;
		//Debug.Log("End drag");
		RaycastHit2D[] hits = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

		var validHits = new List<RaycastHit2D>(hits).Where(h => h.transform.GetComponent<Targetable>() != null).ToList();

		if (validHits.Count != 0)
		{
			CardController.Instance.playCard(gameObject.GetComponent<Card>(), validHits[0].transform.GetComponent<CombatCharacters>());
		} else // If there is no valid hit, just run CantPlay immediately, return card to hand
        {
			//CardController.Instance.CantPlay(gameObject.GetComponent<Card>());
        }
		CardController.Instance.CantPlay(gameObject.GetComponent<Card>());
		canvasGroup.blocksRaycasts = true;
		HidePlayArea();
	}

    public void OnDrag(PointerEventData eventData)
    {
		myCard.transform.SetAsLastSibling();
		rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
	}

	public void OnPointerEnter(PointerEventData eventData)
    {
		if (!hovered && !UIControl.Instance.draggingCard)
        {
            siblingIndex = myCard.transform.GetSiblingIndex();
            hovered = true;
            myCard.transform.SetAsLastSibling();			
        }
    }

	public void OnPointerExit(PointerEventData eventData)
    {
		if (hovered)
        {
            myCard.transform.SetSiblingIndex(siblingIndex);
            hovered = false;
        }
    }

	IEnumerator CardInteractionDelay()
    {
		float timeElapsed = 0;
		while (timeElapsed < 0.5f)
        {
			timeElapsed += Time.deltaTime;
			yield return null;
        }
		dragging = false;

    }

	public void SetCanvas(Canvas newCanvas)
    {
		canvas = newCanvas;
    }
}
