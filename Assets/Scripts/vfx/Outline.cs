using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericOutline : MonoBehaviour
{
    // Default outline properties.
    [SerializeField]
    Color outlineColor = new Color(0f, 0f, 0f, 1f);
    [SerializeField]
    float outlineWidth = 0.015f;
    [SerializeField]
    float outlineAlpha = 1f;

    public GenericOutline setColor(Color newColor) { outlineColor = newColor; dirty = true; return this; }
    public GenericOutline setWidth(float newWidth) { outlineWidth = newWidth; dirty = true; return this; }
    public GenericOutline setAlpha(float newAlpha) { outlineAlpha = newAlpha; dirty = true; return this; }

    private SpriteRenderer myRenderer;
    private Material oldMaterial;
    private MaterialPropertyBlock matProperties;
    bool dirty = true;

    // Start is called before the first frame update
    void Start()
    {
        myRenderer = GetComponent<SpriteRenderer>();
        oldMaterial = myRenderer.material;
        myRenderer.material = Resources.Load<Material>("Materials/SpriteOutline");
        matProperties = new MaterialPropertyBlock();
    }

    void onDestroy()
    {
        myRenderer.material = oldMaterial;
    }

    // Update is called once per frame
    void Update()
    {
        if (dirty)
        {
            myRenderer.GetPropertyBlock(matProperties);
            matProperties.SetColor("_OutlineColor", outlineColor);
            matProperties.SetFloat("_OutlineWidth", outlineWidth);
            matProperties.SetFloat("_OutlineAlpha", outlineAlpha);
            myRenderer.SetPropertyBlock(matProperties);
            dirty = false;
        }
    }
}
