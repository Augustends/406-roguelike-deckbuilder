/* MouseoverOutline.cs
 * by Haydn V. Harach
 * for CMPT406 Roguelike Deckbuildere
 * Feb22 '21
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseoverOutline : MonoBehaviour
{
    // Default outline properties.
    [SerializeField]
    Color normalOutlineColor = new Color(0f, 0f, 0f, 1f);
    [SerializeField]
    float normalOutlineWidth = 0.015f;
    [SerializeField]
    float normalOutlineAlpha = 1f;

    // Outline properties when the mouse is over the sprite.
    [SerializeField]
    Color selectedOutlineColor = new Color(1f, 1f, 1f, 1f);
    [SerializeField]
    float selectedOutlineWidth = 0.015f;
    [SerializeField]
    float selectedOutlineAlpha = 1f;

    [SerializeField]
    bool selected = false;

    bool usingSelectedProperties = false;
    private SpriteRenderer myRenderer;
    private MaterialPropertyBlock matProperties;

    // Start is called before the first frame update
    void Start()
    {
        myRenderer = GetComponent<SpriteRenderer>();
        matProperties = new MaterialPropertyBlock();
        myRenderer.GetPropertyBlock(matProperties);
        matProperties.SetColor("_OutlineColor", normalOutlineColor);
        matProperties.SetFloat("_OutlineWidth", normalOutlineWidth);
        matProperties.SetFloat("_OutlineAlpha", normalOutlineAlpha);
        myRenderer.SetPropertyBlock(matProperties);
    }

    // Update is called once per frame
    void Update()
    {
        if (selected && !usingSelectedProperties)
        {
            myRenderer.GetPropertyBlock(matProperties);
            matProperties.SetColor("_OutlineColor", selectedOutlineColor);
            matProperties.SetFloat("_OutlineWidth", selectedOutlineWidth);
            matProperties.SetFloat("_OutlineAlpha", selectedOutlineAlpha);
            myRenderer.SetPropertyBlock(matProperties);
            usingSelectedProperties = true;

        }

        if (!selected && usingSelectedProperties)
        {
            myRenderer.GetPropertyBlock(matProperties);
            matProperties.SetColor("_OutlineColor", normalOutlineColor);
            matProperties.SetFloat("_OutlineWidth", normalOutlineWidth);
            matProperties.SetFloat("_OutlineAlpha", normalOutlineAlpha);
            myRenderer.SetPropertyBlock(matProperties);
            usingSelectedProperties = false;
        }
    }

    void OnMouseEnter()
    {
        selected = true;
    }

    void OnMouseExit()
    {
        selected = false;
    }
}
