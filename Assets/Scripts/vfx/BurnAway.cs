using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurnAway : MonoBehaviour
{
    Material burnMaterial = null;

    [SerializeField]
    float timeToBurn = 2f;
    public void setBurnTime(float newBurnTime) { timeToBurn = newBurnTime; }

    [SerializeField]
    Color burnColor = new Color(1f, 0.5f, 0f, 1f);
    public void setBurnColor(Color newColor) { burnColor = newColor; }

    [SerializeField]
    Action<GameObject> overrideDestroy = null;
    public void OverrideDestroy(Action<GameObject> action) { overrideDestroy = action; }

    private bool active = true;
    private float threshold = 1f;

    private SpriteRenderer myRenderer;
    private MaterialPropertyBlock matProperties;

    // Start is called before the first frame update
    void Start()
    {
        burnMaterial = Resources.Load<Material>("Materials/BurnAway");
        myRenderer = GetComponent<SpriteRenderer>();
        myRenderer.material = burnMaterial;
        matProperties = new MaterialPropertyBlock();
    }

    // Update is called once per frame
    void Update()
    {
        if (!active) return;

        threshold -= Time.deltaTime / timeToBurn;

        myRenderer.GetPropertyBlock(matProperties);
        matProperties.SetColor("_BurnColor", burnColor);
        matProperties.SetFloat("_Threshold", threshold);
        myRenderer.SetPropertyBlock(matProperties);
        
        if (threshold <= 0f)
        {
            if (overrideDestroy != null)
            {
                overrideDestroy(this.gameObject);
                active = false;
            }
            else Destroy(this);
        }
    }
}
