using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy Encounter")]
[System.Serializable]
public class EncounterDataSO:ScriptableObject
{
    public EncounterType type;
    public int Count { get => enemyPrefabs.Length; }
    [SerializeField] private int cr;
    // Add up all monster CR. Apply multiplier based on how many enemies there are. 3 monsters increased the CR by a 2.4 multiplier as
    // there are more mosters attacking the player per turn. THis is similar to how DnD encounters are generated
    public int CR {
        get
        {
            int totalCR = 0;
            foreach (var enemy in enemyPrefabs)
            {
                totalCR += enemy.CR;
            }
            totalCR = (int)(totalCR * multiplierTable[Count]);
            cr = totalCR;
            return totalCR;
        }
    }
    public EnemyCharacter[] enemyPrefabs;
    public float[] enemyXPosition;
    // For scaling encounter CR by how many monsters are in combat
    Dictionary<int, float> multiplierTable = new Dictionary<int, float>() { { 1, 1 }, { 2, 1.5f }, { 3, 2 }, { 4, 2 }, { 5, 2 }, { 6, 3} };
}
public enum EncounterType
{
    NORMAL,
    ELITE,
    SHOP,
    CAMP,
    RANDOM
}
