using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossroadEvent : Singleton<CrossroadEvent>
{
	[SerializeField]
	GameObject description;

	public GameObject Description
	{
		get { return description; }
	}

	[SerializeField]
	GameObject[] optionButtons;

	public GameObject[] OptionButtons
	{
		get { return optionButtons; }
	}

	[SerializeField]
	GameObject continueButton;

	public GameObject ContinueButton 
	{
		get { return continueButton; }
	}
}
