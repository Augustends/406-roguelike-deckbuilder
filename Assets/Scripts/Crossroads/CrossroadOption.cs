using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossroadOption : MonoBehaviour
{
    public virtual void activate() {
		AudioManager.Instance.Play("Button"); // Plays named sound effect (Simon)
		CrossroadController.Instance.CrossroadOptionSelected();
	}
}
