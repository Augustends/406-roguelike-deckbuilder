using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossroadController : Singleton<CrossroadController>
{
	[SerializeField]
	GameObject[] crossroadEvents;



    // Start is called before the first frame update
    void Start()
    {
		// get a random event from the available crossroad events
		int randomCEIdx = Random.Range(0, crossroadEvents.Length);
		Debug.Log(randomCEIdx);
		GameObject selectedEvent = crossroadEvents[randomCEIdx];
		// load the event prefab into the scene
		Instantiate(selectedEvent);
        FadesManager.Instance.FadeIn();
    }

	public void CrossroadOptionSelected() 
	{
		// disable the options so the player can't click another one
		foreach (GameObject option in CrossroadEvent.Instance.OptionButtons)
		{
			option.SetActive(false);
		}

		// disable the scene description so it isn't floating around awkwardly
		CrossroadEvent.Instance.Description.SetActive(false);

		// enable the continue button so the player can return to the overworld
		CrossroadEvent.Instance.ContinueButton.SetActive(true);

		// TODO: add a display for the effects of the option selected
	}
}
