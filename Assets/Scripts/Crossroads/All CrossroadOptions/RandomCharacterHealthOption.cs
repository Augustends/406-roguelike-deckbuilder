using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomCharacterHealthOption : CrossroadOption
{
	[SerializeField]
	int amount;

	public override void activate()
	{
		// activate body

		// get a random character from the party data
		PlayerData playerData = Resources.Load<PlayerData>("PlayerData");
		int randIdx = Random.Range(0, playerData.party_members.Count);

		CharacterData character = playerData.party_members[randIdx];

		Debug.Log("Before health modified: " + character.cur_health);

		character.cur_health += amount;
		if (character.cur_health <= 0) 
		{
			character.cur_health = 1;
		}
		else if (character.cur_health > character.max_health)
		{
			character.cur_health = character.max_health;
		}

		Debug.Log("After health modified: " + character.cur_health);

		// call base for proper exiting
		base.activate();
	}
}
