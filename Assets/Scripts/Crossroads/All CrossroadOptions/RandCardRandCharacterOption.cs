using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandCardRandCharacterOption : CrossroadOption
{
	public override void activate()
	{
		// get a random character from the party data
		PlayerData playerData = Resources.Load<PlayerData>("PlayerData");
		int randIdx = Random.Range(0, playerData.party_members.Count);

		CharacterData character = playerData.party_members[randIdx];

		Card card = character.full_card_list[Random.Range(0, character.full_card_list.Count)];

		Debug.Log("Character deck before: " + character.character_deck);

		character.AddCard(card);

		Debug.Log("Character deck after: " + character.character_deck);

		// call base for proper exiting
		base.activate();
	}
}
