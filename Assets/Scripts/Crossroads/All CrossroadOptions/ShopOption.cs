using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShopOption : CrossroadOption
{
	[SerializeField]
	int percentChance;
	public override void activate()
	{
		if (Random.Range(0, 100) < percentChance)
		{
			Debug.Log("Shop start!");
			// unload the crossroad scene
			GameController.Instance.UnloadLevel(SceneManager.GetActiveScene().name);
			// load the combat scene
			GameController.Instance.LoadLevel("Shop");
		}
		else
			Debug.Log("Shop will not start.");

		// call base activate for proper exiting
		base.activate();
	}
}
