using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CombatOption : CrossroadOption
{
	[SerializeField]
	int percentChance;
	public override void activate()
	{
		if (Random.Range(0, 100) < percentChance)
		{
			Debug.Log("Combat start!");
			// unload the crossroad scene
			GameController.Instance.UnloadLevel(SceneManager.GetActiveScene().name);
			// load the combat scene
			GameController.Instance.LoadLevel("CombatSceneDuncan");
		}
		else
			Debug.Log("Combat will not start.");

		// call base activate for proper exiting
		base.activate();
	}
}
