using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyOption : CrossroadOption
{
	[SerializeField]
	int amount;

	public override void activate()
	{
		PlayerData playerData = Resources.Load<PlayerData>("PlayerData");

		Debug.Log(playerData.money);
		playerData.AddMoney(amount);
		Debug.Log(playerData.money);
		base.activate();
	}
}
