/*Alex Shepherd, AAS870
 * 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

public class ShopItem : MonoBehaviour

{
    //the item that is displayed for sale
    protected GameObject item;
    //cost of the item
    public int price;
    public TextMeshProUGUI priceDisplay;
    protected PlayerData playerData;

    private void Start()
    {
        playerData = Resources.Load<PlayerData>("PlayerData");
    }
    //checks if the player has enough money to purchase item
    public bool CanBuy()
    {
        if (playerData.money >= price)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    //sets the price for the item. Is called by the ShopController when the item is generated
    public void SetPrice(int x)
    {
        price = x;
        priceDisplay.GetComponent<TextMeshProUGUI>().text = "$" + x;
    }

}
