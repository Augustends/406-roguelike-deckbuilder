/*Alex Shepherd, AAS870
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ShopController : LootDisplay
{
    [SerializeField]
    private ShopCard shopCard;
    //Reference to local canvas.
    public GameObject cardParent;
    //A ui text element that displays the player's current cash amount
    [SerializeField]
    private TextMeshProUGUI playerMoneyDisplay;


    // Start is called before the first frame update
    void Start()
    {
        playerData = Resources.Load<PlayerData>("PlayerData");
        //set money display to players current money
        RefreshMoneyDisplay();
        //generate part members*4 cards
        GenerateCardRewardDisplay(cardParent.gameObject, playerData.party_members.Count, 4, new Vector2(25, 150), new Vector2(90, 170));
        for(int i = 0; i < 4; i++)
        {
            for(int j = 0; j < 3; j++)
            {
                cardLoot[j, i].gameObject.GetComponent<RectTransform>().localScale = new Vector3(0.23f, 0.23f, 1);
            }
        }
        for(int i = 0; i< playerData.party_members.Count; i++)
        {
            for(int j = 0; j < 4; j++)
            {
                //put shopCard object onto cards
                ShopCard shopCardStats = GameObject.Instantiate(shopCard, cardLoot[i, j].transform);
                shopCardStats.SetPrice(75 + (5 * Random.Range(0, 10)));
            }
        }
    }

    public override void CardSelected()
    {
        //get card
        Card choice = EventSystem.current.currentSelectedGameObject.GetComponentInParent<Card>();
        Debug.Log(choice.name);
        ShopCard shopStats = choice.gameObject.transform.Find("Shop Card(Clone)").GetComponent<ShopCard>();
        if (shopStats.CanBuy())
        {
            AudioManager.Instance.Play("ShopPurchase"); // Plays named sound effect (Simon)
            Debug.Log("I've been purchased");
            //Subtract cards cost from players wallet
            playerData.AddMoney(-shopStats.price);
            //update wallet display
            RefreshMoneyDisplay();
            //this is to check which character the card belongs to and then places it in their deck
            for (int i = 0; i < playerData.party_members.Count; i++)
            {
                //add card to deck, position i should be the same character with a card in position i of selectedCards
                for (int j = 0; j < playerData.party_members[i].full_card_list.Count; j++)
                {
                    Debug.Log("I see the card: " + playerData.party_members[i].full_card_list[j].name);
                    if (choice.Card_name == playerData.party_members[i].full_card_list[j].Card_name)
                    {
                        Debug.Log("I found it!");
                        playerData.party_members[i].AddCard(playerData.party_members[i].full_card_list[j]);
                        TextMeshProUGUI text = choice.transform.Find("Shop Card(Clone)").Find("Price").GetComponent<TextMeshProUGUI>();
                        text.text = "SOLD";

                        // If change back to destroying the card, then uncomment below line to remove the tooltip that will persist
                        //choice.gameObject.GetComponent<TooltipCardHover>().destroyTooltip(); // editted by Travis Baldwin, needed to destroy tooltip when purchased
                        Destroy(choice.transform.Find("CardButton(Clone)").GetComponent<Button>());
                        return;
                    }
                }
            }
        }
        else
        {
            Debug.Log("This card costs too much!");
            AudioManager.Instance.Play("Locked"); // Plays named sound effect (Simon)
        }
    }
    //refreshes the UI element displaying the players current wallet amount
    public void RefreshMoneyDisplay()
    {
        playerMoneyDisplay.text = "$" + playerData.money;
    }
}
