using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class LootDisplay : MonoBehaviour
{
    [SerializeField] protected PlayerData playerData;
    [SerializeField] protected GameObject cardButton;
    protected Card[,] cardLoot = new Card[10,10];


    /*
    * Finds the necessary information needed to generate the Card selection screen in a grid
    * @parent: a gameObject that will be the parent for the cards
    * @rows: The number of rows you want to display
    * @columns the number of columns you want to display
    * @startPosition: The position of the top left card displayed stored as a Vector2
    * @offset: The distance each card have between each other stored as a Vector2
    * @author: Alex Shepherd
    */
    public void GenerateCardRewardDisplay(GameObject parent, int rows, int columns, Vector2 startPosition, Vector2 offset)
    {
        //Gets cards to display as loot
        for (int i = 0; i < rows; i++)
        {
            //get rowLength cards and store them in the cards array
            Card[] cards = new Card[columns];
            cards = GetCards(columns, playerData.party_members[i]);
            for (int j = 0; j < columns; j++)
            {
                //instantiate card in worldspace
                Card card = GameObject.Instantiate(cards[j].GetComponent<Card>(), parent.transform);
                //position card in correct spot
                card.GetComponent<RectTransform>().localPosition = new Vector3(startPosition.x + (j * offset.x), startPosition.y - (i * offset.y), 0);
                //scale card to fit canvas
                card.GetComponent<RectTransform>().localScale = new Vector3(0.25f, 0.25f, 0);
                //set state so that you can actually see the card
                card.SetState(Card.CardState.DISPLAYED);
                //add button to card so it can be selected
                GameObject button = Instantiate(cardButton, card.transform);
                button.GetComponent<Button>().onClick.AddListener(delegate { CardSelected(); });
                //place card in cardLoot array with other cards
                cardLoot[i, j] = card;
            }
        }
    }
    /* Retrieves a given amount of cards from a character's full cardlist 
  * @amount: The number of cards you want to retrieve
  * @character: The character whose card list you want to draw from
  * @return: an array of cards retrieved from the full card list from the given character 
  * @author: Alex Shepherd
  */
    public Card[] GetCards(int amount, CharacterData character)
    {
        //shuffles the deck of cards and draws one from it
        Card[] loot = new Card[amount];
        for (int i = 0; i < amount; i++)
        {
            Card newLoot = character.full_card_list[Random.Range(0, character.full_card_list.Count)];
            while (CheckDuplicate(newLoot, loot))
            {
                newLoot = character.full_card_list[Random.Range(0, character.full_card_list.Count)];
            }
            loot[i] = newLoot;
        }
        return loot;
    }
    /*
     * Checks if a given card exists in the given array
     * @card: The card to be checked for dupes
     * @cardList: an array of cards to search through
     * @return: a bool that returns true if the card already exists in the array, and false otherwise
     * @author: Alex
     */
    public bool CheckDuplicate(Card card, Card[] cardList)
    {
        for (int i = 0; i < playerData.party_members.Count; i++)
        {
            if(cardList[i] != null)
            {
                if (card.Card_name == cardList[i].Card_name && cardList[i])
                {
                    return true;
                }
            }
        }
        return false;
    }
    //To be overwritten by class that inherits from this one. The function activates when a card is clicked
    public virtual void CardSelected()
    {
    }
}
