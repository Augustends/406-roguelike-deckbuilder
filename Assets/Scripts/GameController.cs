using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


// Track what level is being played
// Load and unload game levels
// Can create/destroy other globally persistent systems
// Knows the current state of the game.
// Can load and save game
// Only One game controller exists at a time and is globally accesable through it's static instance
// Author Duncan Boyes
public class GameController : Singleton<GameController>
{

    GameState currentGameState = GameState.PREGAME;
    private GameState CurrentGameState { get => currentGameState;}
    [SerializeField] GameStateChangeEvent gameStateChangeEvent;


    public GameObject[] systemPrefabs;
    private List<GameObject> instancedSystemPrefabs;

    public PlayerData playerData;
    public OverworldStateData overworldStateData;
    CharacterRoster partyMemberRoster;
    public string currentLevelName = string.Empty;

    [SerializeField] VoidEvent LevelLoadedEvent;

    protected override void Awake()
    {
        base.Awake(); //Initialize singleton

        playerData = Resources.Load<PlayerData>("Playerdata");  // Get player data

        partyMemberRoster = Resources.Load<CharacterRoster>("character_data/PartyMemberRoster"); // Get full list of all possible party members
    }

    void Start()
    {
        DontDestroyOnLoad(gameObject);

        InstantiateSystemPrefabs();
    }


    void InstantiateSystemPrefabs()
    {
        instancedSystemPrefabs = new List<GameObject>();
        for (int i = 0; i < systemPrefabs.Length; i++)
        {
            instancedSystemPrefabs.Add(Instantiate(systemPrefabs[i])); // Instance the public inspector list of desired game systems and track them
        }
    }


    public void LoadLevel(string levelName)
    {
        AsyncOperation ao = SceneManager.LoadSceneAsync(levelName,LoadSceneMode.Additive); // Loads in background and doesnt block code
        if (ao == null)
        {
            Debug.LogError("Gamemanager unable to load level: " + levelName);
        }
        ao.completed += OnLoadOperationComplete; // Actions to complete upon finishing load
        currentLevelName = levelName;
    }


    /// <summary>
    /// Actions to perform upon completing load
    /// </summary>
    /// <param name="obj"></param>
    private void OnLoadOperationComplete(AsyncOperation obj)
    {
        UpdateState(GameState.RUNNING);
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(currentLevelName));
        // Notify listeners
        LevelLoadedEvent.Raise();
    }


    public void UnloadLevel(string levelName)
    {
        AsyncOperation ao = SceneManager.UnloadSceneAsync(levelName);
        if (ao == null)
        {
            Debug.LogError("Gamemanager unable to unload level: " + levelName);
        }
        ao.completed += OnUnloadComplete;
    }


    private void OnUnloadComplete(AsyncOperation obj)
    {
        Debug.Log("Unload complete");
        
    }


    void RecruitCharacter(string name)
    {
        playerData.Recruit(partyMemberRoster.GetCharacter(name));
    }


    void DismissCharacter(string name)
    {
        playerData.Dismiss(partyMemberRoster.GetCharacter(name));
    }


    public void NewGame()
    {
        //playerData.Clear(); TODO NOT IMPLEMENTED
        overworldStateData.Clear();
        playerData.Reset();
        StartGame();
    }


    public void StartGame()
    {
        if (currentGameState == GameState.PREGAME)
        {
            LoadLevel("CharacterSelect");
        }
    }

    public void ContinueGame()
    {
        if (currentGameState == GameState.PREGAME)
        {
            
            LoadLevel("OverWorld");
            Debug.LogError("NOT IMPLEMENTED");
            //Load level assets
        }
    }

    public void GameOver()
    {
        UnloadLevel(currentLevelName);
        LoadLevel("GameOverScene");
    }

    public void Victory()
    {
        UnloadLevel(currentLevelName);
        LoadLevel("VictoryScreen");
    }

    public void QuitGame()
    {
        // Implement features for quiting
    }


    public void RestartGame()
    {
        // Clear Data
        overworldStateData.Clear();
        playerData.Reset();
        // Go to pregame state
        UpdateState(GameState.PREGAME);
    }


    void Load()
    {
        // Not implemented
    }


    void Save()
    {
        // Not implemented
    }


    void UpdateState(GameState state)
    {
        GameState prevState = currentGameState;
        currentGameState = state;

        switch (state)
        {
            case GameState.PREGAME:
                break;
            case GameState.RUNNING:
                break;
            case GameState.PAUSED:
                break;
        }
        //dispatch message of state change
        gameStateChangeEvent.Raise(currentGameState, prevState);
        //transition between scenes
    }


    protected override void OnDestroy()
    {
        for (int i = instancedSystemPrefabs.Count - 1; i >= 0; i--)
        {
            Destroy(instancedSystemPrefabs[i]);
        }
        instancedSystemPrefabs.Clear();
        base.OnDestroy();
    }


    void Update()
    {
        if (Input.GetButtonDown("Cancel")) 
        {
            TooglePause();
        }
    }


    void TooglePause()
    {
        UpdateState(currentGameState == GameState.PAUSED ? GameState.RUNNING : GameState.PAUSED);
    }


    public void HandleMainMenuFadeComplete(Events.FadeType fadeType)
    {
        // If fading in. Main menu should cover everything and we can unload the current level
        if (fadeType == Events.FadeType.IN)
        {
            UnloadLevel(currentLevelName);
            UpdateState(GameState.PREGAME);
        }
    }

    public void HandleEncounterSelected(Vertex v)
    {
        EncounterType encounterType = v.type;
        switch (encounterType)
        {
            case EncounterType.NORMAL:
                {
                    UnloadLevel(currentLevelName);
                    LoadLevel("CombatSceneDuncan");
                    break;
                }
            case EncounterType.ELITE:
                {
                    UnloadLevel(currentLevelName);
                    LoadLevel("CombatSceneDuncan");
                    break;
                }
            case EncounterType.SHOP:
                UnloadLevel(currentLevelName);
                LoadLevel("Shop");
                break;
            case EncounterType.CAMP:
                UnloadLevel(currentLevelName);
                LoadLevel("Camp");
                break;
            case EncounterType.RANDOM:
                UnloadLevel(currentLevelName);
                LoadLevel("ChoiceEvent");
                break;
        }
    }

    public void ReturnToOverworld()
    {
        UnloadLevel(currentLevelName);
        LoadLevel("OverWorld");
    }

    public void ReturnToOverworldWithFade()
    {
        FadesManager.Instance.FadeOutThen(ReturnToOverworld);
    }
}
    public enum GameState
{
    PREGAME,
    RUNNING,
    PAUSED
}