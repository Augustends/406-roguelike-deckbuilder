// Code by: Lachlan Muir

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class CardController : Singleton<CardController>
{
	// card events
	public UnityEvent cardDragging;
	public UnityEvent cardDropped;

	// the current active player. obtained from TurnMechanic.
	private PlayerCharacter currentPlayer
	{
		get { return (PlayerCharacter)TurnMechanic.Instance.ActiveCharacter; }
	}

	private List<PlayerCharacter> playerList
	{
		get { return CombatController.Instance.Party; }
	}
		

	// keeps track of the previously active player. used for hiding card containers when turns change.
	private PlayerCharacter previousPlayer;

	protected override void Awake()
	{
		base.Awake();
		cardDragging = new UnityEvent();
		cardDropped = new UnityEvent();
	}

	protected override void OnDestroy()
	{
		base.OnDestroy();
	}

	// Start is called before the first frame update
	void Start()
    {
		// set the previous player to the current player
		previousPlayer = currentPlayer;
	}

	private void Update()
	{
		//StartCoroutine(DebugDeck());
	}

	private IEnumerator DebugDeck()
	{
		yield return new WaitForSeconds(0.5f);
		Debug.Log(CombatController.Instance.Party[1].name + "'s deck update: " + CombatController.Instance.Party[1].PlayerDeck);
	}

	public void hideCardsAtEndOfCombat(PlayerCharacter player)
    {
		hideCardContainers(player);
    }

	private void hideCardContainers(PlayerCharacter player)
	{
		player.PlayerDeck.hide();
		player.PlayerDiscard.hide();
		player.PlayerHand.hide();
		//Debug.Log("Card containers for " + player.name + "hidden.");
	}

	private void unHideCardContainers(PlayerCharacter player)
	{
		player.PlayerDeck.unHide();
		player.PlayerDiscard.unHide();
		player.PlayerHand.unHide();
		//Debug.Log("Card containers for " + player.name + " unhidden.");
	}

	/*
	 * Activated when the event for a new player's turn starting is received by CardController.
	 * 
	 * Handles front end and back end
	 * 
	 * @param: player - the player starting their turn. sent with new player turn event.
	 * @author: Lachlan Muir
	 */
	public void handleNewPlayerTurn(CombatCharacters player)
	{
		//Debug.Log("Handling new player turn for: " + player.name);
		PlayerCharacter castedPlayer = (PlayerCharacter)player;
/*		Debug.Log(castedPlayer.name + "'s deck: " + castedPlayer.PlayerDeck);
		Debug.Log(castedPlayer.name + "'s hand: " + castedPlayer.PlayerHand);
		Debug.Log(castedPlayer.name + "'s discard: " + castedPlayer.PlayerDiscard);*/


		// resolve new player--------------------------------------------------------
		// enable the current player's card containers and their interactions
		unHideCardContainers(castedPlayer);

		// draw new hand for current player
		int draw_amount = 5 + castedPlayer.DrawBoost;
		if (draw_amount < 1) draw_amount = 1; //Minimum of 1 card should be drawn
		drawHand(castedPlayer, draw_amount);

		// update previous player to the current one, so that the new player will be the "previous" player at the end of their turn
		if (previousPlayer != null) {
			//Debug.Log("previous player:" + previousPlayer.name);
		}
		previousPlayer = castedPlayer;
		//Debug.Log("updated previous player:" + previousPlayer.name);
	}

	public void handlePlayerEndOfTurn(CombatCharacters player)
    {
		//Debug.Log("Handling ending player turn for: " + player);
		PlayerCharacter castedPlayer = (PlayerCharacter)player;
		hideCardContainers(castedPlayer);
		discardHand(castedPlayer);
	}

	/**
	 * Draws a card from the top of the deck to the hand
	 * 
	 * Handles front end and back end
	 * 
	 * @author: Lachlan Muir
	 * @param: player - the player whose containers should be manipulated
	 */
	public void drawFromDeck(PlayerCharacter player) 
	{
		Deck curDeck = player.PlayerDeck;
		Hand curHand = player.PlayerHand;
		Discard curDiscard = player.PlayerDiscard;

		//Debug.Log("Drawing Card from " + player.name + "'s deck: " + player.PlayerDeck);
		// if the deck is empty, shuffle the discard pile back into the deck before we draw a card
		if (curDeck.isEmpty())
		{
			resetDeckAndDiscardPiles(player);
			AudioManager.Instance.Play("CardShuffle"); // Plays named sound effect (Simon)
		}

		// get the card from the top of the deck...
		Card card = curDeck.draw();
		// ... and add it to the hand
		if (card != null)
			curHand.addCardToHand(card);

		// front end
		UIControl.Instance.moveCard(card, UIControl.Destination.HAND, player);

	}

	/*
	 * Inserts given card into the deck
	 * @param: card_to_add - card to add to deck
	 * @returns: the card added to the deck
	 * @author: Tanner Skomar
	 */
	public Card insertCardCopyToDeck(PlayerCharacter player, Card card_to_add)
	{
		Deck current_deck_script = player.PlayerDeck;
		Card newCard = current_deck_script.insertCardCopy(card_to_add);
		return newCard;
	}

	/*
	 * Draws a specific card from the deck
	 * @author: Tanner Skomar
	 */
	public void drawSpecificCard(PlayerCharacter player, Card card_to_draw)
    {
		Hand current_hand_script = player.PlayerHand;

		//Debug.Log("Drawing Specific Card");

		current_hand_script.addCardToHand(card_to_draw);

		// front end
		UIControl.Instance.moveCard(card_to_draw, UIControl.Destination.HAND, player);
	}

	public void sendToCurPlayerDiscard(Card card)
	{
		Discard curDiscard = currentPlayer.PlayerDiscard;

	}

	/*
	 * Draws a new hand from the deck - use at start of new player's turn
	 * Handles front end as well through UIControl.
	 * 
	 * @author: Lachlan Muir
	 */
	public void drawHand(PlayerCharacter player, int handSize = 5) 
	{
		drawMultipleFromDeck(player, handSize);
		UIControl.Instance.HandWaitTimer();
		AudioManager.Instance.Play("CardSelect"); // Plays named sound effect (Simon)
	}

	/*
	 * Discards the player's hand to their discard pile.
	 * Handles front end as well through UIControl.
	 * 
	 * @author: Lachlan Muir
	 */
	public void discardHand(PlayerCharacter player)
	{
		// discard the cards from the back-end hand
		//Debug.Log("Hand BEFORE discard: " + player.PlayerHand);
		List<Card> discardedCards = player.PlayerHand.discardAll();
		//Debug.Log("Hand AFTER discard: " + player.PlayerHand);

		// back-end
		//Debug.Log("Discard BEFORE discarding hand: " + player.PlayerDiscard);
		player.PlayerDiscard.appendMultipleCards(discardedCards.ToArray());
		//Debug.Log("Discard AFTER discarding hand: " + player.PlayerDiscard);
		// front-end: removes cards from hand display and adds them to discard display
		foreach (Card card in discardedCards)
		{
			UIControl.Instance.moveCard(card, UIControl.Destination.DISCARD, player);
		}
	}

	public void drawMultipleFromDeck(PlayerCharacter player, int num_cards) 
	{
		for (int i = 0; i < num_cards; i++)
		{
			drawFromDeck(player);
		}
	}

	// reset the deck and discard pile without touching the hand
	public void resetDeckAndDiscardPiles(PlayerCharacter player) 
	{
		Deck curDeck = player.PlayerDeck;
		Discard curDiscard = player.PlayerDiscard;

		// pull all the cards from the discard pile and add them to the deck
		Card[] removedCards = curDiscard.removeAll();
		curDeck.appendMultipleCards(removedCards);

		// send the cards to the deck in the front-end
		UIControl.Instance.moveMultiple(removedCards, UIControl.Destination.DECK, player);

		// shuffle the deck
		curDeck.shuffle();
	}

	/**
	 * Discards a given card from the hand.
	 * 
	 * @author: Lachlan Muir
	 * @param: card - the card to be discarded
	 * @param: player - the player whose card containers should be manipulated
	 */
	public void discardCardFromHand(Card card, PlayerCharacter player)
	{
		// select the card in the hand
		if (!player.PlayerHand.findAndSelectCard(card))
		{
			Debug.LogError("Could not find the card: " + card.name + " in the hand of:" + player.name);
			return;
		}
		// discard the card from the hand
		player.PlayerHand.discardCard();
		// add the card to the discard pile
		player.PlayerDiscard.prependCard(card);
		//reset card's cost to default just in case it was changed at some point.
		card.APCost = card.DefaultCost;

		// front end
		UIControl.Instance.moveCard(card, UIControl.Destination.DISCARD, player);
	}
	 

	/*
	 * Plays the given card. Will move the card from the current player's hand to the current player's discard pile.
	 * Targeting is handled based on the card's targeting type. If the card target type is TARGETED, then the specific target must be passed to 
	 * the target param. The other targeting types are handled automatically.
	 *
	 * @param: card - the card to be played.
	 * @param: target - the target on which the card should be played. Only pass a value to this if the card is TARGETED, and needs to receive the specific target it was played on.
	 */
	public void playCard(Card card, CombatCharacters target = null)
	{
		//Debug.Log("Card played.");
		//check if the player has enough AP to play the card. and that energy is available if required. If not, cancel the play.
		if(currentPlayer.AP < card.APCost)
        {
			Debug.Log("Not enough AP!");
			CantPlay(card);
			AudioManager.Instance.Play("Locked"); // Plays named sound effect (Simon)
			return;
        } else if (!card.isEnergyPlayable()) //stops REQUIRED energy cards from being played with insufficient energy
        {
			Debug.Log("Not enough Energy!");
			CantPlay(card);
			AudioManager.Instance.Play("Locked"); // Plays named sound effect (Simon)
			return;
		} else if(target != null)
        {
			if(target.character_type == Character_Type.ENEMY && card.TargetType == Card.CardTargetType.SINGLE_PLAYER)
            {//Trying to target an enemy with a player only card
				Debug.Log("Players Only!");
				CantPlay(card);
				AudioManager.Instance.Play("Locked"); // Plays named sound effect (Simon)
				return;
			} else if (target.character_type == Character_Type.PLAYER && card.TargetType == Card.CardTargetType.SINGLE_ENEMY)
			{//Trying to target an player with a enemy only card
				Debug.Log("Enemies Only!");
				CantPlay(card);
				AudioManager.Instance.Play("Locked"); // Plays named sound effect (Simon)
				return;
			}
        }

		// get the targets by grabbing all the characters and enemies from the scene - GET THIS FROM COMBAT CONTROLLER
		CombatCharacters[] party_targets = CombatController.Instance.Party.ToArray();
		CombatCharacters[] enemy_targets = CombatController.Instance.Enemies.ToArray();
		//CombatCharacters[] all_targets = new CombatCharacters[party_targets.Length + enemy_targets.Length];
		CombatCharacters[] all_targets = CombatController.Instance.AllCharacters.ToArray();
		// copy the players and enemies to the set of available targets
		Array.Copy(party_targets, all_targets, party_targets.Length);
		Array.Copy(enemy_targets, 0, all_targets, party_targets.Length, enemy_targets.Length);

		// create an empty array. If successfully obtained targets in switch case, this empty array will be replaced with a non-empty array.
		CombatCharacters[] targets = { };

		switch (card.TargetType) 
		{
			case Card.CardTargetType.ALL:
				targets = all_targets;
				currentPlayer.AttackTrigger();
				break;
			case Card.CardTargetType.ALLIES:
				targets = party_targets;
				currentPlayer.AttackTrigger();
				break;
			case Card.CardTargetType.ENEMIES:
				targets = enemy_targets;
				currentPlayer.AttackTrigger();
				break;
			case Card.CardTargetType.SELF:
				targets = new CombatCharacters[] { currentPlayer };
				currentPlayer.AttackTrigger();
				break;
			case Card.CardTargetType.TARGETED:
				if (target != null)
				{
					targets = new CombatCharacters[] { target };
					currentPlayer.AttackTrigger();
				}
				break;
			case Card.CardTargetType.SINGLE_PLAYER:
				if (target != null)
				{
					targets = new CombatCharacters[] { target };
					currentPlayer.AttackTrigger();
				}
				break;
			case Card.CardTargetType.SINGLE_ENEMY:
				if (target != null)
				{
					targets = new CombatCharacters[] { target };
					currentPlayer.AttackTrigger();
				}
				break;
			default:
				Debug.LogError("Card does not have a valid target type.");
				break;
		}

		if (targets.Length != 0)
		{
			card.activate(targets);
			//Debug.Log("Card activated.");
			//If the card is played, remove it's cost from the current player's AP
			currentPlayer.ModifyAP(-card.APCost);

			discardCardFromHand(card, currentPlayer);
		}
		else
		{
			CantPlay(card);
			Debug.LogError("Failed to obtain targets for card of type: " + card.TargetType);
		}
	}

	/*Cancels the card trying to be played in case it is missing some requirement (Like the cost)
	 */ 
	/*
	 * Handles the target selection phase of playing a card.
	 * @param: card_script - the card script attached to the played card object
	 * @param: all_targets - all the available targets
	 * @param: effect_index - for recursively iterating over array of card effects. Pass 0 to start at beginning of effects.
	 * @author: Lachlan Muir
	 */
	//IEnumerator TargetSelection(Card card_script, CombatCharacters[] all_targets, int effect_index)
	//{
	//	// helper function for applying the effect to the selected targets and moving on to the next effect
	//	void goNextEffect(CombatCharacters[] targs)
	//	{
	//		// apply the effects to the targeted characters
	//		card_script.effects[effect_index].setTargets(targs);
	//		// move to next card effect, if there is another remaining
	//		if (effect_index + 1 <= card_script.effects.Length - 1)
	//			StartCoroutine(TargetSelection(card_script, all_targets, effect_index + 1));
	//	}
		
	//	// start a list to keep track of the selected targets
	//	List<CombatCharacters> targets_selected = new List<CombatCharacters>();
	//	// while the num_targets_selected is less than the number of targets we can select for the current effect
	//	while (targets_selected.Count < card_script.effects[effect_index].getMaxTargets() && targets_selected.Count < all_targets.Length) 
	//	{
			
	//		if (Input.GetMouseButtonDown(0))
	//		{
	//			RaycastHit2D hit = Physics2D.Raycast(Camera.main.transform.position, Input.mousePosition);
	//			if (hit.collider != null)
	//			{
	//				GameObject hit_object = hit.transform.gameObject;
	//				// if the clicked object is tagged as a player or enemy, it is in the array of valid targets, and is not in the list of selected targets...
	//				if (hit_object.tag == "Player" || hit_object.tag == "Enemy")
	//				{
	//					CombatCharacters hit_char_script = hit_object.GetComponent<CombatCharacters>();
	//					if (all_targets.Contains(hit_char_script)) 
	//					{
	//						if (!targets_selected.Contains(hit_char_script))
	//						{
	//							// ... then add it to the list of selected targets
	//							Debug.Log(hit_char_script.gameObject.name + "added to targets");
	//							targets_selected.Add(hit_char_script);
	//						}
	//						else Debug.Log("Target already selected.");
	//					}
	//					else Debug.Log("Invalid target clicked.");
	//				}
	//				else if (hit_object.tag == "EndSelection")
	//				{
	//					goNextEffect(targets_selected.ToArray());
	//					yield break;
	//				}
	//			}
	//			// reset the raycast hit
	//			hit = new RaycastHit2D();
	//		}
	//		yield return null;
	//	}
	//	goNextEffect(targets_selected.ToArray());
	//}


	/*Cancels the card trying to be played in case it is missing some requirement (Like the cost), returns the card to the hand
	 */ 
	public void CantPlay(Card card)
    {
		//UIControl.Instance.ReturnToHand(card.gameObject);
		UIControl.Instance.UpdateCardHand();
    }
}
