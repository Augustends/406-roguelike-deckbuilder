using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaveScene : MonoBehaviour
{
    public void ReturnToOverworld()
    {
        AudioManager.Instance.Play("Button"); // Plays named sound effect (Simon)
        GameObject.Find("GameManager").GetComponent<GameController>().ReturnToOverworldWithFade();
    }
}
