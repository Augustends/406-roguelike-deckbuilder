using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RRR : MonoBehaviour
{
    public ChooseCardFromDeckDisplay chooseFromDeck;
    public enum RRRtype{ REDUCE,REUSE,RECYCLE,UNSELECTED }
    public RRRtype choice = RRRtype.UNSELECTED;
    [SerializeField]bool done = false;
    [SerializeField] PlayerData playerData;
    [SerializeField] Text resultText;
    [SerializeField] Button top_button;

    public void activateReuse()
    {
        chooseFromDeck.gameObject.SetActive(true);
        choice = RRRtype.REUSE;
    }

    public void activateRecycle()
    {
        chooseFromDeck.gameObject.SetActive(true);
        choice = RRRtype.RECYCLE;
    }

    public void activateReduce()
    {
        chooseFromDeck.gameObject.SetActive(true);
        choice = RRRtype.REDUCE;
    }

    private void Update()
    {
        if (!done && chooseFromDeck.confirmedSelection)
        {
            // Find a matched card in player data character decks that matches the selected instance in scene
            string nameOfCard = chooseFromDeck.currentCardSelected.Card_name;
            CharacterData matchedCharacter = null;
            Card match = null;
            foreach (var character in playerData.party_members)
            {
                foreach (var card in character.character_deck)
                {
                    if (card.Card_name.CompareTo(nameOfCard)==0)
                    {
                        //found matched card in data of character deck
                        match = card;
                        matchedCharacter = character;
                        break;
                    }
                }
                if (match != null)
                {
                    break;
                }
            }

            // Sanity check
            if (match == null || matchedCharacter == null)
            {
                Debug.LogWarning("Should have a match for the selected card, but do not");
            }
            

            switch (choice)
            {
                case RRRtype.REDUCE:
                    matchedCharacter.RemoveCard(match);
                    resultText.text = match.Card_name + " was removed!";
                    Destroy(chooseFromDeck.currentCardSelected.gameObject);
                    done = true;
                    break;
                case RRRtype.REUSE:
                    matchedCharacter.AddCard(match);
                    GameObject duped =  Instantiate(chooseFromDeck.currentCardSelected.gameObject);
                    duped.transform.position = chooseFromDeck.currentCardSelected.transform.position + Vector3.right * 0.5f;
                    resultText.text = match.Card_name + " was duplicated!";
                    done = true;
                    break;
                case RRRtype.RECYCLE:
                    matchedCharacter.RemoveCard(match);
                    Card newCard = matchedCharacter.full_card_list[UnityEngine.Random.Range(0, matchedCharacter.full_card_list.Count)];
                    resultText.text = match.Card_name + " was recyled to : " + newCard.Card_name+ "!"; 
                    Destroy(chooseFromDeck.currentCardSelected.gameObject);
                    GameObject recycled = Instantiate(newCard.gameObject);
                    matchedCharacter.AddCard(newCard);
                    done = true;
                    break;
                case RRRtype.UNSELECTED:
                    break;
                default:
                    break;
            }

            top_button.GetComponentInChildren<TMP_Text>().text = "=>";
            top_button.onClick.AddListener(delegate { Done(); });
        }
    }

    private void Done()
    {
        AudioManager.Instance.Play("Button"); // Plays named sound effect (Simon)
        GameController.Instance.ReturnToOverworldWithFade();
    }
}
