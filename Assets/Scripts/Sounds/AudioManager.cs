using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

// Tracks what audio files are being played
// Only one manager is active at a time
// Author Simon Schmidt

public class AudioManager : Singleton<AudioManager>
{

    public Sound[] sounds;
    private string currentSong;
    private bool paused = false;

    // Use this for Initialization. 
    void Awake()
    {
        base.Awake();
        if (sounds==null)
        {
            sounds = new Sound[0];
            Debug.LogWarning("Sounds list is empty");
        }
        DontDestroyOnLoad(gameObject);
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    void Start()
    {
        PlaySong("MainTheme");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            StopPlaying(currentSong);
            PlaySong("MainTheme");
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            StopPlaying(currentSong);
            PlaySong("ForestTheme");
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            StopPlaying(currentSong);
            PlaySong("FarmTheme");
        }

        if (Input.GetKeyDown(KeyCode.V))
        {
            StopPlaying(currentSong);
            PlaySong("CombatTheme");
        }
        
        if (Input.GetKeyDown(KeyCode.B))
        {
            StopPlaying(currentSong);
            PlaySong("CityTheme");
        }

        if (Input.GetKeyDown(KeyCode.N))
        {
            StopPlaying(currentSong);
            PlaySong("FinalBossTheme");
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            if (!paused)
            {
                PauseTrack(currentSong);
            }
            else
            {
                ResumeTrack(currentSong);
            }

        }
    }

    public void Play(string name)
    {
        if (sounds.Length == 0)
        {
            Debug.LogWarning("Sounds list is empty");
            return;
        }
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not Found!");
            return;
        }
        s.source.Play();
    }

    public void PlaySong(string name)
    {
        if (sounds.Length == 0)
        {
            Debug.LogWarning("Sounds list is empty");
            return;
        }
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not Found!");
            return;
        }
        currentSong = name;
        // Debug.Log("Song Playing " + name);
        s.source.Play();
    }

    public string GetCurrentSong()
    {
        return (currentSong);
    }

    public void StopPlaying(string name)
    {
        if (sounds.Length == 0)
        {
            Debug.LogWarning("Sounds list is empty");
            return;
        }
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not Found!");
            return;
        }

        s.source.Stop();
    }

    private void PauseTrack(string name)
    {
        if (sounds.Length == 0)
        {
            Debug.LogWarning("Sounds list is empty");
            return;
        }
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not Found!");
            return;
        }
        //Debug.Log("Song Paused");
        paused = true;
        s.source.pitch = 0f;
    }

    private void ResumeTrack(string name)
    {
        if (sounds.Length == 0)
        {
            Debug.LogWarning("Sounds list is empty");
            return;
        }
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not Found!");
            return;
        }
        //Debug.Log("Song Resumed"):
        paused = false;
        s.source.pitch = 1f;
    }


}
