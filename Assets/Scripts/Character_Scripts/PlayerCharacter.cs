using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter: CombatCharacters
{

    [SerializeField]
	private GameObject prefab_deck;
	[SerializeField]
	private GameObject prefab_discard;
	[SerializeField]
	private GameObject prefab_hand;
    [SerializeField]

    private CharacterData character_data;
	private Deck deck;
	private Hand hand;
	private Discard discard;

    public CharacterData CharacterData { get => character_data; }
    public Deck PlayerDeck { get => deck; }
	public Hand PlayerHand { get => hand; }
	public Discard PlayerDiscard { get => discard; }

    //Current AP
	public int AP = 0;
    //Default AP amount
    public int StartAP = 3;
    //How much exta AP is added next turn
    public int APBoost;
    [SerializeField] CombatCharacterGameEvent startPlayerTurnEvent;
    [SerializeField] CombatCharacterGameEvent endPlayerTurnEvent;

    public int DrawBoost = 0; //Amount of cards to modify start of turn draw by


    /*This can be used to determine if the card is being used on the player
     * @param: col - thing that collided with the trigger
     * Placeholder for now
    */
    private void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log("Collide With Player");
    }


    /*This can be used to determine if the card is being used on the player
     * @param: col - thing that collided with the trigger
     * Placeholder for now
    */
    private void OnTriggerStay2D(Collider2D col)
    {
        Debug.Log("Staying on Player");
    }

    public void AttackTrigger()
    {
        transform.Find(character_data.character_name + "Rigged").GetComponent<Animator>().SetTrigger("Attack");
        Invoke("MoveForwardForAttack", 0.2f);
    }

    void MoveForwardForAttack()
    {
        transform.Translate(Vector3.right * 0.7f);
        Invoke("MoveBackwardToIdle", 0.75f);
    }
    void MoveBackwardToIdle()
    {
        transform.Translate(-Vector3.right * 0.7f);
    }
    public void Init(CharacterData characterData)
    {
		health.setMaxHealth(characterData.max_health);
        health.setCurrentHealth(characterData.cur_health);

        // initialize card containers and hide them
        // deck - positioned and anchored in bottom right
        if (prefab_deck != null)
		{
			deck = GenCardContainer(prefab_deck, new Vector3(0, 0, 0), new Vector2(-0.5f, 0), true).GetComponent<Deck>();
			if (deck == null)
				Debug.LogError("Failed to generate player deck - no deck component found on instantiated object.");
			deck.gameObject.name = gameObject.name + "'s Deck";
			deck.owner = this;
			deck.Init(characterData.character_deck.ToArray());
		}
		else
			Debug.LogError("Error generating deck container for: " + gameObject.name + ". Missing deck prefab.");

		// hand - positioned and anchored in bottom middle
		if (prefab_hand != null)
		{
			hand = GenCardContainer(prefab_hand, new Vector3(0, 0, 0), new Vector2(0.5f, 0), true).GetComponent<Hand>();
			hand.gameObject.name = gameObject.name + "'s Hand";
		}
		else
			Debug.LogError("Error generating hand container for: " + gameObject.name + ". Missing hand prefab.");

		// discard - positioned and anchored in bottom left
		if (prefab_discard != null)
		{
			discard = GenCardContainer(prefab_discard, new Vector3(0, 0, 0), new Vector2(1.5f, 0), true).GetComponent<Discard>();
			discard.gameObject.name = gameObject.name + "'s Discard";
		}
		else
			Debug.LogError("Error generating discard container for: " + gameObject.name + ". Missing discard prefab.");
	}

        
    public override void takeTurn()
    {
        base.takeTurn();
        //Debug.Log(name+ "'s Turn: Remaining AP:" + AP);
        AP = StartAP + APBoost;
        startPlayerTurnEvent.Raise(this); // Raise new player turn event
    }


    /* Place holder function so the player can tell the battlesystem that their turn is over.
     * This will be handled elsewhere but that UI does not exist yet
     * @author: Tanner Skomar
    */
    public override void endTurn()
    {
        base.endTurn();
        endPlayerTurnEvent.Raise(this);
    }


    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start(); //Call combat character Start()
        character_type = Character_Type.PLAYER; //Set Character Type

	}

	/*
	 * Generates a card container using the provided prefab. Anchors the container in the UI.
	 * @param: prefab - the prefab to instantiate
	 * @param: position - the position to instantiate the container in
	 * @param: anchor - the UI anchor
	 * @param: hidden - whether the container should start hidden
	 * @return: the instantiated GameObject
	 * 
	 * @author: Lachlan Muir
	 */
	private GameObject GenCardContainer(GameObject prefab, Vector3 position, Vector2 anchor, bool hidden)
	{
		if (prefab == null)
		{
			Debug.LogError("prefab missing");
			return null;
		}
		RectTransform trans = Instantiate(prefab, position, Quaternion.identity).GetComponent<RectTransform>();
		// set position again to be relative to parent
		trans.localPosition = position;

		trans.anchorMin = anchor;
		trans.anchorMax = anchor;
		trans.pivot = anchor;

		if (hidden)
			trans.gameObject.GetComponent<IHideableCContainer>().hide();

		if (trans.gameObject == null)
			Debug.LogError("GenCardContainer did not produce a gameobject");

		trans.SetParent(UIControl.Instance.transform, false);

		return trans.gameObject;
	}

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        // Order of checks here matter
        if (!active)
        {
            return;
        }
    }

    /*
     * Changes the AP amount that this character will have on their next turn
     * @param: value - the amount of AP you want to be added to the next turn
     * @author: Alex
     */
    public void BoostAP(int value)
    {
        APBoost += value;
    }

    /*
    * Changes the draw amount that this character will have on their next turn
    * @param: value - the amount of AP you want to be added to the next turn
    * @author: Tanner Skomar
    */
    public void BoostDraw(int value)
    {
        DrawBoost += value;
    }

    /*
     * Changes the current AP by the given Amount
     * @param: value - the amount of AP to add
     * @author: Tanner Skomar
     */
    public void ModifyAP(int value)
    {
        AP += value;
        GameObject.Find("APdisplayImage").GetComponent<DisplayAP>().ChangeDisplay();
    }
}
