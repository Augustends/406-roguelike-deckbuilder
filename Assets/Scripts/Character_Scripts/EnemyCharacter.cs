using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyCharacter : CombatCharacters
{
    [SerializeField]
    private GameObject FloatingTextActionPrefab;
    [SerializeField]
    private string enemyName;
    public string EnemyName { get => enemyName; }

    private EnemyAI my_brain;
    public int CR; // Monster challenge Rating


    /*This can be used to determine if the card is being used on the enemy
     * @param: col - thing that collided with the trigger
     * Placeholder for now
    */
    private void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log("Collide With" + name);
    }

    /*This can be used to determine if the card is being used on the enemy
     * @param: col - thing that collided with the trigger
     * Placeholder for now
    */
    private void OnTriggerStay2D(Collider2D col)
    {
        Debug.Log("Staying on" + name);
    }

    /*
     * Causes actions that are planned to target a single player to target the
     * given combat character instead 
     */
    public void tauntEnemy(CombatCharacters new_target)
    {
        my_brain.tauntEnemy(new_target);
    }


    /*
     * Temporary function for enemy to take its turn. Will be updated later
     * Currently Enemy Decides on a random action on it's turn and then does it.
     * @author: Tanner Skomar
     */
    public override void takeTurn()
    {
        base.takeTurn();

        if (health.getCurrentHealth() > 0)//only start action if still alive after start of turn effects
        {
            if (my_brain.next_action.Item1 == null) //decide action on first turn
            {
                my_brain.decideNextAction();
            }
            //showActionFloatText(my_brain.next_action.Item1.name);//Show Action float text

            my_brain.takeAction();

            if (health.getCurrentHealth() > 0)//Combat COntroller will end their turn for them is they die
            {
                Invoke("endTurn", 1.8f);
            }
        }
    }

    protected void Awake()
    {
        my_brain = GetComponent<EnemyAI>(); //Get refence to my combat AI
    }

    // Start is called before the first frame update
    // @author: Tanner Skomar
    protected override void Start()
    {
        base.Start();//Call combat character Start()
        character_type = Character_Type.ENEMY; //Set Character Type
    }

    // Update is called once per frame
    // @author: Tanner Skomar
    protected override void Update()
    {
        base.Update();

    }

    public void showActionFloatText(string new_text)
    {
        if (FloatingTextActionPrefab && new_text != "")
        {
            showFloatingText(new_text, FloatingTextActionPrefab);
        }
    }
}
