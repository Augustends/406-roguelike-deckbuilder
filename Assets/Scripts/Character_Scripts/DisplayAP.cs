using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using UnityEngine.UI;

public class DisplayAP : MonoBehaviour
{
	public TextMeshProUGUI APtext;
	private PlayerCharacter currentPlayer;

    public void OnTurnStart(CombatCharacters character)
    {
		currentPlayer = (PlayerCharacter)TurnMechanic.Instance.ActiveCharacter;
		GameObject.Find("EndTurnGlow").GetComponent<Image>().enabled = false;
		ChangeDisplay();
	}
	public void ChangeDisplay()
    {
		APtext.text = currentPlayer.AP.ToString();
		if(currentPlayer.AP <= 0)
        {
			GameObject.Find("EndTurnGlow").GetComponent<Image>().enabled = true;
		}
	}
}
