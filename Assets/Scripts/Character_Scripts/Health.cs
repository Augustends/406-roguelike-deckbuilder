using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Health : MonoBehaviour
{
    [SerializeField]
    private int max_health;
    [SerializeField]
    private int current_health;

    [SerializeField]
    private HealthBar health_bar;
    [SerializeField]
    private GameObject health_text_object;
    [SerializeField]
    private GameObject armour_text_object;
    private GameObject armour_icon; // gameobject to toggle visibility of armor when there is none

    private TextMeshProUGUI health_text;
    private TextMeshProUGUI armour_text;

    [SerializeField] //For testing becuase no armour ui yet
    protected int armour = 0;
    //protected const int ARMOUR_CAP = 30; //Do we even need a cap?

    [SerializeField]
    private GameObject FloatingTextHealthPrefab;

    /*
     * Getter for max_health
     * @return: int value of max_health 
     * @author: Tanner Skomar
     */
    public int getMaxHealth()
    {
        return max_health;
    }

    /*
     * Setter for max_health
     * @param: health - the value to set max_health to
     * @return: void
     * @author: Tanner Skomar
     */
    public void setMaxHealth(int health)
    {
        max_health = health;
        updateHealthText();
    }

    /*
     * Setter for current_health
     * @param: health - the value to set current_health to
     * @return: void
     * @author: Tanner Skomar
     */
    public void setCurrentHealth(int health)
    {
        current_health = health;
        health_bar.setHealth(health);
        updateHealthText();
    }

    /*
     * Getter for current_health
     * @return: int value of current_health 
     * @author: Tanner Skomar
     */
    public int getCurrentHealth()
    {
        return current_health;
    }

    /*
     * Setter for armour
     * @param: new_armour - the value to set armour to
     * @return: void
     * @author: Tanner Skomar
     */
    public void setCurrentArmour(int new_armour)
    {
        armour = new_armour;
        if (armour > 0)
        {
            armour_icon.SetActive(true);
            updateArmourText();
        }
        else
        {
            armour_icon.SetActive(false);
        }
    }

    /*
     * Getter for armour
     * @return: int value of armour 
     * @author: Tanner Skomar
     */
    public int getCurrentArmour()
    {
        return armour;
    }

    /*
     * Increases armour by given amount
     * @param: amount - the amount of armour to add
     * @return: void
     * @author: Tanner Skomar
     */
    public void addArmour(int amount)
    {
        armour += amount;
        armour_icon.SetActive(true);
        updateArmourText();
        updateHealthText(amount, true);
    }

    /*
     * Reduces health by the given amount
     * Will not cause current_health to go below 0
     * Damage is first subtracted from armour before affecting current_health
     * @param: amount - the amount of damage current_health should take
     * @return: void
     * @author: Tanner Skomar
     */
    public void damage(int amount)
    {
        int remaining_armour = armour;
        remaining_armour -= amount;

        if (remaining_armour <= 0) //Damage broke through armour and should be subtracted from health
        {
            armour = 0; //Armour was fully depleted
            armour_icon.SetActive(false);

            current_health += remaining_armour; //add remaing armour (a negative #) to health
            if (current_health < 0)
            {
                current_health = 0;
            }
            health_bar.setHealth(current_health);
        }
        else if (remaining_armour == 0)
        {
            armour = 0;
            armour_icon.SetActive(false);
        }
        else //Damage did not break through armour
        {
            armour = remaining_armour;
        }
        updateHealthText(-amount);
        updateArmourText();
    }

    /*
     * Increases health by the given amount
     * Will not cause current_health to go above max_health
     * @param: amount - the amount of healing to be applied to current_health
     * @return: void
     * @author: Tanner Skomar
     */
    public void heal(int amount)
    {
        current_health += amount;
        if (current_health > max_health)
        {
            current_health = max_health;
        }
        health_bar.setHealth(current_health);
        updateHealthText(amount);
    }

    /* Updates the text element representing the health
     * @param: health_change - the change in health to be displayed in floating text
     * @author: Travis Baldwin
     */
    private void updateHealthText(int health_change = 0, bool addingArmour = false)
    {
        health_text.text = current_health.ToString() + "/" + max_health.ToString();

        if(health_change != 0)//If health changed do the health floating text
        {
            showFloatingText(health_change, addingArmour);
        }
    }

    /* Updates the text element representing the armour
    * @author: Travis Baldwin
    */
    private void updateArmourText()
    {
        armour_text.text = armour.ToString();
    }

    /* Spawns floating text showing the change in health
     * @param: amount - the amount to show as floating text
     * @param: adding armour - true if you want the text to be blue because armour was added
     * @author: Tanner Skomar
     */
    private void showFloatingText(int amount, bool addingArmour = false)
    {
        if (FloatingTextHealthPrefab)
        {


            var float_text = Instantiate(FloatingTextHealthPrefab, new Vector4(transform.position.x, transform.position.y, -3), Quaternion.identity, gameObject.transform);
            var text_mesh_comp = float_text.transform.GetChild(0).GetComponent<TextMeshPro>();
            if (addingArmour)
            {
                text_mesh_comp.color = new Color32(0, 255, 255, 255);
                if (amount > 0) { text_mesh_comp.text = "+" + amount.ToString(); }
                else { text_mesh_comp.text = amount.ToString(); }
            }  else
            {
                if (amount > 0) 
                {
                    text_mesh_comp.color = new Color32(0, 255, 0, 255);
                    text_mesh_comp.text = "+" + amount.ToString();
                }
                else 
                {
                    text_mesh_comp.color = new Color32(255, 0, 0, 255);
                    text_mesh_comp.text = amount.ToString(); 
                }
            }
            
        }
    }

    private void Awake()
    {
        current_health = max_health; //Default current_health to max_health
        health_text = health_text_object.GetComponent<TextMeshProUGUI>();
        armour_text = armour_text_object.GetComponent<TextMeshProUGUI>();
        armour_icon = armour_text_object.transform.parent.gameObject;
        armour_icon.SetActive(false);
        health_text.text = max_health.ToString() + "/" + max_health.ToString();
    }

    // Start is called before the first frame update
    void Start()
    {
        health_bar.setMaxHealth(max_health); //Default health_bar to full
    }

    // Update is called once per frame
    void Update()
    {
    }
}
