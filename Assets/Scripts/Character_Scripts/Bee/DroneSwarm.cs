using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneSwarm : BeeSwarm
{
    /*
    * Add 3 armour to host
    * Remove passive defense buff
    * 
    * @author Tanner Skomar
    */
    public override void signalEffect()
    {
        if(amount_of_bees > 0)//Prevents 0 armour from showing up 
        {
            host_character.increaseArmour(amount_of_bees * 3);
        }
    }

    /*

    * Remove passive defense buff
    * then do normal reset
    * 
    * @author Tanner Skomar
    */
    public override void resetSwarm()
    {
        host_character.modifyDefense(-effect_stacks * effect_amount);//Remove the defense bonus
        base.resetSwarm();
    }

    /*
     * Updates the modifier being applied to hosts defense mod 
     * if required amount of bees has been reached
     * @author Tanner Skomar
     */
    public override void updatePassiveEffect()
    {
        double new_stack_amount = System.Math.Floor(amount_of_bees / stack_size);
        if (new_stack_amount > effect_stacks)
        {
            effect_stacks = (int)new_stack_amount;
            host_character.modifyDefense(effect_amount);//Add effect amount to host defense mod
        }
    }
}
