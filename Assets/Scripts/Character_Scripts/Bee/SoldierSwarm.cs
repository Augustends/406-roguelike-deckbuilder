using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierSwarm : BeeSwarm
{
    int soldier_damage = 5; //the base dealt per soldier when activated

    /*
    * Deal 1 damage for every stack_size bees in swarm
    * 
    * @author Tanner Skomar
    */
    public override void activateContinuous()
    {
        host_character.takeDamage(effect_stacks * effect_amount);
    }

    /*
    * Deal soldier_damage to host damage for
    * each bee in swarm
    * 
    * @author Tanner Skomar
    */
    public override void signalEffect()
    {
        host_character.takeDamage(amount_of_bees * soldier_damage);
    }

    /*
     * Updates the number of stacks to multiply damage at end of turn by
     * if required amount of bees has been reached
     * @author Tanner Skomar
     */
    public override void updatePassiveEffect()
    {
        double new_stack_amount = System.Math.Floor(amount_of_bees / stack_size);
        if (new_stack_amount > effect_stacks)
        {
            effect_stacks = (int)new_stack_amount;
        }
    }
}
