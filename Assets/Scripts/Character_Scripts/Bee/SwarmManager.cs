using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum SwarmType { ALL, DRONE, SOLDIER, WORKER };

public class SwarmManager : MonoBehaviour
{
    [SerializeField]
    private CombatCharacters character;

    private BeeSwarm drone_swarm;
    private BeeSwarm soldier_swarm;
    private BeeSwarm worker_swarm;


    //UI
    private TextMeshProUGUI drone_ui_value;
    private TextMeshProUGUI soldier_ui_value;
    private TextMeshProUGUI worker_ui_value;

    //private GameObject swarm_display;

    private GameObject drone_display;
    private GameObject soldier_display;
    private GameObject worker_display;


    private void Awake()
    {
        drone_swarm = new DroneSwarm();
        drone_swarm.setCharacter(character);
        soldier_swarm = new SoldierSwarm();
        soldier_swarm.setCharacter(character);
        worker_swarm = new WorkerSwarm();
        worker_swarm.setCharacter(character);
    }


    // Start is called before the first frame update
    void Start()
    {
        //UI could be done like Energy Manager maybe
        // Assign TextMeshProUGUI references
        drone_ui_value = transform.Find("Swarm Display").Find("Drone").GetChild(0).GetComponent<TextMeshProUGUI>();
        soldier_ui_value = transform.Find("Swarm Display").Find("Soldier").GetChild(0).GetComponent<TextMeshProUGUI>();
        worker_ui_value = transform.Find("Swarm Display").Find("Worker").GetChild(0).GetComponent<TextMeshProUGUI>();

        //swarm_display = transform.Find("Swarm Display").gameObject;
        drone_display = transform.Find("Swarm Display").Find("Drone").gameObject;
        soldier_display = transform.Find("Swarm Display").Find("Soldier").gameObject;
        worker_display = transform.Find("Swarm Display").Find("Worker").gameObject;

        updateSwarmUi();
    }

    /*Checks if the UI was found in the scene, If it's not their use debug message instead
     * @return: true if requred TextMeshProUGUI could be found else false
     * @author: Tanner Skomar
     */
    private bool uiFound()
    {
        if (drone_ui_value != null && soldier_ui_value != null && worker_ui_value != null)
        {
            return true;
        }
       
        return false;
    }

    /*Return the amount of bees in the swarm pf the given type*/
    public int getBeeAmount(SwarmType swarm_type)
    {
        switch (swarm_type)
        {
            case SwarmType.DRONE:
                return drone_swarm.amount_of_bees;
            case SwarmType.SOLDIER:
                return soldier_swarm.amount_of_bees;
            case SwarmType.WORKER:
                return worker_swarm.amount_of_bees;
            case SwarmType.ALL:
            default:
                return drone_swarm.amount_of_bees + soldier_swarm.amount_of_bees + worker_swarm.amount_of_bees;
        }
    }

    /*
     * Updates swarm ui 
     * 
     * If no ui exists it will print a debug message instead
     */
    public void updateSwarmUi()
    {
        if (uiFound())
        {
            //TODO hide swarms ui when 0 bees are in it
            drone_ui_value.text = "x" + drone_swarm.amount_of_bees.ToString();
            soldier_ui_value.text = "x" + soldier_swarm.amount_of_bees.ToString();
            worker_ui_value.text = "x" + worker_swarm.amount_of_bees.ToString();
            showOrHideBees();
        } else
        {
            Debug.Log(name + " Drones : " + drone_swarm.amount_of_bees + " Soldiers: " + soldier_swarm.amount_of_bees + " Workers: " + worker_swarm.amount_of_bees);
        }
    }

    /*Hide or show the different bee types individually*/
    public void showOrHideBees()
    {
        if(drone_swarm.amount_of_bees > 0)
        {
            drone_display.SetActive(true);
        } else
        {
            drone_display.SetActive(false);
        }

        if (soldier_swarm.amount_of_bees > 0)
        {
            soldier_display.SetActive(true);
        }
        else
        {
            soldier_display.SetActive(false);
        }

        if (worker_swarm.amount_of_bees > 0)
        {
            worker_display.SetActive(true);
        }
        else
        {
            worker_display.SetActive(false);
        }
    }


    /*Return true if at least one of the bee types is active
     * checked to see if UI should be shown
     */
    public bool hasBees()
    {
        return (drone_swarm.amount_of_bees > 0 || soldier_swarm.amount_of_bees > 0 || soldier_swarm.amount_of_bees > 0);
    }



    /*
     * Signals the given swarmtype amount times
     * @param swarm: swarm to signal
     * @param amount: amount of times to activate effect
     * @author Tanner Skomar
     */
    public void signalSwarm(SwarmType swarm, int amount)
    {
        switch (swarm)
        {
            case SwarmType.DRONE:
                drone_swarm.signal(amount);
                break;
            case SwarmType.SOLDIER:
                soldier_swarm.signal(amount);
                break;
            case SwarmType.WORKER:
                worker_swarm.signal(amount);
                break;
            case SwarmType.ALL:
            default:
                drone_swarm.signal(amount);
                soldier_swarm.signal(amount);
                worker_swarm.signal(amount);
                break;
        }
        updateSwarmUi();
    }

    /*
     * Sends the given amount of bees to the given swarm type
     * @param swarm: swarm to send bees to 
     * @param amount: amount to send
     * @author Tanner Skomar
     */
    public void sendToSwarm(SwarmType swarm, int amount)
    {
        switch (swarm)
        {
            case SwarmType.DRONE:
                drone_swarm.increaseBees(amount);
                break;
            case SwarmType.SOLDIER:
                soldier_swarm.increaseBees(amount);
                break;
            case SwarmType.WORKER:
                worker_swarm.increaseBees(amount);
                break;
            case SwarmType.ALL:
            default:
                drone_swarm.increaseBees(amount);
                soldier_swarm.increaseBees(amount);
                worker_swarm.increaseBees(amount);
                break;
        }
        updateSwarmUi();
    }

    /*
     * Increase existing bee swarms by amount
     * @param amount: amount to increase by
     * @author Tanner Skomar
     */
    public void increaseExistsingBees(int amount)
    {
        if (drone_swarm.amount_of_bees > 0)
        {
            drone_swarm.increaseBees(amount);
        }
        if (soldier_swarm.amount_of_bees > 0)
        {
            soldier_swarm.increaseBees(amount);
        }
        if (worker_swarm.amount_of_bees > 0)
        {
            worker_swarm.increaseBees(amount);
        }
        updateSwarmUi();
    }

    /**
     * Doubles the amount of any existing bees in the swarm
     * 
     * @Auth: Ryan Larsen
     * 
     */
    public void doubleExistingBees()
    {
        if (drone_swarm.amount_of_bees > 0)
        {
            drone_swarm.increaseBees(drone_swarm.amount_of_bees);
        }
        if (soldier_swarm.amount_of_bees > 0)
        {
            soldier_swarm.increaseBees(soldier_swarm.amount_of_bees);
        }
        if (worker_swarm.amount_of_bees > 0)
        {
            worker_swarm.increaseBees(worker_swarm.amount_of_bees);
        }
        updateSwarmUi();
    }

    /*
     * Called at the end of turn to activate continuous effects of the swarm
     * @author Tanner Skomar
     */
    public void activateContinuous()
    {

        drone_swarm.activateContinuous();
        soldier_swarm.activateContinuous();
        worker_swarm.activateContinuous();
    }

}
