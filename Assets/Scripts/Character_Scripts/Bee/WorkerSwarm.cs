using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkerSwarm : BeeSwarm
{
    /*
    * Increase 1 random energy by 1 for each bee
    * 
    * @author Tanner Skomar
    */
    public override void signalEffect()
    {
        for (int i = 0; i < amount_of_bees; i++)
        {
            EnergyManager.Instance.increaseRandomEnergy(1);
        }
    }

    /*
    * Remove passive speed buff
    * then do normal reset
    * 
    * @author Tanner Skomar
    */
    public override void resetSwarm()
    {
        host_character.modifySpeed(-effect_stacks * effect_amount);//Remove the speed bonus
        base.resetSwarm();
    }

    /*
     * Updates the modifier being applied to hosts speed mod 
     * if required amount of bees has been reached
     * @author Tanner Skomar
     */
    public override void updatePassiveEffect()
    {
        double new_stack_amount = System.Math.Floor(amount_of_bees / stack_size);
        if (new_stack_amount > effect_stacks)
        {
            effect_stacks = (int)new_stack_amount;
            host_character.modifySpeed(effect_amount);//Add effect amount to host speed mod
        }
        //Debug.Log("Workers Update Passive");
    }
}
