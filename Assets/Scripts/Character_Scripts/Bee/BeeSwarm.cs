using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSwarm
{


    public CombatCharacters host_character;

    public int amount_of_bees = 0;//amount of bees in swarm
    public int effect_stacks = 0;//Amount of times the continuous effect has stacked

    public static int effect_amount = 1; //Amount of effect per stack
    public static double stack_size = 1f; //amount of bees per stack

    public BeeSwarm(){}

    /*
    * To be overriden by different swarm types
    * Put an effect that should happen at the end of a hosts turn here
    * 
    * @author Tanner Skomar
    */
    public virtual void activateContinuous()
    {

    }

    /*
    * To be overriden by different swarm types
    * Put effect that should happen when swarm is signaled here
    * 
    * @author Tanner Skomar
    */
    public virtual void signalEffect()
    {

    }

    /*
    * To be overriden by different swarm types
    * put changes to passive effect here
    * called when bees are added
    * 
    * @author Tanner Skomar
    */
    public virtual void updatePassiveEffect()
    {

    }

    /*
    * To be overriden by different swarm types
    * Reset swarm to base state
    * passive modifiers should be removed here when overriden
    * 
    * @author Tanner Skomar
    */
    public virtual void resetSwarm()
    {
        amount_of_bees = 0;
        effect_stacks = 0;
    }

    /*
    * Used to set the character field
    * @param: new_character - combat character that owns this swarm
    * @author Tanner Skomar
    */
    public void setCharacter(CombatCharacters new_character)
    {
        host_character = new_character;
    }

    /*
    * Signal the swarm to do it's effect amount times
    * Then reset to bees and stacks to 0
    * and removes passive modifers
    * 
    * @param: amount - # of times to trigger signal effect before removing bees
    * @author Tanner Skomar
    */
    public void signal(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            signalEffect();
        }
        resetSwarm();
    }


    /*
     * Increase the amount_of_bees by given amount
     * recalculate passive effects
     */
    public void increaseBees(int amount)
    {
        amount_of_bees += amount;
        updatePassiveEffect();
    }


}
