using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EnemyAI : MonoBehaviour
{
    private Transform intent_display_root;
    private Transform enemy_intent_root;
    [SerializeField] private GameObject targetting_curve_prefab;
    public List<GameObject> targetting_curves; // List of all current intent target curves, for hover display
    //private Transform targetting_curve_root;

    [SerializeField] private Sprite all_player_sprite;
    [SerializeField] private Sprite all_enemy_sprite;

    private EnemyAction[] all_actions; //List of all actions this enemy can take
    [SerializeField] //For testing
    private EnemyAction[] available_actions; // List of actions that can be taken this turn, built from all actions

    private CombatCharacters[] player_targets
    {
        get { return CombatController.Instance.Party.ToArray(); }
    }
    private CombatCharacters[] enemy_targets
    {
        get { return CombatController.Instance.Enemies.ToArray(); }
    }
    private CombatCharacters[] self_target;
    private EnemyCharacter self_script;

    public (EnemyAction, CombatCharacters[]) next_action; //(Action, Targets)
    private (EnemyAction, CombatCharacters[]) last_action; //(Action, Targets)


    /*
     * Activates the decided next action
     * IF no targets are living it will do no action
     * Sets last_action to next_action at the end.
     * @author: Tanner Skomar
     */
    public void takeAction()
    {
        next_action.Item2 = removeDeadTargets(next_action.Item2);
        if(next_action.Item2.Length > 0 && next_action.Item1.isRequiredUsable()) //Targets are available and energy cost can be paid
        {
            self_script.showActionFloatText(next_action.Item1.name);
            next_action.Item1.activate(next_action.Item2);
            Invoke("MoveForwardForAttack",0.2f);
            //Debug.Log(gameObject.GetComponent<EnemyCharacter>().EnemyName + "Rigged");
            transform.Find(gameObject.GetComponent<EnemyCharacter>().EnemyName + "Rigged").GetComponent<Animator>().SetTrigger("Attack");
        } else
        {
            self_script.showActionFloatText("PASS");
            Debug.Log("Planned action is no longer usable " + next_action.Item1.name);
        }
        last_action = next_action;
        decideNextAction(); //decide on next action
    }

    /*
     * Removes dead Characters from the given list of saved targets.
     * Use this before taking action so that only valid targets will be affected
     * @param: chars_to_check - array to remove dead from
     * @author: Tanner Skomar
     */
    private CombatCharacters[] removeDeadTargets(CombatCharacters[] chars_to_check)
    {
        List<CombatCharacters> temp_list = new List<CombatCharacters>();

        foreach (CombatCharacters character in chars_to_check)
        {
            if (character.isAlive())
            {
                temp_list.Add(character);
            }
        }

        return temp_list.ToArray();
    }

    /* 
     * Decides on an action for this enemy to take
     * and puts it and the decided targets into next action
     * 
     * @author: Tanner Skomar
     */
    public void decideNextAction()
    {
        available_actions = buildAvailableActions(); //build array of available actions

        //This picks a random action from available actions
        EnemyAction chosen_action = available_actions[Random.Range(0, available_actions.Length)];

        switch (chosen_action.getTargetType())
        {
            case Action_Target_Type.ALL_ENEMY:
                next_action = (chosen_action, enemy_targets);
                break;
            case Action_Target_Type.ALL_PLAYER:
                next_action = (chosen_action, player_targets);
                break;
            case Action_Target_Type.SINGLE_ENEMY:
                next_action = (chosen_action, decideSingleTarget(Action_Target_Type.SINGLE_ENEMY));
                break;
            case Action_Target_Type.SINGLE_PLAYER:
                next_action = (chosen_action, decideSingleTarget(Action_Target_Type.SINGLE_PLAYER));
                break;
            case Action_Target_Type.SELF:
            default:
                next_action = (chosen_action, self_target);
                break;
        }

        displayNextAction();
    }

    /*
     * Builds array of available actions for  the enemy to take this turn.
     * By default this just puts all available actions in except for the last used action.
     * 
     * This can be overwritten by a subclass for enemy types that need more complex descision making. 
     */
    protected virtual EnemyAction[] buildAvailableActions()
    {
        List<EnemyAction> temp_list = new List<EnemyAction>();

        foreach (EnemyAction action in all_actions)
        {
            if (last_action.Item1 == null || (last_action.Item1 != null && last_action.Item1 != action)) //Add all actions but last_action
            {
                if (action.isRequiredUsable()) //If action is usable then add it to the list
                {
                    temp_list.Add(action);
                }
            }
        }
        if(temp_list.Count == 0) //If no other actions are available then last_action is ok
        {
            temp_list.Add(last_action.Item1);
        }
        return temp_list.ToArray();
    }

    /*
     * Used to decide who to target for single target
     * NOTE: Right now chooses a random target
     * @param target_type: only expects SINGLE_PLAYER or SINGLE_ENEMY
     * @returns: Array of one element, where that element is the decided target
     */
    public CombatCharacters[] decideSingleTarget(Action_Target_Type target_type)
    {
        switch (target_type)
        {
            case Action_Target_Type.SINGLE_PLAYER:
                    return new CombatCharacters[] {player_targets[Random.Range(0, player_targets.Length)]};
            default:
                return new CombatCharacters[] {enemy_targets[Random.Range(0, enemy_targets.Length)]};
        }
    }

    /* If next_action is targting a single character
     * Override the target with the given character
     * 
     * @param new_target: combat character to override target with
     * @author: Tanner Skomar
     */
    public void overrideNextSingleTarget(CombatCharacters new_target)
    {
        switch (next_action.Item1.getTargetType())
        {
            case Action_Target_Type.SINGLE_PLAYER: //only override single targets
            case Action_Target_Type.SINGLE_ENEMY:
                next_action.Item2 = new CombatCharacters[] { new_target };
                displayNextAction();//Update displayed action
                break;
            default:
                break;
        }    
    }

    /*
     * If next action is targeting a single player update it to target the given player
     * 
     * @param new_target: combat character to override target with
     * @author: Tanner Skomar
     */
    public void tauntEnemy(CombatCharacters new_target)
    {
        switch (next_action.Item1.getTargetType())
        {
            case Action_Target_Type.SINGLE_PLAYER: //only override if targeting player
                overrideNextSingleTarget(new_target);
                break;
            default:
                break;
        }
    }

    /*
     * How confuse could work if needed
     */
    public void confuseEnemy()
    {
        Debug.LogError("confuseEnemy() is a placeholder function");
        //switch (next_action.Item1.getTargetType())
        //{
        //    case Action_Target_Type.SINGLE_PLAYER: //hit self with action instead of using it
        //        overrideNextSingleTarget(self_target[0]);
        //        break;
        //    default:
        //        break;
        //}
    }

    /*
     * Display the next intended action above the character's model
     * @author: Travis Baldwin
     */
    private void displayNextAction()
    {
        // when display next action, destroy old action display
        if (intent_display_root.childCount != 0)
        {
            foreach (Transform child in intent_display_root)
            {
                Destroy(child.gameObject);
            }
        }

        targetting_curves = new List<GameObject>();

        Effect[] next_action_effects = next_action.Item1.getActionEffects();
        float spacing = 0.15f; // vertical spacing between effects
        
        // Map of who has been targeted with an arrow so far
        Dictionary<CombatCharacters,bool> seenCharacters = new Dictionary<CombatCharacters, bool>();
        foreach (var character in CombatController.Instance.AllCharacters)
        {
            seenCharacters.Add(character,false);
        }

        List<GameObject> effect_tooltips_list = new List<GameObject>();
        List<GameObject> check_duplicate_tooltip = new List<GameObject>();

        // Loop to display all effects of the action
        for (int i = 0; i < next_action_effects.Length; i++)
        {
            // Sets up horizontal layout group for displaying intent
            GameObject intent_display = new GameObject("Intent_Display");
            intent_display.transform.SetParent(intent_display_root);
            intent_display.transform.SetAsLastSibling();
            intent_display.AddComponent<RectTransform>();
            intent_display.GetComponent<RectTransform>().localPosition = new Vector2(0, 0 + (1 * i) + (spacing * i));
            intent_display.GetComponent<RectTransform>().sizeDelta = intent_display_root.GetComponent<RectTransform>().sizeDelta;
            intent_display.GetComponent<RectTransform>().localScale = Vector3.one*0.75f;


            intent_display.AddComponent<HorizontalLayoutGroup>();
            intent_display.GetComponent<HorizontalLayoutGroup>().spacing = 0.05f; // horizontal spacing
            intent_display.GetComponent<HorizontalLayoutGroup>().childAlignment = TextAnchor.MiddleCenter;
            intent_display.GetComponent<HorizontalLayoutGroup>().childForceExpandWidth = false;
            //intent_display.GetComponent<HorizontalLayoutGroup>().childControlWidth = false;

            // Display the value of the intended effect
            GameObject effect_amount_text = new GameObject("EffectAmountText");
            effect_amount_text.transform.SetParent(intent_display_root);
            effect_amount_text.transform.SetAsLastSibling();
            effect_amount_text.AddComponent<RectTransform>();
            //effect_amount_text.GetComponent<RectTransform>().localPosition = new Vector2(-0.55f, (0 + (1 * i) + (spacing * i)) - 0.21f); // old x spacing
            effect_amount_text.GetComponent<RectTransform>().localPosition = new Vector2(0, (0 + (1 * i) + (spacing * i)) - 0.21f); // new x spacing
            effect_amount_text.GetComponent<RectTransform>().sizeDelta = intent_display_root.GetComponent<RectTransform>().sizeDelta;
            effect_amount_text.GetComponent<RectTransform>().localScale = Vector3.one;

            effect_amount_text.AddComponent<TextMeshProUGUI>();
            //effect_amount_text.GetComponent<TextMeshProUGUI>().text = "temp";
            effect_amount_text.GetComponent<TextMeshProUGUI>().fontSize = 0.3f;
            effect_amount_text.GetComponent<TextMeshProUGUI>().verticalAlignment = VerticalAlignmentOptions.Bottom;
            effect_amount_text.GetComponent<TextMeshProUGUI>().horizontalAlignment = HorizontalAlignmentOptions.Center;

            // create objects to go in the layout group and display the images for the intent
            GameObject intent_action = new GameObject("Intent_Action");
            //GameObject intent_target = new GameObject("Intent_Target");

            intent_action.transform.SetParent(intent_display.transform);
            //intent_target.transform.SetParent(intent_display.transform);

            intent_action.transform.localScale = Vector3.one;
            //intent_target.transform.localScale = Vector3.one;


            // Creates a curve for each target of the current effect
            for (int k = 0; k < next_action.Item2.Length; k++)
            {
                // Check Preconditions that target has not already been visited, and that effect is not energy consume and gain
                var target = next_action.Item2[k];
                if (seenCharacters[target] == true)
                {
                    continue; // Character already has a line to it, dont need an additional line for the other effects
                }
                else
                {
                    seenCharacters[target] = true; // Visit target
                }
                if (next_action_effects[i] is GenerateEarth || next_action_effects[i] is GenerateFire || next_action_effects[i] is GenerateWater || next_action_effects[i] is GenerateWind || next_action_effects[i] is ConsumeWind || next_action_effects[i] is ConsumeWater || next_action_effects[i] is ConsumeEarth || next_action_effects[i] is ConsumeFire)
                {
                    continue; // Energy effects dont use lines
                }
                // End of Check Preconditions

                // bezier curve to target
                GameObject curve = Instantiate(targetting_curve_prefab, intent_display_root);
                targetting_curves.Add(curve);

                Vector3 screen_to_world_pos = intent_display_root.position + 
                    Vector3.Scale(intent_display.GetComponent<RectTransform>().localPosition, intent_display_root.localScale) +
                    Vector3.Scale(new Vector3(-0.8f,spacing * i,0), intent_display_root.localScale);

                DrawQuadraticBezierCurve(Camera.main.ScreenToWorldPoint(screen_to_world_pos), 
                    next_action.Item2[k].transform.Find("TargettingCurve Root").position, 
                    curve.GetComponent<LineRenderer>());
                //Debug.Log(gameObject.name + " targets: " + k + " " + next_action.Item2[k]);

                curve.SetActive(false);
            }

            intent_action.AddComponent<Image>();
            intent_action.AddComponent<LayoutElement>();
            intent_action.GetComponent<LayoutElement>().preferredWidth = 1f;
            intent_action.GetComponent<LayoutElement>().flexibleWidth = 0f;

            //intent_target.AddComponent<Image>();
            //intent_target.AddComponent<LayoutElement>();
            //intent_target.GetComponent<LayoutElement>().preferredWidth = 100f;
            //intent_target.GetComponent<LayoutElement>().flexibleWidth = 0f;

            intent_action.GetComponent<Image>().sprite = next_action_effects[i].getEffectToken();


            //Action_Target_Type action_target_type = next_action.Item1.getTargetType();
            //if (action_target_type == Action_Target_Type.SINGLE_PLAYER ||
            //    action_target_type == Action_Target_Type.SINGLE_ENEMY ||
            //    action_target_type == Action_Target_Type.SELF)
            //{
            //    intent_target.GetComponent<Image>().sprite = next_action.Item2[0].UITurnToken;
            //}
            //else if (action_target_type == Action_Target_Type.ALL_PLAYER)
            //{
            //    intent_target.GetComponent<Image>().sprite = all_player_sprite;
            //}
            //else
            //{
            //    intent_target.GetComponent<Image>().sprite = all_enemy_sprite;
            //}

            Effect current_effect = next_action_effects[i];
            if (current_effect is Damage)
            {
                int damage = current_effect.getAmount() + this.gameObject.GetComponent<EnemyCharacter>().getAttackMod();
                effect_amount_text.GetComponent<TextMeshProUGUI>().text = damage.ToString();
            }
            else if (current_effect is MultiAttack)
            {
                MultiAttack multi_attack = (MultiAttack)current_effect;
                int damage = multi_attack.getDamage() + this.gameObject.GetComponent<EnemyCharacter>().getAttackMod();
                int num_attacks = multi_attack.getNumOfAttacks();
                effect_amount_text.GetComponent<TextMeshProUGUI>().text = num_attacks.ToString() + " x " + damage.ToString();
            }
            else if (current_effect is ConsumeFire || current_effect is ConsumeEarth || current_effect is ConsumeWater || current_effect is ConsumeWind)//Put negative if decreasing energy
            {
                effect_amount_text.GetComponent<TextMeshProUGUI>().text = "-" + current_effect.getAmount().ToString();
            }
            else
            {
                effect_amount_text.GetComponent<TextMeshProUGUI>().text = current_effect.getAmount().ToString();
            }

            if (current_effect)
            {
                if (current_effect.tooltip_prefab != null)
                {
                    if (!check_duplicate_tooltip.Contains(current_effect.tooltip_prefab))
                    {
                        check_duplicate_tooltip.Add(current_effect.tooltip_prefab);
                        effect_tooltips_list.Add(current_effect.tooltip_prefab);
                    }
                }
            }
        }
        // need to reverse order to have tooltips and intent display reflect their order from bottom up
        effect_tooltips_list.Reverse();
        GameObject[] effect_tooltips_array = effect_tooltips_list.ToArray();
        intent_display_root.GetComponent<TooltipObjectHover>().setTooltipPrefabs(effect_tooltips_array);


        // old output
        //string message = name + " will use: " + next_action.Item1 + " on: " + next_action.Item1.getTargetType();
        
        //if(next_action.Item1.getTargetType() == Action_Target_Type.SINGLE_ENEMY || next_action.Item1.getTargetType() == Action_Target_Type.SINGLE_PLAYER)
        //{
        //    message += ": "+ next_action.Item2[0].name;
        //}
        ////Debug.Log(message);
    }

    /*
     * Creates a series of vertices in a LineRenderer component in the shape of a bezier curve
     * @param: A - origin of the curve
     * @param: C - end point of the curve
     * @param: current_curve - LineRenderer component of the generated curve
     * @author: Travis Baldwin
     */
    private void DrawQuadraticBezierCurve(Vector3 A, Vector3 C, LineRenderer current_curve)
    {
        Vector3 v;
        Vector3 AC = C - A;

        //float offset = 1;
        //offset = offset * (AC.magnitude / 500f);
        if (AC.x <= 0) // Determines if v needs to be pos. or neg. to create the curve with an arch
        {
            v = new Vector3(0, 0, 1); // set the perpendicular offset of center vector
        }
        else
        {
            v = new Vector3(0, 0, -1); // set the perpendicular offset of center vector
        }

        Vector3 B_prime = A + AC / 2;
        Vector3 B = B_prime + Vector3.Normalize(Vector3.Cross(AC, v));

        current_curve.positionCount = 200;
        float t = 0f;
        Vector3 vertex;
        for (int i = 0; i < current_curve.positionCount; i++)
        {
            // Quadratic Bezier Curve Equation: (1-t)^2A + 2(1-t)tB + t^2C
            vertex = (1 - t) * (1 - t) * A + 2 * (1 - t) * t * B + t * t * C;
            current_curve.SetPosition(i, vertex);
            t += (1 / (float)current_curve.positionCount);
        }
    }

    void MoveForwardForAttack()
    {
        transform.Translate(-Vector3.right*0.7f);
        Invoke("MoveBackwardToIdle", 1.5f);
    }
    void MoveBackwardToIdle()
    {
        transform.Translate(Vector3.right *0.7f);
    }

    private void Awake()
    {
        intent_display_root = transform.Find("EnemyIntent Canvas").GetChild(0);
        enemy_intent_root = transform.Find("EnemyIntent Root");
        //targetting_curve = transform.Find("EnemyIntent Canvas").GetChild(1).GetComponent<LineRenderer>();

       
        targetting_curves = new List<GameObject>();

        all_actions = GetComponentsInChildren<EnemyAction>();
        if (all_actions.Length == 0)
        {
            Debug.LogError("Enemy has no actions:" + transform.name);
        }
        last_action = (null, null);

        self_target = gameObject.GetComponents<CombatCharacters>();
        self_script = (EnemyCharacter)self_target[0];
    }

    // Start is called before the first frame update
    void Start()
    {
        Vector3 root_pos = Camera.main.WorldToScreenPoint(enemy_intent_root.position);
        intent_display_root.position = root_pos;
        decideNextAction();
    }

    /*
    * For Debugging
    * Prints out available actions and targets
    */
    private void printOptions()
    {
        Debug.Log("Available Actions: ");
        foreach (EnemyAction a in available_actions)
        {
            Debug.Log(a);
        }
        Debug.Log("Player Targets: ");
        foreach (CombatCharacters p in player_targets)
        {
            Debug.Log(p);
        }
        Debug.Log("Enemy Targets: ");
        foreach (CombatCharacters e in enemy_targets)
        {
            Debug.Log(e);
        }
    }

}
