using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

// To be attached to enemies to display their targetting curves when hovered over
public class DisplayEnemyCurveHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private EnemyAI enemy_ai;

    // Start is called before the first frame update
    void Start()
    {
        enemy_ai = gameObject.GetComponent<EnemyAI>();
    }

    /*
     * Activates when the mouse hovers over the gameobject this script is attached to
     * @param: eventData - used to determine pointer events
     * @author: Travis Baldwin
     */
    public void OnPointerEnter(PointerEventData eventData)
    {
        foreach (var item in enemy_ai.targetting_curves)
        {
            item.SetActive(true);
        }
    }

    /*
     * Activates when the mouse stops hovering over the gameobject this script is attached to
     * @param: eventData - used to determine pointer events
     * @author: Travis Baldwin
     */
    public void OnPointerExit(PointerEventData eventData)
    {
        foreach (var item in enemy_ai.targetting_curves)
        {
            item.SetActive(false);
        }
    }

}
