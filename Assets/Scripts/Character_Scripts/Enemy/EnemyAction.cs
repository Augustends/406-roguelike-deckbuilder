using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Action_Target_Type {SELF, ALL_PLAYER, SINGLE_PLAYER, ALL_ENEMY, SINGLE_ENEMY}
public enum Energy_Requirement { NONE, REQUIRED, OPTIONAL } //Used to mark if energy is required, optional, or not needed to use this action.
                                                            //REQUIRED if this action can only be played if energy is available
                                                            //OPTIONAL to activate effects marked as activation_requires_energy or effects marked without if energy is not available
                                                            //NONE if all effects can be activated no matter what
public class EnemyAction : MonoBehaviour
{
    [SerializeField]
    public Effect[] action_effects; //base effects of action
    [SerializeField]
    public Effect[] energy_effects; //alternate effects for if energy is spent

    [SerializeField]
    private Action_Target_Type target_type;

    [SerializeField]
    private Energy_Requirement energy_requirement; //Denotes energy requirement for this action to be play
    [SerializeField]
    private Energy_Type energy_type_required;
    [SerializeField]
    private int energy_amount_consumed;

    [SerializeField]
    private GameObject visual_effect_prefab;

    [SerializeField]
    private string sound_name;

    /*Getter for target_type*/
    public Action_Target_Type getTargetType()
    {
        return target_type;
    }

    /*Getter for action_effects*/
    public Effect[] getActionEffects()
    {
        if(action_effects.Length > 0)
        {
            return action_effects;
        } else //show energy effects if energy is required
        {
            return energy_effects;
        }
    }

    /*
     * Checks if the card is usable based on current energy
     * @return: true if action is playable, else false
     * @author Tanner Skomar
     */
    public bool isRequiredUsable()
    {
        switch (energy_requirement)
        {
            case Energy_Requirement.REQUIRED:
                return EnergyManager.Instance.isEnergyAvailable(energy_type_required, energy_amount_consumed);
            case Energy_Requirement.OPTIONAL: //Optionals can always be used but will have different effects if energy is available
            case Energy_Requirement.NONE: //these can always be used because no energy is required
            default: //Energy_Requirement.None
                return true;
        }
    }

    /*
     * Checks if this action is playable based on energy requirement
     * and available energy
     * @return: true if energy is available to play the action
     * @author Tanner Skomar
     */
    public bool isEnergyPlayable()
    {
        switch (energy_requirement)
        {
            case Energy_Requirement.REQUIRED:
            case Energy_Requirement.OPTIONAL:
                return EnergyManager.Instance.isEnergyAvailable(energy_type_required, energy_amount_consumed);
            case Energy_Requirement.NONE:
            default: //Energy_Requirement.None
                return true;
        }
    }

    /*
     * Activates all effects of the action on the given target
     * Will only ever be called if action is usable.
     * @param: action_targets - array of targets to use action on
     * @author Tanner Skomar
     */
    public void activate(CombatCharacters[] action_targets)
    {
        if (sound_name != null) AudioManager.Instance.Play(sound_name); // Plays named sound effect (Simon)
        switch (energy_requirement)
        {
            case Energy_Requirement.OPTIONAL:
                if (isEnergyPlayable()) //activate effects in energy_effects list
                {
                    Debug.Log("Required energy is available");
                    foreach (Effect e in energy_effects)
                    {
                        
                        e.setTargets(action_targets);
                        e.activate();
                        
                    }
                }
                else  //activate effects in effects list
                {
                    foreach (Effect e in action_effects)
                    {
                        e.setTargets(action_targets);
                        e.activate();
                    }
                }
                break;
            case Energy_Requirement.REQUIRED: //Activate effects in energy effects
                foreach (Effect e in energy_effects)
                {
                    e.setTargets(action_targets);
                    e.activate();
                }
                break;
            case Energy_Requirement.NONE: //Activate base effects on target
            default:
                foreach (Effect e in action_effects)
                {
                    e.setTargets(action_targets);
                    e.activate();
                }
                break;
        }
        showVisualEffectOnTargets(action_targets);
    }

    /*
	 * Show Visual effect prefab at targets location
	 * @param: new_targets: array of targets to play effect on
	 * @author: Tanner Skomar
	 */
    public void showVisualEffectOnTargets(CombatCharacters[] new_targets)
    {
        if (visual_effect_prefab)
        {
            foreach (CombatCharacters t in new_targets)
            {
                t.showVisualEffect(visual_effect_prefab);
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //Put all effects into action_effects so that they can be easily accessed
       // action_effects = gameObject.GetComponents<Effect>();
    }
}
