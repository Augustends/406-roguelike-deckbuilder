using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField]
    private Slider slider;
    [SerializeField]
    private Gradient gradient;
    [SerializeField]
    private Image fill;

    /*
      * Sets health of health bar
      * @param: health - the value to set healthbar slider to
      * @return: void
      * @author: Tanner Skomar
      */
    public void setHealth(int health)
    {
        slider.value = health;

        fill.color = gradient.Evaluate(slider.normalizedValue);
    }

    /*
     * Sets max health of health bar
     * @param: health - the value to set healthbar max to
     * @return: void
     * @author: Tanner Skomar
     */
    public void setMaxHealth(int health)
    {
        slider.maxValue = health;
        fill.color = gradient.Evaluate(1f);
    }
}
