using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{

    public Events.RestartEvent RestartButtonEvent;

    public void HandleMainMenuButtonClick()
    {
        //Audio Manager Files added by Simon.
        AudioManager.Instance.Play("Button");
        AudioManager.Instance.StopPlaying(AudioManager.Instance.GetCurrentSong());
        AudioManager.Instance.PlaySong("MainTheme");
        RestartButtonEvent.Invoke();
    }
}
