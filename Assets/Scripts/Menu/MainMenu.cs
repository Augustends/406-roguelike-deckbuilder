using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MainMenu : MonoBehaviour
{
    // Functions to play fade in/out animations

    [SerializeField] Animation mainMenuAnimation;
    [SerializeField] AnimationClip fadeInAnim;
    [SerializeField] AnimationClip idleAnim;
    [SerializeField] AnimationClip fadeOutAnim;
    public FadeCompleteEvent onFadeEvent;
    [SerializeField] PlayerData playerData;


    public void NewGame()
    {
        GameController.Instance.NewGame();
        AudioManager.Instance.Play("Button"); // Plays named sound effect (Simon)
    }

    public void ContinueGame()
    {
        GameController.Instance.StartGame();
        AudioManager.Instance.Play("Button"); // Plays named sound effect (Simon)
    }

    public void OnFadeInComplete()
    {
        Debug.Log("Main Menu Fade in Complete");
        onFadeEvent.Raise(Events.FadeType.IN);
        UIManager.Instance.SetDummyCameraActive(true);
        mainMenuAnimation.Stop();
        mainMenuAnimation.clip = idleAnim;
        mainMenuAnimation.Play();
    }

    public void OnFadeOutComplete()
    {
        onFadeEvent.Raise(Events.FadeType.OUT);
    }

    public void FadeIn()
    {
        mainMenuAnimation.Stop();
        mainMenuAnimation.clip = fadeInAnim;
        mainMenuAnimation.Play();
    }

    public void FadeOut()
    {
        UIManager.Instance.SetDummyCameraActive(false);
        mainMenuAnimation.Stop();
        mainMenuAnimation.clip = fadeOutAnim;
        mainMenuAnimation.Play();
    }

    public void HandleGameStateChanged(GameState cur, GameState prev)
    {
        if (cur == GameState.RUNNING && prev == GameState.PREGAME)
        {
            FadeOut();
        }
        else if(cur == GameState.PREGAME && prev == GameState.RUNNING)
        {
            FadeIn();
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
