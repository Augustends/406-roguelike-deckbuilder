using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : Singleton<UIManager>
{
    [SerializeField] private MainMenu mainMenu;
    [SerializeField] private PauseMenu pauseMenu;
    [SerializeField] private Camera dummyCamera;
    [SerializeField] private TooltipDisplay tooltip_display;

    void Start()
    {
        pauseMenu.RestartButtonEvent.AddListener(HandlePauseRequest);
    }

    public TooltipDisplay getTooltipScript()
    {
        return tooltip_display;
    }

    public void SetDummyCameraActive(bool active)
    {
        dummyCamera.gameObject.SetActive(active);
    }

    public void HandleGameStateChanged(GameState cur, GameState prev)
    {
        pauseMenu.gameObject.SetActive(cur == GameState.PAUSED);
    }

    public void HandlePauseRequest()
    {
        mainMenu.FadeIn();
        pauseMenu.gameObject.SetActive(false);
    }

}
