using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Targetable : MonoBehaviour, IDropHandler
{
	private CardController cardController;
	
	public void OnDrop(PointerEventData eventData)
	{
		// Commented out to prevent issues with cards being played on intent/debuffs/buffs @auth: Ryan Larsen 3/31/21
		//Card card = eventData.pointerDrag.GetComponent<Card>();
		//cardController.playCard(card, gameObject.GetComponent<CombatCharacters>());
	}

	// Start is called before the first frame update
	void Start()
    {
		cardController = GameObject.FindGameObjectWithTag("CardController").GetComponent<CardController>();
    }
}
