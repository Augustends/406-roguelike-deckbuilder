using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Active_Effect_Type { NONE, BUFF, DEBUFF }//Type of active effect this is, Defaults to none, is set by active effects when applied
public class Effect : MonoBehaviour
{
    [SerializeField]
    private Sprite effect_token;

    public GameObject tooltip_prefab;

	//[SerializeField]
	protected int max_targets; // the number of targets an effect can be applied to @deprecated

    [SerializeField]
    protected bool force_target_self = false; // set to true if self should be targeted instead of the targets array  
                                              // Makes it easy to do things like, damage all targets and apply armour to self in 1 action
                                                              
    
    //[SerializeField]
    protected bool activation_requires_energy = false; //@deprecated Set this to true if this effect should only be activated if the energy required by the card/action is available
                                                                  //Enemy actions and cards will activate all effects on them that can be activated with the available energy 
    protected CombatCharacters[] targets;
    [SerializeField]
    protected int effect_amount;

    [SerializeField]
    protected bool use_ap_cost_for_amount; //Set this to true if this effect should replace effect_amount with the amount of AP spent on the card CARD EFFECTS ONLY

    protected GameObject status_display_object;

    public bool isEndTurnEffect = false;

    protected Active_Effect_Type active_type = Active_Effect_Type.NONE;//Effects that are not active should keep NONE, only set when effects are applied

    public override string ToString()
    {
        return GetType() + ": " + effect_amount;
    }


    /*
     * To be overriden by different effects
     * Use the sandbox funtions below to build activate()
     * This will be called when the effect is first activated
     * in each subclass
     * @author Tanner Skomar
     */
    public virtual void activate()
    {

    }

    /*
     * To be overriden by different effects
     * Use the sandbox funtions below to build activateContinuous()
     * in each subclass 
     * Only needed if effect will persist over multiple turns
     * This will be called at the end of a characters turn 
     * 
     * @author Tanner Skomar
     */
    public virtual void activateContinuous()
    {
        
    }

    /*
     * Get the current active character. obtained from TurnMechanic.
     */
    private CombatCharacters currentCharacter
    {
        get { return TurnMechanic.Instance.ActiveCharacter; }
    }

    /*
	 * Gets the maximum targets an effect can be applied to
	 * @author: Lachlan Muir
	 * @deprecated
	 */
    public int getMaxTargets() {
		return max_targets;
	}

    /*
    * Used to set the targets of an effect
    * @param: targs - CombatCharcter[] of targets
    * @author: Tanner Skomar
    */
    public void setTargets(CombatCharacters[] targs)
    {
        targets = targs;
    }

    /*
     * Sets the amount field to the given amount
     * @param: new_amount - int amount should be set to
     * @author: Tanner Skomar
     */
    public void setAmount(int new_amount)
    {
        effect_amount = new_amount;
    }

    /*
     * Get the amount field
     * @return: amount on effect
     * @author: Tanner Skomar
     */
    public int getAmount()
    {
        return effect_amount;
    }

    /*
    * Get the use_ap_cost_for_amount field
    * @return: true if use_ap_cost_for_amount, else false
    * @author: Tanner Skomar
    */
    public bool getUseAPCostForAmount()
    {
        return use_ap_cost_for_amount;
    }

    /*
    * Set the use_ap_cost_for_amount field
    * @param: new_value - bool field should be set to
    * @author: Tanner Skomar
    */
    public void setUseAPCostForAmount(bool new_value)
    {
        use_ap_cost_for_amount = new_value;
    }

    /*
     * Sets the activation_requires_energy
     * @param: requires_energy - boolean to set value to
     * @author: Tanner Skomar
     * @deprecated
     */
    public void setRequiresEnergy(bool requires_energy)
    {
        activation_requires_energy = requires_energy;
    }

    /*
     * Get the activation_requires_energy field
     * @return: activation_requires_energy bool value
     * @author: Tanner Skomar
     * @deprecated
     */
    public bool getRequiresEnergy()
    {
        return activation_requires_energy;
    }

    /*
     * Sets the active_type field
     * @param: new_active_type - Active_Effect_Type to set value to
     * @author: Tanner Skomar
     */
    public void setActiveType(Active_Effect_Type new_active_type)
    {
        active_type = new_active_type;
    }

    /*
     * Get the active_type field
     * @return: active_type Active_Effect_Type value
     * @author: Tanner Skomar
     */
    public Active_Effect_Type getActiveType()
    {
        return active_type;
    }

    /*
     * Used to easily set all 3 fields at once since their is no constructor
     * @param: new_amount - int amount should be set to
     * @param: new_targets - CombatCharcter[] of targets to set
     * @author: Tanner Skomar
     */
    public void setAllFields(int new_amount, CombatCharacters[] new_targets, Sprite new_effect_token, Active_Effect_Type new_effect_Type = Active_Effect_Type.NONE)
    {
        setAmount(new_amount);
        setTargets(new_targets);
        setEffectToken(new_effect_token);
        setActiveType(new_effect_Type);
    }

    /*
     * asks Energy manager for the available amount of energy
     * @param: energy_type - type of energy to get amount of
     * @return: int amount of given energy type
     * @author: Tanner Skomar
     */
    protected int getAvailableEnergy(Energy_Type energy_type)
    {
        return EnergyManager.Instance.getEnergyAmount(energy_type);
    }

    /*
     * Set the effect_token field
     * @param: new_effect_token - new token
     * @author: Travis Baldwin
     */
    public void setEffectToken(Sprite new_effect_token)
    {
        effect_token = new_effect_token;
    }

    /*
     * Get the effect_token field
     * @return: effect_token Sprite
     * @author: Travis Baldwin
     */
    public Sprite getEffectToken()
    {
        return effect_token;
    }

    /*
     * Used to check if this is the final turn an effect should be active
     * Used in activateContinuous()
     * @author Tanner Skomar
     */
    protected bool isFinalTurn()
    {
        return effect_amount <= 0;
    }

    /*
     * Calls function in targets scripts when effect is applied to them, displays status effect element on targets
     *      saves reference to the created gameobject to update the status effect display when necessary
     * @author: Travis Baldwin
     */
    protected void createDisplay() //@Tanner Skomar modifed when duration and amount were unified
    {
        foreach (CombatCharacters target in targets)
        {
            status_display_object = target.displayStatus(effect_token, effect_amount);
        }
    }

    /*
     * Decreases remaining turns of active effect by 1
     * This goes in activateContinuous() 
     * @author Tanner Skomar
     */
    protected void decrementActiveEffect()
    {
        effect_amount -= 1;
        updateDisplayTurn();
    }

    /*
     * Calls function in targets scripts, updates status effect element on targets
     * @author: Travis Baldwin
     */
    protected void updateDisplayTurn()
    {
        foreach (CombatCharacters target in targets)
        {
            target.updateStatusDisplayTurn(status_display_object, effect_amount);
        }
    }


    /*
    * Sandbox function
    * Add effect as component based on given effect to each target of given effect
    * The new effect will be a component on each target that only targets itself
    * @param: e - Effect to add
    * @author Tanner Skomar
    */
    protected void addActiveEffect(Effect e, string float_text = "", Active_Effect_Type new_effect_Type = Active_Effect_Type.NONE)
    {
        System.Type type = e.GetType();
        if (force_target_self)
        {
            Effect new_effect = currentCharacter.gameObject.AddComponent(type) as Effect;
            new_effect.setAllFields(e.getAmount(), new CombatCharacters[] { currentCharacter }, e.getEffectToken(), new_effect_Type);
            new_effect.createDisplay();
            new_effect.tooltip_prefab = this.tooltip_prefab;
            currentCharacter.showEffectFloatText(float_text);
        } else
        {
            foreach (CombatCharacters target in targets)
            {
                Effect new_effect = target.gameObject.AddComponent(type) as Effect;
                new_effect.setAllFields(e.getAmount(), new CombatCharacters[] { target }, e.getEffectToken(), new_effect_Type);
                new_effect.createDisplay();
                new_effect.tooltip_prefab = this.tooltip_prefab;
                target.showEffectFloatText(float_text);
            }
        }
        
    }

    /*
     * Sandbox function
     * remove effect from active effects of targets
     * Can be overwritten if additional things must be altered 
     * when removed before the end of the effect as long as base is called at the end.
     * 
     * @param: e - Effect to remove
     * @author Tanner Skomar
     */
    public virtual void removeActiveEffect(Effect e)
    {
        foreach (CombatCharacters target in targets)
        {
            target.removeStatusDisplay(status_display_object);
        }
        Destroy(e);
    }


    /*
    * Sandbox function
    * Remove given type of Active Effects from targets
    * @param: type_to_remove - type of active_effect to remove (NONE will remove all)
    * @author Tanner Skomar
    */
    protected void clearActiveEffects(Active_Effect_Type type_to_remove)
    {
        if (force_target_self)
        {
            currentCharacter.removeActiveEffects(type_to_remove);
        }
        else
        {
            foreach (CombatCharacters target in targets)
            {
                target.removeActiveEffects(type_to_remove);
            }
        }
    }


    /*
     * Sandbox function
     * Increase targets health by given amount.
     * @param: amount - amount to increase health by
     * @author Tanner Skomar
     */
    protected void increaseHealth(int amount)
    {
        if (force_target_self)
        {
            currentCharacter.recoverHealth(amount);
        }
        else
        {
            foreach (CombatCharacters target in targets)
            {
                target.recoverHealth(amount);
            }
        }
    }

    /*
     * Sandbox function
     * Decreases targets health by given amount + the users outgoing_damage_mod
     * @param: amount - amount to decrease health by
     * @param: useAttackMod - if the users attack modifer should be added (default true)
     * @author Tanner Skomar
     */
    protected void decreaseHealth(int amount, bool useAttackMod = true)
    {
        int damage = amount;
        if (useAttackMod)
        {
            damage += currentCharacter.getAttackMod();
        }

        if (force_target_self)
        {
            currentCharacter.takeDamage(damage); //add modifier
        }
        else
        {
            foreach (CombatCharacters target in targets)
            {
                target.takeDamage(damage);
            }
        }
    }


    /*
     * Sandbox function
     * Modify all targets speed by the given amount 
     * 
     * @param: amount - amount to add modify speed by
     * @author Tanner Skomar
     */
    protected void modifySpeed(int amount)
    {
        if (force_target_self)
        {
            currentCharacter.modifySpeed(amount); //add modifier
        } else
        {
            foreach (CombatCharacters target in targets)
            {
                target.modifySpeed(amount); //add modifier
            }
        }  
    }

    /*
    * Sandbox function
    * Modify all targets attack by the given amount 
    * 
    * @param: amount - amount to add modify attack by
    * @author Tanner Skomar
    */
    protected void modifyAttack(int amount)
    {
        if (force_target_self)
        {
            currentCharacter.modifyAttack(amount); //add modifier
        }
        else
        {
            foreach (CombatCharacters target in targets)
            {
                target.modifyAttack(amount); //add modifier
            }
        }
    }

    /*
    * Sandbox function
    * Modify all targets defense by the given amount 
    * 
    * @param: amount - amount to add modify defense by
    * @author Tanner Skomar
    */
    protected void modifyDefense(int amount)
    {
        if (force_target_self)
        {
            currentCharacter.modifyDefense(amount); //add modifier
        }
        else
        {
            foreach (CombatCharacters target in targets)
            {
                target.modifyDefense(amount); //add modifier
            }
        }
    }

    /*
     * Sandbox function
     * make user the target of targets hostile Single Target Actions
     * 
     * CARDS ONLY
     * @author Tanner Skomar
     */
    public void tauntEnemies()
    {
        foreach (CombatCharacters target in targets)
        {
            if (target.character_type == Character_Type.ENEMY) //only effects enemies
            {
                EnemyCharacter current_enemy = (EnemyCharacter)target;
                current_enemy.tauntEnemy(currentCharacter);
            }
        }
    }

    /*
    * Sandbox function
    * Modifies the amount of AP targets will start with on future turns
    * 
    * @param: amount - amount to add to future AP
    * @author Tanner Skomar
    */
    public void modifyFutureAP(int amount)
    {
        if (force_target_self)
        {
            if(currentCharacter.character_type == Character_Type.PLAYER)
            {
                PlayerCharacter current_player = (PlayerCharacter)currentCharacter;
                current_player.BoostAP(amount); //add modifier
            }
        }
        else
        {
            foreach (CombatCharacters target in targets)
            {
                if (target.character_type == Character_Type.PLAYER) //THis can only work on players
                {
                    PlayerCharacter current_player = (PlayerCharacter)target;
                    current_player.BoostAP(amount); //add modifier
                }
            }
        }
    }

    /*
    * Sandbox function
    * Adds amount ap to the current players pool immediatly
    * CARDS ONLY
    * 
    * @param: amount - amount to add to AP pool
    * @author Tanner Skomar
    */
    public void modifyCurrentAP(int amount)
    {
        if (currentCharacter.character_type == Character_Type.PLAYER)
        {
            PlayerCharacter current_player = (PlayerCharacter)currentCharacter;
            current_player.ModifyAP(amount);
        }
    }


    /*
     * Sandbox function
     * Draws amount cards into the current players hand
     * CARDS ONLY
     * 
     * @param: amount - cards to draw
     * @author Tanner Skomar
     */
    public void drawCards(int amount)
    {
        if (currentCharacter.character_type == Character_Type.PLAYER)
        {
            PlayerCharacter current_player = (PlayerCharacter)currentCharacter;
            CardController.Instance.drawMultipleFromDeck(current_player, amount);
        }
    }

    /*
     * Sandbox function
     * Modify the start of turn draw amount of player
     * CARDS ONLY
     * 
     * @param: amount - amount to boost draw by
     * @author: Tanner Skomar
     */
    public void modifyFutureDraw(int amount)
    {
        if (force_target_self)
        {
            if (currentCharacter.character_type == Character_Type.PLAYER)
            {
                PlayerCharacter current_player = (PlayerCharacter)currentCharacter;
                current_player.BoostDraw(amount); //add modifier
            }
        }
        else
        {
            foreach (CombatCharacters target in targets)
            {
                if (target.character_type == Character_Type.PLAYER) //THis can only work on players
                {
                    PlayerCharacter current_player = (PlayerCharacter)target;
                    current_player.BoostDraw(amount); //add modifier
                }
            }
        }
    }

    /*
     * Sandbox function
     * Add armour to all targets 
     * 
     * @param: amount - amount to add to armour
     * @author Tanner Skomar
     */
    protected void applyArmour(int amount)
    {   
        if (force_target_self)
        {
            currentCharacter.increaseArmour(amount);
        } else
        {
            foreach (CombatCharacters target in targets)
            {
                target.increaseArmour(amount);
            }
        }
    }

    /*
     * Sandbox function
     * Generate amount of energytype
     * 
     * @param: amount - amount to generate
     * @param: type - Energy type to consume
     * @author Tanner Skomar
     */
    public void generateEnergy(int amount, Energy_Type type)
    {
        EnergyManager.Instance.increaseEnergy(type, amount);
    }

    /*
     * Sandbox function
     * Consume the given amount of the given
     * 
     * @param: amount - amount to consome
     * @param: type - Energy type to consume
     * @author Tanner Skomar
     */
    public void consumeEnergy(int amount, Energy_Type type)
    {
        EnergyManager.Instance.decreaseEnergy(type, amount);
    }


    /*
     * Sandbox function
     * Consume the all of the given energy
     * 
     * @param: type - Energy type to consume
     * @return: amount of energy consumed
     * @author Tanner Skomar
     */
    public int consumeAllEnergy(Energy_Type type)
    {
        int consumed = EnergyManager.Instance.getEnergyAmount(type);
        EnergyManager.Instance.decreaseEnergy(type, consumed);
        return consumed;
    }

    /*
     * Sandbox function
     * Consume the all of the given energy
     * Deal amount damage for each energy consumed
     * @param: amount - amount of damage each attack should do
     * @author Tanner Skomar
     */
    public void energyToAttacks(Energy_Type type)
    {
        int consumed = consumeAllEnergy(type);

        for (int i = 0; i < consumed; i++)
        {
            decreaseHealth(effect_amount);
        }
    }

    /*
     * Sandbox function
     * Consume the all of the given energy
     * Gain amount AP per energy consumed
     * CARDS ONLY
     * 
     * @param: amount - amount AP to gain per energy
     * @author Tanner Skomar
     */
    public void energyToAP(Energy_Type type)
    {
        if (currentCharacter.character_type == Character_Type.PLAYER)
        {
            int consumed = consumeAllEnergy(type);
            for (int i = 0; i < consumed; i++)
            {
                modifyCurrentAP(effect_amount);
            }
        }
    }

    /*
     * Sandbox function
     * Consume the all of the given energy
     * Targets gain effect amount armour for each energy consumed
     * @param: amount - amount of armour gained per energy
     * @author Tanner Skomar
     */
    public void energyToArmour(Energy_Type type)
    {
        int consumed = consumeAllEnergy(type);
        for (int i = 0; i < consumed; i++)
        {
            applyArmour(effect_amount);
        }
    }

    /*
    * Sandbox function
    * Consume the all of users armour
    * deal amount * armour consumed damage to targets
    * 
    * @param: damage_multiplier - multiply this by the armour consumec to get damage
    * @author Tanner Skomar
    */
    public void armourToAttack(int damage_multiplier)
    {
        int armour_consumed = currentCharacter.health.getCurrentArmour();
        currentCharacter.health.setCurrentArmour(0); //Remove armour
        decreaseHealth(damage_multiplier * armour_consumed);//Deal damage for each armour consumed
    }


    /*
     * Sandbox function
    * When this effect is activated decrease targets
    * health by an amount inversly proportional to users
    * current HP
    * 
    * Damage = Numerator/[(userHP/10)rounded up]
    * 
    * @author Tanner Skomar
    */
    public void currentHealthToAttack(int multiplier)
    {
        int lostHealth = (int)System.Math.Ceiling(currentCharacter.health.getMaxHealth()-currentCharacter.health.getCurrentHealth() /2f);
        decreaseHealth(multiplier * lostHealth);
    }



    /*
     * Sandbox function
     * Add given card to the target players deck
     * 
     * @param: card_to_add - card to add
     * @param: amount - amount of copies to add
     * @returns: added card if force target self
     * @author Tanner Skomar
     */
    public void addCardToDecks(Card card_to_add, int amount)
    {
        if (force_target_self)
        {
            if (currentCharacter.character_type == Character_Type.PLAYER)
            {
                PlayerCharacter current_player = (PlayerCharacter)currentCharacter;
                for (int i = 0; i < amount; i++)
                {
                    CardController.Instance.insertCardCopyToDeck(current_player, card_to_add);
                }
            }
        }
        else
        {
            foreach (CombatCharacters target in targets)
            {
                if (currentCharacter.character_type == Character_Type.PLAYER)
                {
                    PlayerCharacter current_player = (PlayerCharacter)target;
                    for (int i = 0; i < amount; i++)
                    {
                        CardController.Instance.insertCardCopyToDeck(current_player, card_to_add);
                    }
                }
            }
        }
    }

    /*
     * Sandbox function
     * Add given card to the directly to players hand
     * CARDS ONLY
     * 
     * @param: card_to_add - card to add
     * @param: amount - amount of copies to add
     * @author Tanner Skomar
     * @Deprecated
     */
    public void addCardToHand(Card card_to_add, int amount)
    {
        if (currentCharacter.character_type == Character_Type.PLAYER)
        {
            for (int i = 0; i < amount; i++)
            {
                PlayerCharacter current_player = (PlayerCharacter)currentCharacter;
                Card new_card = CardController.Instance.insertCardCopyToDeck(current_player, card_to_add); //add card to deck
                CardController.Instance.drawSpecificCard(current_player, new_card); // bring card from deck to hand if their is space
            }
        }
    }

    public void poison(int amount)
    {
        if (force_target_self)
        {
            currentCharacter.modifyPoison(amount); //add modifier
        }
        else
        {
            foreach (CombatCharacters target in targets)
            {
                target.modifyPoison(amount); //add modifier
            }
        }
    }


    //CANNON EFFECT: Rustbot ONLY

    /*
     * Consume all energy and deal mulitplier * consumed damage to targets
     * then take consumed recoil damage 
     * I think this will only be used for 1 card because it is so specilized but here it is anyway
     * @param: enemy_damage_multiplier - multiplies damage done to enemies by this
     * @author: Tanner Skomar
     */
    public void particleBeam(int enemy_damage_multiplier)
    {
        int consumed = consumeAllEnergy(Energy_Type.FIRE);
        consumed += consumeAllEnergy(Energy_Type.WATER);
        consumed += consumeAllEnergy(Energy_Type.WIND);
        consumed += consumeAllEnergy(Energy_Type.EARTH);

        decreaseHealth(enemy_damage_multiplier * consumed); //Damage targets multiplier * energy consumed

        currentCharacter.takeDamage(consumed); //energy consumed recoil damage
    }


    /*
     * For duration take all damage in place of party members
     * 
     * Rustbot should only use this, but this is called on other party members not rustbot
     * @author: Tanner Skomar
     */
    public void modifyDefenseMatrix(bool new_value)
    {
        foreach (CombatCharacters target in CombatController.Instance.Party)
        {
            if(currentCharacter != target)//do not add to rustbot
            {
                if (new_value)//enable
                {
                    target.modifyDefenseMatrix(true, currentCharacter);
                }
                else //disable
                {
                    target.modifyDefenseMatrix(false);
                }
            }
        }
    }



    //SWARM EFFECTS: Queen Bee ONLY

    /*
    * Sandbox function
    * Send amount bees of given type to targets
    * QUEEN BEE ONLY
    * 
    * @param: swarm - type of bee being sent
    * @param: amount - amount of bees to send
    * @author Tanner Skomar
    */
    public void sendBees(SwarmType swarm, int amount)
    {
        if (force_target_self)
        {
            currentCharacter.swarm_manager.sendToSwarm(swarm, amount);
        }
        else
        {
            foreach (CombatCharacters target in targets)
            {
                target.swarm_manager.sendToSwarm(swarm, amount);
            }
        }
    }

    /*
    * Sandbox function
    * Signal bees of swarmtype on targets amount times
    * QUEEN BEE ONLY
    * 
    * @param: swarm - type of bee being signaled
    * @param: amount - amount of times to signal
    * @author Tanner Skomar
    */
    public void signalBees(SwarmType swarm, int amount)
    {
        if (force_target_self)
        {
            currentCharacter.swarm_manager.signalSwarm(swarm, amount);
        }
        else
        {
            foreach (CombatCharacters target in targets)
            {
                target.swarm_manager.signalSwarm(swarm, amount);
            }
        }
    }

    /*
    * Sandbox function
    * Increase bees of swarmtype on targets by amount
    * if at least 1 bee of swarmtype is already on target
    * QUEEN BEE ONLY
    * 
    * @param: swarm - type of bee being increased
    * @param: amount - amount to increase by
    * @author Tanner Skomar
    */
    public void increaseAllExistingSwarms(int amount)
    {
        if (force_target_self)
        {
            currentCharacter.swarm_manager.increaseExistsingBees(amount);
        }
        else
        {
            foreach (CombatCharacters target in targets)
            {
                target.swarm_manager.increaseExistsingBees(amount);
            }
        }
    }

    /**
     * Sandbox Function
     * Doubles the amount of all types of bees in the swarm on the target
     * QUEEN BEE ONLY
     * 
     * @Auth: Ryan Larsen
     * 
     */
    public void doubleAllExistingSwarms()
    {
        if (force_target_self)
        {
            currentCharacter.swarm_manager.doubleExistingBees();
        } else
        {
            foreach (CombatCharacters target in targets)
            {
                target.swarm_manager.doubleExistingBees();
            }
        }
    }

    /*
     * Current Character draws cards equal to the number of workers in their swarm
     * @author: Tanner Skomar
     */
    public void workerToCards()
    {
        drawCards(currentCharacter.swarm_manager.getBeeAmount(SwarmType.WORKER));
    }

    /*
     * Gets tooltip prefab from Resources folder
     * @param path: path to the prefab
     * @return: tooltip prefab gameobject
     * @author: Travis Baldwin
     */
    protected void fetchTooltip(string path)
    {
        tooltip_prefab = Resources.Load<GameObject>(path);
    }
}
