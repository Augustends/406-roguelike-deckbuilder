using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendSoldier : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Bee_Soldier_Tooltip");
    }
    /*
     * Send effect_amount soldier bees to target
     * @author Tanner Skomar
     */
    public override void activate()
    {
        sendBees(SwarmType.SOLDIER, effect_amount);
    }
}
