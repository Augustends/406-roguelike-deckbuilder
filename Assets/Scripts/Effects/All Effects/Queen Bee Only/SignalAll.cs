using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignalAll : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Signal_Tooltip");
    }
    /*
     * Signal all bees on target effect_amount times
     * @author Tanner Skomar
     */
    public override void activate()
    {
        signalBees(SwarmType.ALL, effect_amount);
    }
}
