using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendDrone : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Bee_Guardian_Tooltip");
    }
    /*
     * Send effect_amount Drone bees to target
     * @author Tanner Skomar
     */
    public override void activate()
    {
        sendBees(SwarmType.DRONE, effect_amount);
    }
}
