using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignalDrone : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Bee_Guardian_Tooltip");
    }
    /*
     * Signal drone bees on target effect_amount times
     * @author Tanner Skomar
     */
    public override void activate()
    {
        signalBees(SwarmType.DRONE, effect_amount);
    }
}
