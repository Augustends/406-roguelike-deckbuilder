using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendWorker: Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Bee_Worker_Tooltip");
    }
    /*
     * Send effect_amount Worker bees to target
     * @author Tanner Skomar
     */
    public override void activate()
    {
        sendBees(SwarmType.WORKER, effect_amount);
    }
}
