using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseExistingSwarms : Effect
{
    /*
     * Increase all existing swarm types on target by effect amount
     * @author Tanner Skomar
     */
    public override void activate()
    {
        increaseAllExistingSwarms(effect_amount);
    }
}

