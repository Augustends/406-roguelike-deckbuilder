using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignalWorker : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Bee_Worker_Tooltip");
    }
    /*
     * Signal Worker bees on target effect_amount times
     * @author Tanner Skomar
     */
    public override void activate()
    {
        signalBees(SwarmType.WORKER, effect_amount);
    }
}
