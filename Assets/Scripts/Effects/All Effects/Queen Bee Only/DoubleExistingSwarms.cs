using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleExistingSwarms : Effect
{
    /**
     * Doubles all bee types on the target
     * @Auth: Ryan Larsen
     * 
     */
    public override void activate()
    {
        doubleAllExistingSwarms();
    }
}
