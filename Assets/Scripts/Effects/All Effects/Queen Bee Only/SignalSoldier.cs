using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignalSoldier : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Bee_Soldier_Tooltip");
    }
    /*
     * Signal soldier bees on target effect_amount times
     * @author Tanner Skomar
     */
    public override void activate()
    {
        signalBees(SwarmType.SOLDIER, effect_amount);
    }
}
