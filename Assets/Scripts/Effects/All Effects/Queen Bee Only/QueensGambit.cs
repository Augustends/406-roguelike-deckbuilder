using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueensGambit : Effect
{
    /*
     * Draws cards equal to number of workers on queenbee
     * @author Tanner Skomar
     */
    public override void activate()
    {
        workerToCards();
    }
}
