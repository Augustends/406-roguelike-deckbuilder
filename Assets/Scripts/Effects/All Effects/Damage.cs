using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : Effect
{
    /*
     * When this effect is activated decrease it's 
     * targets health by amount
     * TODO: add animation and sound stuff
     * @author Tanner Skomar
     */
    public override void activate()
    {
        decreaseHealth(effect_amount);
    }
}
