using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmourToAttack : Effect
{
    /*
     * When this effect is activated 
     * Consume all user's armour and deal 
     * effect_amount * armour consumed damage to targets
     * 
     * @author Tanner Skomar
     */
    public override void activate()
    {
        armourToAttack(effect_amount);
    }
}
