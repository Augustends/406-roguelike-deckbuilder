using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlightStatus : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Blight_Tooltip");
    }
    /*
     * When this effect is activated 
     * Remove Buff Active Effects From targets
     * @author Tanner Skomar
     */
    public override void activate()
    {
        clearActiveEffects(Active_Effect_Type.BUFF);
    }
}
