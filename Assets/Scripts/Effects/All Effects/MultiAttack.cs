using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiAttack : Effect
{
    [SerializeField]
    private int damage = 1;

    public int getNumOfAttacks()
    {
        return effect_amount;
    }

    public int getDamage()
    {
        return damage;
    }

    /*
     * When this effect is activated decrease it's 
     * targets health by effect_amount * damage
     * Each as a seperate strike
     * TODO: add animation and sound stuff
     * @author Tanner Skomar
     */
    public override void activate()
    {
        for (int i = 0; i < effect_amount; i++)
        {
            decreaseHealth(damage);
        }
    }

}
