using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Burn : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Burn_Tooltip");
    }

    /*
     * When this effect is activated 
     * add Burn to active effects
     * @author Tanner Skomar
     */
    public override void activate()
    {
        addActiveEffect(this, "Burned", Active_Effect_Type.DEBUFF); //Add effect to active_effects
    }

    /*
     * At the end of every turn after activation
     * 
     * EveryTurn:
     *      Deal amount damage to targets
     *      Decrease remaining turns and amount
     * 
     * LastTurn:
     *      Remove from active effects
     * 
     * @author Tanner Skomar
     */
    public override void activateContinuous()
    {
        decreaseHealth(effect_amount, false); //Do not add users attack modifier to the continuous damage
        decrementActiveEffect();

        if (isFinalTurn())//Last Turn of effect
        {
            removeActiveEffect(this); //remove effect from active_effects
        }
    }
}
