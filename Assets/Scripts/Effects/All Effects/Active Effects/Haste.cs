using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Haste : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Haste_Tooltip");
    }
    /*
     * When this effect is activated 
     * increase targets speed modifier by amount
     * @author Tanner Skomar
     */
    public override void activate()
    {
        modifySpeed(effect_amount); //add modifier
        addActiveEffect(this, "Hasted", Active_Effect_Type.BUFF); //Add effect to active_effects
    }

    /*
     * At the end of every turn after activation
     * 
     * EveryTurn:
     *      Decrease remaining turns and amount
     * 
     * LastTurn:
     *      Remove speed modifier and take out of active effects
     * 
     * @author Tanner Skomar
     */
    public override void activateContinuous()
    {
        modifySpeed(-1);
        decrementActiveEffect();

        if (isFinalTurn())//Effect should now end
        {
            removeActiveEffect(this); //remove effect from active_effects
        }
    }

    /*
     * Override so that stat modifcation can be removed effect reaches 0
     * @author Tanner Skomar
     */
    public override void removeActiveEffect(Effect e)
    {
        modifySpeed(-effect_amount);//Remove Remaining modifier
        base.removeActiveEffect(e);
    }
}
