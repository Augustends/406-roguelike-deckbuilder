using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackUp : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Strength_Tooltip");
        isEndTurnEffect = true;
    }
    /*
     * When this effect is activated 
     * increase targets attack modifier by amount
     * @author Tanner Skomar
     */
    public override void activate()
    {
        modifyAttack(effect_amount); //add modifier
        addActiveEffect(this, "Strengthened", Active_Effect_Type.BUFF); //Add effect to active_effects
    }

    /*
     * Called at the end of every turn after activation
     * 
     * EveryTurn:
     *      Decrease remaining turns and amount
     * 
     * LastTurn:
     *       take out of active effects
     * 
     * @author Tanner Skomar
     */
    public override void activateContinuous()
    {
        modifyAttack(-1);
        decrementActiveEffect();

        if (isFinalTurn())//Effect should now end
        {
            removeActiveEffect(this); //remove effect from active_effects
        }
    }

    /*
    * Override so that stat modifcation can be removed effect reaches 0
    * @author Tanner Skomar
    */
    public override void removeActiveEffect(Effect e)
    {
        modifyAttack(-effect_amount);//Remove Remaining modifier
        base.removeActiveEffect(e);
    }
}
