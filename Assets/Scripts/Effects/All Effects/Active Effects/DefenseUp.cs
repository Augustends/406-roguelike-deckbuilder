using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenseUp : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Sturdy_Tooltip");
    }
    /*
     * When this effect is activated 
     * increase targets defense modifier by amount
     * @author Tanner Skomar
     */
    public override void activate()
    {
        modifyDefense(effect_amount); //add modifier
        addActiveEffect(this, "Sturdy", Active_Effect_Type.BUFF); //Add effect to active_effects
    }

    /*
     * Called at the end of every turn after activation
     * 
     * EveryTurn:
     *     Decrease remaining turns and amount
     * 
     * LastTurn:
     *      Remove defense modifier and take out of active effects
     * 
     * @author Tanner Skomar
     */
    public override void activateContinuous()
    {
        modifyDefense(-1);
        decrementActiveEffect();

        if (isFinalTurn())//Effect should now end
        {
            removeActiveEffect(this); //remove effect from active_effects
        }
    }

    /*
    * Override so that stat modifcation can be removed effect reaches 0
    * @author Tanner Skomar
    */
    public override void removeActiveEffect(Effect e)
    {
        modifyDefense(-effect_amount);//Remove Remaining modifier
        base.removeActiveEffect(e);
    }
}
