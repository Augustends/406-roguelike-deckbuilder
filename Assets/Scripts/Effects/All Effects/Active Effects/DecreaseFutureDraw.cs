using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecreaseFutureDraw : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Draw-_Tooltip");
        isEndTurnEffect = true;
    }
    /*
     * When this effect is activated 
     * increase player targets drawBoost by Effect Amount
     * @author Tanner Skomar
     */
    public override void activate()
    {
        modifyFutureDraw(-effect_amount); //add modifier
        addActiveEffect(this, "Draw-", Active_Effect_Type.DEBUFF); //Add effect to active_effects
    }

    /*
     * Called at the end of every turn after activation
     * 
     * EveryTurn:
     *      Decrease remaining turns and amount
     * 
     * LastTurn:
     *      take out of active effects
     * 
     * @author Tanner Skomar
     */
    public override void activateContinuous()
    {
        modifyFutureDraw(1);
        decrementActiveEffect();

        if (isFinalTurn())//Effect should now end
        {
            removeActiveEffect(this); //remove effect from active_effects
        }
    }

    /*
     * Override so that stat modifcation can be removed effect reaches 0
     * @author Tanner Skomar
     */
    public override void removeActiveEffect(Effect e)
    {
        modifyFutureDraw(effect_amount);//Remove Remaining modifier
        base.removeActiveEffect(e);
    }
}
