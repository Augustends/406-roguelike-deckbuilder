using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*Poison prevents all healing for X turns
 */
public class Poison : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Poison_Tooltip");
        isEndTurnEffect = true;
    }
    /*
     * Increases the poison level on someone by the given amount
     * @author Alex Shepherd
     */
    public override void activate()
    {
        poison(effect_amount); //add modifier
        addActiveEffect(this, "Poisoned", Active_Effect_Type.DEBUFF); //Add effect to active_effects
    }

    /*
     * Called at the end of every turn after activation
     * 
     * EveryTurn:
     *      Decrease remaining turns and amount
     * 
     * LastTurn:
     *       take out of active effects
     * 
     * @author Tanner Skomar
     */
    public override void activateContinuous()
    {
        poison(-1);
        decrementActiveEffect();

        if (isFinalTurn())//Effect should now end
        {
            removeActiveEffect(this); //remove effect from active_effects
        }
    }

    /*
    * Override so that stat modifcation can be removed effect reaches 0
    * @author Tanner Skomar
    */
    public override void removeActiveEffect(Effect e)
    {
        poison(-effect_amount);//Remove Remaining modifier
        base.removeActiveEffect(e);
    }
}
