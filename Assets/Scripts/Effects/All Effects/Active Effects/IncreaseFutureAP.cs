using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseFutureAP : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/AP+_Tooltip");
        isEndTurnEffect = true;
    }
    /*
     * When this effect is activated 
     * increase player targets APBoost modifier by amount
     * @author Tanner Skomar
     */
    public override void activate()
    {
        modifyFutureAP(effect_amount); //add modifier
        addActiveEffect(this, "AP+", Active_Effect_Type.BUFF); //Add effect to active_effects
    }

    /*
     * Called at the end of every turn after activation
     * 
     * EveryTurn:
     *      Decrease remaining turns and amount
     * 
     * LastTurn:
     *      take out of active effects
     * 
     * @author Tanner Skomar
     */
    public override void activateContinuous()
    {
        modifyFutureAP(-1);
        decrementActiveEffect();

        if (isFinalTurn())//Effect should now end
        {
            removeActiveEffect(this); //remove effect from active_effects
        }
    }

    /*
     * Override so that stat modifcation can be removed effect reaches 0
     * @author Tanner Skomar
     */
    public override void removeActiveEffect(Effect e)
    {
        modifyFutureAP(-effect_amount);//Remove Remaining modifier
        base.removeActiveEffect(e);
    }
}
