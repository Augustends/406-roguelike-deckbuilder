using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Regenerate : Effect
{
    private void Start()
    {
        fetchTooltip("Tooltip/Regenerate_Tooltip");
    }
    /*
     * When this effect is activated 
     * add Regenerate to active effects
     * @author Alex Shepherd
     */
    public override void activate()
    {
        addActiveEffect(this, "Regenerate", Active_Effect_Type.BUFF); //Add effect to active_effects
    }

    /*
     * At the end of every turn after activation
     * 
     * EveryTurn:
     *      Heal amount to target
     *      Decrease remaining turns and amount healed
     * 
     * LastTurn:
     *      Remove from active effects
     * 
     * @author Alex Shepherd
     */
    public override void activateContinuous()
    {
        increaseHealth(effect_amount);
        decrementActiveEffect();

        if (isFinalTurn())//Last Turn of effect
        {
            removeActiveEffect(this); //remove effect from active_effects
        }
    }
}
