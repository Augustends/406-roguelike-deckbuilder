using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindEnergyToArmour : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Consume_Tooltip");
    }
    /*
     * Convert All Available Wind Energy into 
     * effect_amount * consumed armour
     * @author Tanner Skomar
     */
    public override void activate()
    {
        energyToArmour(Energy_Type.WIND);
    }
}

