using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireEnergyToMultiAttack : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Consume_Tooltip");
    }
    /*
     * Convert All Available Fire Energy into an attack
     * that deals effect_amount damage
     * @author Tanner Skomar
     */
    public override void activate()
    {
        energyToAttacks(Energy_Type.FIRE);
    }
}
