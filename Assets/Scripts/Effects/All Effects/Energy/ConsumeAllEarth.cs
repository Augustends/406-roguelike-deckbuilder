using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumeAllEarth : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Consume_Tooltip");
    }
    /*
     * Consume All Available Earth Energy
     * @author Tanner Skomar
     */
    public override void activate()
    {
        consumeAllEnergy(Energy_Type.EARTH);
    }
}
