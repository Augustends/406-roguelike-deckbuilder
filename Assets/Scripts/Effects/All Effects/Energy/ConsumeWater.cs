using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumeWater : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Consume_Tooltip");
    }
    /*
     * When this effect is activated consume amount Water energy
     * TODO: add animation and sound stuff
     * @author Tanner Skomar
     */
    public override void activate()
    {
        consumeEnergy(effect_amount, Energy_Type.WATER);
    }
}
