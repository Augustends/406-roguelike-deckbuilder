using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireEnergyToBurn : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Burn_Tooltip");
    }

    /*
     * When this effect is activated 
     * consume all fire energy and add a burn to targets
     * with effect_amount = energy consumed
     * add Burn to active effects
     * @author Tanner Skomar
     * 
     * Changed to make burn duration scale with fire consumed instead of a set amount.  Ryan Larsen, 3/23/21
     */
    public override void activate()
    {
        int fire_consumed = consumeAllEnergy(Energy_Type.FIRE); //Consume fire

        Burn new_burn = new Burn();//Create a new burn
        new_burn.setAllFields(fire_consumed, targets, getEffectToken());//Set all fields on burn
        new_burn.activate();//Activate the burn
    }
}
