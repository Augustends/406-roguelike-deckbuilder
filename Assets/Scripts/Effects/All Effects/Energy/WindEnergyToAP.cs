using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindEnergyToAP : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Consume_Tooltip");
    }
    /*
     * Convert All Available Wind Energy into 
     * effect_amount AP per energy
     * @author Tanner Skomar
     */
    public override void activate()
    {
        energyToAP(Energy_Type.WIND);
    }
}
