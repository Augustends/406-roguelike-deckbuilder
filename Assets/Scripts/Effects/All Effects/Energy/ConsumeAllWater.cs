using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumeAllWater : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Consume_Tooltip");
    }
    /*
     * Consume All Available Water Energy
     * @author Tanner Skomar
     */
    public override void activate()
    {
        consumeAllEnergy(Energy_Type.WATER);
    }
}
