using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllEnergyToAPHalf : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Consume_Tooltip");
    }
    /*
    * Consume all energy and convert it to half the amount in AP
    * @author Alex Shepherd
     */
    public override void activate()
    {
        int consumed = consumeAllEnergy(Energy_Type.FIRE);
        consumed += consumeAllEnergy(Energy_Type.WATER);
        consumed += consumeAllEnergy(Energy_Type.WIND);
        consumed += consumeAllEnergy(Energy_Type.EARTH);
        modifyFutureAP(consumed / 2);
    }
}
