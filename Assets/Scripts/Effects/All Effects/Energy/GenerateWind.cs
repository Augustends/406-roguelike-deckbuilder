using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateWind : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Generate_Tooltip");
    }
    /*
     * When this effect is activated generate amount Wind energy
     * TODO: add animation and sound stuff
     * @author Tanner Skomar
     */
    public override void activate()
    {
        generateEnergy(effect_amount, Energy_Type.WIND);
    }
}
