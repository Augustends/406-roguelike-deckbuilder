using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateRandom : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Generate_Tooltip");
    }
    /*
 * When this effect is activated generate an amount of a random element
 * TODO: add animation and sound stuff
 * @author Ryan Larsen
 */
    public override void activate()
    {
        switch (Random.Range(1, 5))
        {
            case 1://Wind
                generateEnergy(effect_amount, Energy_Type.WIND);
                break;

            case 2://Earth
                generateEnergy(effect_amount, Energy_Type.EARTH);
                break;

            case 3://Fire
                generateEnergy(effect_amount, Energy_Type.FIRE);
                break;

            case 4://Water
                generateEnergy(effect_amount, Energy_Type.WATER);
                break;
        }
    }
}
