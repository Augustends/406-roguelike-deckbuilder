using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterEnergyToArmour : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Consume_Tooltip");
    }
    /*
     * Convert All Available Water Energy into 
     * effect_amount * consumed armour
     * @author Tanner Skomar
     */
    public override void activate()
    {
        energyToArmour(Energy_Type.WATER);
    }
}

