using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Condense : Effect
{
    /*
     * When this effect is activated convert
     * up to amount wind energy to water energy
     * TODO: add animation and sound stuff
     * @author Tanner Skomar
     */
    public override void activate()
    {
        int convert_amount = getAvailableEnergy(Energy_Type.WIND);  //Get available Wind energy to conver
        if (effect_amount < convert_amount) //If the given amount is less than the available amount convert that much instead
        {
            convert_amount = effect_amount;
        }
        //"Convert" Energy by consuming one and generating the other
        consumeEnergy(convert_amount, Energy_Type.WIND);
        generateEnergy(convert_amount, Energy_Type.WATER);
    }
}
