using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyRandomEffects : Effect
{
    [SerializeField]
    private Effect[] effects;
    /*
     * Get effects from the buffs[] array and apply X amount to the target
     * @author Alex Shepherd
     */
    public override void activate()
    {
        for(int i = 0; i < effect_amount; i++)
        {
            Effect effect = effects[Random.Range(0, effects.Length)];
            Debug.Log(effect.name + " is applied");
            effect.setTargets(targets);
            effect.activate();
        }
    }
}
