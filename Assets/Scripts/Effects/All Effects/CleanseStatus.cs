using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CleanseStatus : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Cleanse_Tooltip");
    }
    /*
     * When this effect is activated 
     * Remove Debuff Active Effects From targets
     * @author Tanner Skomar
     */
    public override void activate()
    {
        clearActiveEffects(Active_Effect_Type.DEBUFF);
    }
}