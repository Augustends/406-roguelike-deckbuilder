using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Taunt : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Taunt_Tooltip");
    }
    /*
     * When this effect is draw hostile Single Target Actions to the user
     * 
     * CARDS ONLY
     * @author Tanner Skomar
     */
    public override void activate()
    {
        tauntEnemies();
    }
}
