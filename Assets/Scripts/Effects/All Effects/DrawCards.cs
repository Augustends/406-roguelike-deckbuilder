using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawCards : Effect
{
    /*
     * Draws effect_amount cards into the players hand
     * @author Tanner Skomar
     */
    public override void activate()
    {
        drawCards(effect_amount);
    }
}
