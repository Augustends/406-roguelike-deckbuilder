using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearStatus : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/ClearStatus_Tooltip");
    }
    /*
     * When this effect is activated 
     * Remove All Active Effects From targets
     * @author Tanner Skomar
     */
    public override void activate()
    {
        clearActiveEffects(Active_Effect_Type.NONE);
    }
}
