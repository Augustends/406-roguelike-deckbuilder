using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddCardToHand : Effect
{
    public Card card_to_add; //Card to add to hand
    /*
     * Add given card to current deck then puts
     * it in players hand effect amount times
     * @author Tanner Skomar
     */
    public override void activate()
    {
        Debug.LogError("Add to hand effect is bugged, use add to deck");
        addCardToHand(card_to_add, effect_amount);
    }
}
