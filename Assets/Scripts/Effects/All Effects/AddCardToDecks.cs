using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddCardToDecks : Effect
{
    public Card card_to_add; //Card to add to decks
    /*
     * Add given card to target players deck effect_amount times
     * @author Tanner Skomar
     */
    public override void activate()
    {
        addCardToDecks(card_to_add, effect_amount);
    }
}
