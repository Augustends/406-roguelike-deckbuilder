using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenseMatrix : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/DefenseMatrix_Tooltip");
    }
    /*
     * When this effect is activated 
     * RUSTBOT ONLY
     * Funnel all damage from party members to Rustbot until ended
     * @author Tanner Skomar
     */
    public override void activate()
    {
        modifyDefenseMatrix(true);//add modifier
        addActiveEffect(this, "Defense Matrix", Active_Effect_Type.DEBUFF); //Add effect to active_effects
    }

    /*
     * Called at the end of every turn after activation
     * 
     * EveryTurn:
     *      Decrease remaining turns
     * 
     * LastTurn:
     *      take out of active effects
     * 
     * @author Tanner Skomar
     */
    public override void activateContinuous()
    {
       decrementActiveEffect();

        if (isFinalTurn())//Effect should now end
        {
            removeActiveEffect(this); //remove effect from active_effects
        }
    }

    /*
     * Override so that stat modifcation can be removed effect reaches 0
     * @author Tanner Skomar
     */
    public override void removeActiveEffect(Effect e)
    {
        modifyDefenseMatrix(false);
        base.removeActiveEffect(e);
    }
}
