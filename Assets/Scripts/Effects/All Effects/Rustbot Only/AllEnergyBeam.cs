using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllEnergyBeam : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Consume_Tooltip");
    }
    /*
     * When this effect is activated consume all energy 
     * and deal effect_amount * consumed damage to targets
     * and take consumed recoil damage
     * @author Tanner Skomar
     */
    public override void activate()
    {
        particleBeam(effect_amount);
    }
}

