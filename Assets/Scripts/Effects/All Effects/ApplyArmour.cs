using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyArmour : Effect
{
    /*
     * When this effect it increases targets armour by amount
     * TODO: add animation and sound stuff
     * @author Tanner Skomar
     */
    public override void activate()
    {
        applyArmour(effect_amount);
    }
}
