using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : Effect
{
    /*
     * When this effect is activated increase it's 
     * targets health by amount
     * TODO: add animation and sound stuff
     * @author Tanner Skomar
     */
    public override void activate()
    {
        increaseHealth(effect_amount);
    }
}
