using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyToRandomEffects : Effect
{
    private void Awake()
    {
        fetchTooltip("Tooltip/Consume_Tooltip");
    }

    [SerializeField]
    private Effect[] effects;
    /*
     * Get effects from the buffs[] array and apply X amount to the target
     * @author Alex Shepherd
     */
    public override void activate()
    {
        int consumed = consumeAllEnergy(Energy_Type.EARTH);
        consumed += consumeAllEnergy(Energy_Type.WIND);
        consumed += consumeAllEnergy(Energy_Type.WATER);
        consumed += consumeAllEnergy(Energy_Type.FIRE);
        consumed /= 2;

        for (int i = 0; i < consumed; i++)
        {
            Effect effect = effects[Random.Range(0, effects.Length)];
            Debug.Log(effect.name + " is applied");
            effect.setTargets(targets);
            effect.activate();
        }
    }
}
