using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LowerHealthStrongerAttack : Effect
{

    /*
     * When this effect is activated decrease targets
     * health by an amount inversly proportional to users
     * current HP
     * 
     * Damage = effect_amount/(userHP/10)[rounded up]
     * 
     * @author Tanner Skomar
     */
    public override void activate()
    {
        currentHealthToAttack(effect_amount);
    }

}
