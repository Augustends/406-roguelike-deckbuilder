using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GainAP : Effect
{
    /*
     * This effect increases the current AP of the current Player by effect_amount
     * TODO: add animation and sound stuff
     * @author Tanner Skomar
     */
    public override void activate()
    {
        modifyCurrentAP(effect_amount);
    }
}
