using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CharacterSelect : MonoBehaviour
{
    [SerializeField]  private PlayerData playerData;
    [SerializeField] protected CharacterData[] selectedChars = new CharacterData[3];
    [SerializeField] protected GameObject[] activeProfiles = new GameObject[3];
    [SerializeField] protected CardDisplay cardDisplay;
    public void Awake()
    {
        GenerateProfiles();
    }
    public void SelectCharacter(CharacterData cha)
    {
        GameObject button = EventSystem.current.currentSelectedGameObject;
        //makes sure you don't add 2 of the same character and removes a character if you're selecting a character already in the party
        for(int i = 0; i < 3; i ++)
        {
            if(selectedChars[i] != null && selectedChars[i].character_name == cha.character_name)
            {
                playerData.Dismiss(cha);
                selectedChars[i] = null;
                Destroy(activeProfiles[i]);
                return;
            }
        }
        for(int i = 0; i < 3; i++)
        {
            if (selectedChars[i] == null)
            {
                playerData.Recruit(cha);
                selectedChars[i] = cha;
                GameObject profile = GameObject.Instantiate(cha.profile_prefab, transform.Find("Profile Canvas").Find("ProfileDisplay").transform);
                activeProfiles[i] = profile;
                Button profileButton = profile.transform.Find("Card Display").gameObject.AddComponent<Button>();
                profileButton.onClick.AddListener(delegate { SeeDeck(); });
                return;
            }
        }
    }
    //instantiates the profile prefabs for selected characters to show their stats
    private void GenerateProfiles()
    {
        foreach(CharacterData c in playerData.party_members)
        {
            GameObject profile = GameObject.Instantiate(c.profile_prefab, transform.Find("Profile Canvas").Find("ProfileDisplay").transform);
            Button profileButton = profile.transform.Find("Card Display").gameObject.AddComponent<Button>();
            profileButton.onClick.AddListener(delegate { SeeDeck(); });
            for (int i = 0; i < 3; i++)
            {
                if(selectedChars[i] == null)
                {
                    selectedChars[i] = c;
                    activeProfiles[i] = profile;
                    i = 3;
                }
            }
        }
    }
    //opens the card viewer for selected character
    public void SeeDeck()
    {
        GameObject profile = EventSystem.current.currentSelectedGameObject.transform.parent.gameObject;
        for (int i = 0; i < 3; i++)
        {
            if (activeProfiles[i] != null && GameObject.ReferenceEquals(profile, activeProfiles[i]))
            {
                cardDisplay.OpenSingleDisplay(selectedChars[i]);
                return;
            }
        }
    }
    //goes to the overworld
    public void Continue()
    {
        for(int i = 0; i < 3; i++)
        {
            if(activeProfiles[i] == null)
            {
                return;
            }
        }
        this.transform.GetComponent<LeaveScene>().ReturnToOverworld();
    }
}
