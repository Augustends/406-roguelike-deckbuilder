using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CardDisplay : DeckDisplay
{
    public override void Awake()
    {
        
    }

    public void OpenSingleDisplay(CharacterData character)
    {
        foreach (GameObject obj in deckDisplay)
        {
            Debug.Log(obj.name);
            foreach (Transform child in obj.transform.Find("Mask").transform.Find("Deck").transform)
            {
                Destroy(child.gameObject);
            }
        }
        this.gameObject.SetActive(true);
        decks[0] = GenerateDeck(character.character_deck, deckDisplay[0].transform.Find("Mask").transform.Find("Deck").transform);
    }
}
