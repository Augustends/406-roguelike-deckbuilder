using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Discard : CardPile, IHideableCContainer
{
	public Discard() { }

	public new void Awake()
	{
		base.Awake();
	}

	public void hide()
	{
		// for each card in the pile...
		foreach (Card card in card_list)
		{
			// ... hide the card
			card.Hidden();
		}
	}

	public void unHide()
	{
		// for each card in the pile...
		foreach (Card card in card_list)
		{
			card.InDiscard();
		}
	}
}
