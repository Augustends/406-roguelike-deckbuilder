using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Deck : CardPile, IHideableCContainer
{
	public Deck() { }

	public new void Awake()
	{
		base.Awake();
	}

	void Start() 
	{
		
	}

	public void Init(Card[] initCardSet)
	{
		card_list = new LinkedList<Card>();
		foreach (Card card in initCardSet)
		{
			if (card != null)
			{
				Card newCard = Instantiate(card, transform.parent).GetComponent<Card>();
				card_list.AddLast(newCard);
				GameObject combatCanvas = GameObject.FindGameObjectWithTag("CombatCanvas");
				newCard.transform.SetParent(gameObject.transform);
				newCard.transform.localPosition = new Vector3(0, 0, 0);
				float scale = 0.8f;
				newCard.transform.localScale = new Vector3(scale, scale, scale);
				newCard.InDeck();
			}
			else
				Debug.LogError("Error in Deck Init for: " + owner.name + " - tried to instantiate null card.");
		}
		if (this.isEmpty())
		{
			Debug.LogError("Error while instantiating cards - empty deck list: " + gameObject.transform.name);
		} else // If it is not empty, we shuffle the deck to prepare for combat
        {
			this.shuffle();
        }
	}

	/*
	 * Inserts given card into the deck
	 * @param: card_to_add - card to add to deck
	 * @returns: the card added to the deck
	 * @author: Tanner Skomar
	 */
	public Card insertCardCopy(Card card_to_add) // Updated to rescale and add card to deck @Auth: Ryan Larsen, 3/18/21
    {
		Card newCard = Instantiate(card_to_add, gameObject.transform).GetComponent<Card>();
		card_list.AddLast(newCard);
		newCard.transform.SetParent(gameObject.transform);
		float scale = 0.8f;
		newCard.transform.localScale = new Vector3(scale, scale, scale);
		newCard.InDeck();
		return newCard;
	}

	public void hide()
	{
		// for each card in the pile...
		foreach (Card card in card_list)
		{
			// ... hide the card
			card.Hidden();
		}
	}

	public void unHide()
	{
		// for each card in the pile...
		foreach (Card card in card_list)
		{
			card.InDeck();
		}
	}
}
