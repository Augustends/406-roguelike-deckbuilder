/* Hand.cs
 * by Haydn V. Harach, HVH552
 * for CMPT 406 Roguelike Deckbuilder.
 * Contains and manages the cards that are currently in the player's hand.
 * Not shown: displaying the cards, direct player controls.  That belongs in a separate HandGui script.
 */


using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Hand : MonoBehaviour, IHideableCContainer
{
    // Define the maximum number of cards that can ever be in your hand.
    // This is NOT how many cards you start your turn with!
    const int MAX_HAND_SIZE = 12;

    private Card[] cards_in_hand = null;
    private int current_hand_size = 0;
    private Card selected_card = null;
    private int selected_card_position = -1;

    [SerializeField]
    private Deck my_deck;
    [SerializeField]
    private Discard discard_pile;

    // Start is called before the first frame update
    void Awake()
    {
        // The cards in our hand is an array of size MAX_HAND_SIZE.
        cards_in_hand = new Card[MAX_HAND_SIZE];
    }

    /*
     * So that deck can be set through code
     */
    public void setDeck(Deck new_deck)
    {
        my_deck = new_deck;
    }

    /*
     * So that discard can be set through code
     */
    public void setDiscard(Discard new_discard)
    {
        discard_pile = new_discard;
    }

	public override string ToString()
	{
		string output = "Hand: " + this.gameObject.name;
		output += "\n    Cards: ";
		if (current_hand_size > 0)
		{
			foreach (Card card in this.cards_in_hand)
			{
				if (card != null)
				{
					output += "\n" + card.gameObject.name;
				}
			}
		}

		return output;
	}

	// Add a card to the player's hand.
	public bool addCardToHand(Card new_card)
    {
        // Drawing from an empty deck can return null; don't clutter the error log.
        if (new_card == null) { return false; }

        // If the hand is already full...
        if (current_hand_size >= MAX_HAND_SIZE)
        {
            // print an error and return false.
            Debug.LogError("Error in Hand.addCardToHand(): Hand is full.\n");
            return false;
        }
        else
        {
            // Otherwise, put the new card at the end of the array.
            cards_in_hand[current_hand_size] = new_card;
            current_hand_size += 1;
            return true;
        }
    }

    // Remove the card at the given position from the player's hand.
    public void removeCardFromHand(int position)
    {
        // Starting at the position we're removing, move each card backwards by one slot.
        for (int i = position; (i < current_hand_size) && (i < (MAX_HAND_SIZE-1)); ++i)
        {
            cards_in_hand[i] = cards_in_hand[i + 1];
        }
        // Nullify the card that was at the back of the list.
        cards_in_hand[current_hand_size] = null;
        current_hand_size -= 1;
        // If we had selected the card we just removed, unselect it.
        if (selected_card_position == position)
        {
            selected_card = null;
            selected_card_position = -1;
        }
        // If we had selected a card after the one we removed, correct our position.
        else if (selected_card_position > position)
        {
            selected_card_position -= 1;
        }
    }

    public int getCurrentHandSize()
    {
        return current_hand_size;
    }

    // Get the currently selected card.
    public Card getSelectedCard()
    {
        return selected_card;
    }

    // Get the position of the currently selected card.
    public int getSelectedCardPosition()
    {
        return selected_card_position;
    }

    public Card getCardAtPosition(int position)
    {
        if (position >= current_hand_size)
        {
            Debug.LogError("Error in Hand.getCardAtPosition(): index out of bounds.\n");
            return null;
        }
        return cards_in_hand[position];
    }

    // Draw a card.
    public void drawCard()
    {
        addCardToHand(my_deck.draw());
    }


    // Select a card.
    public void selectCard(int position)
    {
        if (position >= current_hand_size)
        {
            Debug.LogError("Error in Hand.selectCard(): index out of bounds.\n");
            selected_card = null;
            selected_card_position = -1;
        }
        else if (position < 0)
        {
            selected_card = null;
            selected_card_position = -1;
        }
        else
        {
            selected_card = cards_in_hand[position];
            selected_card_position = position;
        }
    }

	/* 
	 * Find a card in the hand and select it
	 * @author: Lachlan Muir
	 * @param: card - the card to find and select
	 * @return: true or false, depending on whether the card was found.
	 */
	public bool findAndSelectCard(Card card)
	{
		for (int i=0; i < current_hand_size; i++) 
		{
			if (getCardAtPosition(i) == card)
			{
				selectCard(i);
				return true;
			}
		}
		return false;
	}

    // Play a card.
    public void playCard(GameObject target)
    {
        // If we haven't selected a card, do nothing.
        if (selected_card == null)
        {
            return;
        }

        // TODO: activate the effect of the card at the chosen target(s?).

        removeCardFromHand(selected_card_position);
    }

    // Discard a card.
    public void discardCard()
    {
        // If we haven't selected a card, do nothing.
        if (selected_card == null)
        {
            return;
        }

        removeCardFromHand(selected_card_position);
    }

	/*
	 * Removes all cards from the hand and returns them.
	 * 
	 * @author: Lachlan Muir
	 */
	public List<Card> discardAll()
	{
		List<Card> cards = new List<Card>();
		for (int i=0; i<cards_in_hand.Length; i++)
		{
			if (cards_in_hand[i] != null)
			{
				cards.Add(cards_in_hand[i]);
				cards_in_hand[i] = null;
				current_hand_size -= 1;
			}
		}

		selected_card = null;
		selected_card_position = -1;

		return cards;
	}
    /**
     * Returns a list of all the cards currently in the hand.
     * 
     * @auth: Ryan Larsen
     */
    public List<Card> CurrentHand()
    {
        List<Card> cards = new List<Card>();
        for (int i = 0; i < cards_in_hand.Length; i++)
        {
            if (cards_in_hand[i] != null)
            {
                cards.Add(cards_in_hand[i]);
            }
        }

        return cards;
    }

	public void hide()
	{
		if (current_hand_size > 0) 
		{
			foreach (Card card in new List<Card>(cards_in_hand).Where(c => c != null))
			{
				if (card != null)
					card.Hidden();
				else
					Debug.LogError("Cannot hide null");
			}
		}
	}

	public void unHide()
	{
		foreach (Card card in cards_in_hand)
		{
			if (card != null)
				card.InHand();
		}
	}
}
