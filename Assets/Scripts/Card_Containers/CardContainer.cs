using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardContainer : MonoBehaviour
{
    /* holds all the cards for this container
	 * uses a LL instead of List or Array because operations for prepending and deleting first object faster
	 */
    protected LinkedList<Card> card_list;

    [SerializeField]
	public PlayerCharacter owner;

    public LinkedList<Card> Card_list { get => card_list; }

    public void Awake()
	{
		card_list = new LinkedList<Card>();
	}

	public bool isEmpty() 
	{
		return card_list.Count == 0;
	}

	/* String conversion for simple debugging
	 * Code by: Lachlan Muir
	 */
	public override string ToString() 
	{
		string output = "Card Container: " + this.gameObject.name;
		output += "\n    Cards: ";
		foreach (Card card in this.card_list)
		{
			output += "\n" + card.gameObject.name;
		}

		return output;
	}

	/* Gets the amount of cards in the container
	 * Code by: Lachlan Muir
	 */
	public int getCardCount()
	{
		return card_list.Count;
	}

	/* adds the given card to the front of the list
	 * Code by: Lachlan Muir
	 */
    public void prependCard(Card card)
    {
		card_list.AddFirst(card);
    }

	/* adds the given card to the end of the container
	 * Code by: Lachlan Muir
	 */
    public void appendCard(Card card)
    {
		card_list.AddLast(card);
    }

	/* Adds the given array of cards to the end of the container in order.
	 * params:
	 *	cards: the array of cards to be added
	 * Code by: Lachlan Muir
	 */
	public void appendMultipleCards(Card[] cards)
	{
		foreach (Card card in cards)
		{
			appendCard(card);
		}
	}

	/* 
	 * Removes the given card from the container
	 * @author: Lachlan Muir
	 * @return: the card removed
	 */
    protected Card removeCard(Card card)
    {
		LinkedListNode<Card> node = card_list.Find(card);
		
		// if found the card, remove it and return the card script
		if (node != null) {
			card_list.Remove(node);
			return node.Value;
		}
		// otherwise, log error and return null
		Debug.LogError("Could not find card");
		return null;
    }

	/* Pulls out all the cards from the container at once
	 * returns: an array of drawn cards containing all the cards in the pile.
	 * Code by: Lachlan Muir
	 */
	public Card[] removeAll()
	{
		Card[] output = new Card[this.card_list.Count];
		// copy the list contents to the output array
		this.card_list.CopyTo(output, 0);
		// clear all the nodes from the card list
		this.card_list.Clear();
		return output;
	}
}
