using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHideableCContainer
{
	void hide();

	void unHide();
}
