using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;

public class CardPile : CardContainer
{
	public new void Awake()
	{
		base.Awake();
	}


	/* Shuffles the pile
	 * this uses a Knuth-Shuffle implementation I found from: https://rosettacode.org/wiki/Knuth_shuffle#C.23
	 * Code by: Lachlan Muir
	 */
	public void shuffle() {
		System.Random random = new System.Random();
		Card[] array = new Card[this.card_list.Count];

		// convert to an array to efficiently apply Knuth-Shuffle
		this.card_list.CopyTo(array, 0);

		for (int i = 0; i < array.Length; i++)
		{
			// Don't select from the entire array on subsequent loops
			int j = random.Next(i, array.Length); 
			Card temp = array[i]; 
			array[i] = array[j]; 
			array[j] = temp;
		}

		// convert back to Linked List and update card_list
		this.card_list = new LinkedList<Card>(array);
	}


	/* Draw from the top of the pile and return the drawn card.
	 * returns: the GameObject of the top card, null if failed
	 * Code by: Lachlan Muir
	 */
	public Card draw() 
	{
		LinkedListNode<Card> node = this.card_list.First;
		try 
		{
			this.card_list.RemoveFirst();
		} 
		catch (InvalidOperationException e) 
		{
			Console.WriteLine(e.Message);
			Debug.Log(gameObject.name + " - Cannot draw from an empty card list: " + this);
			return null;
		}
		return node.Value;
	}

	/* Draws multiple cards at once
	 * returns: an array of drawn cards. If there are not enough cards to draw then 
	 * Code by: Lachlan Muir
	 */
	public Card[] drawMultiple(int num_cards) 
	{
		List<Card> output = new List<Card>();
		for (int i=0; i < num_cards; i++) 
		{
			// if there aren't any cards left in the deck to draw, then quit trying
			if (card_list.Count == 0) 
			{
				break;
			}
			output.Add(draw());
		}

		return output.ToArray();
	}

	/* Inserts the given card at the specified index.
	 * Code by: Lachlan Muir
	 */
	public void insertAtIndex(Card card, int index) 
	{
		if (index > this.card_list.Count - 1 || index < 0) 
		{
			Debug.LogError("insertAtIndex failed - Index out of range");
			throw new IndexOutOfRangeException("Given index out of range");
		}
		// this currently isn't super efficient - may want to change it to just use the LList enumerator directly?
		// insert the new card before the card currently at the given index - will push the card currently there forwards one position
		this.card_list.AddBefore(this.card_list.Find(this.card_list.ElementAt(index)), card);
	}

	/* Inserts the given card at a random index.
	 * Code by: Lachlan Muir
	 */
	public void insertAtRandom(Card card)
	{
		System.Random rand = new System.Random();
		int index = rand.Next(0, this.card_list.Count);
		try
		{
			insertAtIndex(card, index);
		}
		catch(IndexOutOfRangeException e)
		{
			Debug.LogError("insertAtRandom failed - Random index out of range");
			throw new InvalidOperationException("Random index out of range");
		}
	}
}
