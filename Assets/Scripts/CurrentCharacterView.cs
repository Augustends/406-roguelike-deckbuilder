using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentCharacterView : MonoBehaviour
{
    CombatCharacters cur_character;
    [SerializeField]GameObject arrow_indicator; // A child object

    private void Awake()
    {
        SetUpArrow();
    }

    void SetUpArrow()
    {
        if (cur_character != null) //Position root of arrow at character
        {
            arrow_indicator.transform.position = cur_character.transform.position + Vector3.up*2.6f;
        }
    }

    public void UpdateArrow(CombatCharacters newCombatCharacter)
    {
        arrow_indicator.transform.position = cur_character.transform.position + Vector3.up * 2.6f;
    }

    public void HandleNewCurrentCharacter(CombatCharacters newCombatCharacter)
    {
        cur_character = newCombatCharacter;
        //Debug.Log("NEW VIEW FOR CURRENT PLAYER: " + cur_character.name);
        UpdateArrow(newCombatCharacter);
    }

}
