using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class OverworldController : Singleton<OverworldController>, ISavable
{
    public GameObject[] systemPrefabs;
    private List<GameObject> instancedSystemPrefabs;
    [SerializeField] OverworldStateData savedOverWorldState;
    [SerializeField] EncounterDataSO currentEncounterDataSO;

    string currentZoneName = "Default";
    [SerializeField] Vertex currentVertex;
    public Vertex CurrentVertex { get { return currentVertex; } set { currentVertex = value; } }

    protected override void Awake()
    {
        base.Awake(); //Initialize singleton
        currentVertex = null;

    }

    void CenterCamera(Vector3 target)
    {
        float z = Camera.main.transform.position.z;
        float y = Camera.main.transform.position.y;
        Camera.main.transform.position = new Vector3(target.x,y,z);

    }

    void Start()
    {
        InstantiateSystemPrefabs();
        LoadState();
        if(currentVertex!=null) CenterCamera(currentVertex.transform.position + Vector3.right * 6f);
        if (AudioManager.Instance.GetCurrentSong() != "ForestTheme") {
            AudioManager.Instance.StopPlaying(AudioManager.Instance.GetCurrentSong()); //Stops playing the song playing while the stage was loading.(Simon)
            AudioManager.Instance.PlaySong("ForestTheme"); //Forest theme is played as that is the current area avaliable. (Simon)
        }
    }


    void InstantiateSystemPrefabs()
    {
        instancedSystemPrefabs = new List<GameObject>();
        for (int i = 0; i < systemPrefabs.Length; i++)
        {
            instancedSystemPrefabs.Add(Instantiate(systemPrefabs[i],transform)); // Instance the public inspector list of desired game systems and track them
        }
    }


    /// <summary>
    /// Determine if the click is valid
    /// </summary>
    /// <param name="vertGO"></param>
    public void HandleEncounterSelected(GameObject vertGO)
    {
        Vertex v = vertGO.GetComponent<Vertex>();
        // TODO Vallidate Node
        currentVertex = v;
        AudioManager.Instance.Play("Button"); // Plays named sound effect (Simon)
        FadesManager.Instance.FadeOutThen(GoToEncounterScene);
    }

    /// <summary>
    /// Save any needed State
    /// Request scene change
    /// </summary>
    public void GoToEncounterScene()
    {
        SaveState();
        GameController.Instance.HandleEncounterSelected(currentVertex);
    }

    public float dragSpeed = -0.3f;
    private Vector3 dragOrigin;
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            dragOrigin = Input.mousePosition;
            return;
        }

        float limNegX = 0;
        float limPosX = 37;
        if (!Input.GetMouseButton(0)) return;

        Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
        Vector3 move = new Vector3(pos.x * dragSpeed, 0, 0);

        if (!(move.x + Camera.main.transform.position.x <limNegX || move.x + Camera.main.transform.position.x>limPosX))
        {
            Camera.main.transform.Translate(move,Space.World);
        }
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
    }

    public void SaveState()
    {
        savedOverWorldState.current_vertex_id = new VertexData(currentVertex);
        savedOverWorldState.level_name = currentZoneName;
        var savables = transform.GetComponentsInChildren<ISavable>().ToList();
        savables.Remove(this);
        foreach (var s in savables)
        {
            s.SaveState();
        }

        // Update current encounter SO
        currentEncounterDataSO.enemyPrefabs = currentVertex.encounterData.enemyPrefabs;
        currentEncounterDataSO.enemyXPosition = currentVertex.encounterData.enemyXPosition;
        int cr = currentVertex.encounterData.CR;
        currentEncounterDataSO.type = currentVertex.encounterData.type;
    }
    public void LoadState()
    {
        currentZoneName = savedOverWorldState.level_name;
        var loadables = transform.GetComponentsInChildren<ISavable>().ToList();
        loadables.Remove(this);
        foreach(var l in loadables)
        {
            l.LoadState();
        }
    }
}

