// Code by: Lachlan Muir

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Card : MonoBehaviour
{
	[SerializeField]
	public Effect[] effects;
	[SerializeField]
	public Effect[] energy_effects;

	[SerializeField]
	private Energy_Requirement energy_requirement; //Denotes energy requirement for this card to be played
	[SerializeField]
	private Energy_Type energy_type_required;
	[SerializeField]
	private int energy_amount_consumed;
	[SerializeField]
	private string card_name;
	[SerializeField]
	private string sound_name;
	public string Card_name { get => card_name; }
	public Vector3 handPos;


	public enum CardState { IN_HAND, IN_DECK, IN_DISCARD, HIDDEN, DISPLAYED };
	public enum CardCostType { FLAT, REMAINING };
	private CardState state;

	[SerializeField]
	private CardCostType costType;
	public CardCostType CostType { get => costType; }
	public enum CardTargetType { SELF, ALLIES, ENEMIES, ALL, TARGETED, SINGLE_PLAYER, SINGLE_ENEMY };

	[SerializeField]
	private CardTargetType targetType;

	public CardTargetType TargetType { get => targetType; }

	[SerializeField] private GameObject faceUp;
	[SerializeField] private GameObject faceDown;
	[SerializeField] private DragNDrop dragScript;
    //Constant cost of the card, should not change
    //cost is set to defaultCost when the card is discarded
    [SerializeField]private int defaultCost = 0;
	//Current cost of the card, can change

	private int cost = 0;

	private PlayerCharacter currentPlayer
	{
		get { return TurnMechanic.Instance.ActiveCharacter is PlayerCharacter ? (PlayerCharacter)TurnMechanic.Instance.ActiveCharacter : null; }
	}

	public int APCost 
	{ 
		get
		{
			switch (CostType)
			{
				case CardCostType.FLAT:
					return cost;
				case CardCostType.REMAINING:
					return currentPlayer.AP;
				default:
					return cost;
			}

		}
		set => cost = value;
	}

	public int DefaultCost { get => defaultCost;}

    private TextMeshProUGUI apDisplay;
	private TextMeshProUGUI targetDisplay;
	private TextMeshProUGUI nameDisplay;
	private Vector3 cardPosition;
	public Vector3 CardPosition { get => cardPosition; set => cardPosition = value; }

	[SerializeField]
	private GameObject visual_effect_prefab;


    void Start()
	{
		// get the necessary components for showing/hiding the card
		faceUp = gameObject.transform.Find("FaceUp").gameObject;
		//Debug.Log("Faceup:" + faceUp);
		if (faceUp == null)
			Debug.LogError("Could not find FaceUp on card." + transform.name);
		if (faceDown == null)
			Debug.LogError("Could not find FaceDown on card." + transform.name);
		if (dragScript == null)
			Debug.LogError("Could not get DragNDrop component on card." + transform.name);
		if (targetType == null)
			Debug.LogError("Error in: " + gameObject.name + ". Target type not set in editor.");
		//get component for displaying the cost of the card
		apDisplay = faceUp.transform.Find("Card Cost Text").GetComponent<TextMeshProUGUI>();
		//get component for Displaying the target of the card
		targetDisplay = faceUp.transform.Find("Card Target").GetComponent<TextMeshProUGUI>();
		//get component for displaying the name of the card
		nameDisplay = faceUp.transform.Find("Card Name").GetComponent<TextMeshProUGUI>();

		cost = defaultCost;

		UpdateCostText();
		UpdateTargetText();
		UpdateNameText();
	}

	public void Update()
	{
		// may want to move this update somewhere else so it isn't being updated every frame - Lachlan
		if (state != CardState.HIDDEN || state != CardState.IN_DECK)
			UpdateCostText();
	}

	public void InHand()
	{
		SetState(CardState.IN_HAND);
	}

	public void InDeck()
	{
		SetState(CardState.IN_DECK);
	}

	public void InDiscard()
	{
		SetState(CardState.IN_DISCARD);
	}

	public void Hidden()
	{
		SetState(CardState.HIDDEN);
	}

	/*
	 * Activates the effects on the card on the given targets
	 * Different effects can be activated based on the Energy Requirement settings
	 * 
	 * @authors: Tanner Skomar, Lachlan
	 */
	public void activate(CombatCharacters[] targets) 
	{
		if (effects.Length == 0 && energy_effects.Length == 0)
		{
			Debug.Log("Both effects and energy_effects lists are empty");
		} else if(energy_effects.Length == 0 && (energy_requirement == Energy_Requirement.REQUIRED || energy_requirement == Energy_Requirement.OPTIONAL))
        {
			Debug.Log("Card is marked with REQUIRED or OPTIONAL energy Effects but none are provided to energy_effects list");
		} else if (energy_requirement == Energy_Requirement.NONE && energy_effects.Length > 0)
        {
			Debug.Log("Card is marked with Energy_Requirement.NONE but Effects are provided to energy_effects list and will never be used");
		}
		else if (energy_requirement == Energy_Requirement.OPTIONAL && (effects.Length == 0 || energy_effects.Length == 0))
		{
			Debug.Log("Card is marked with Energy_Requirement.OPTIONAL but Effects are not provided to both effects and energy_effects lists.");
		}

		bool activate_energy_effects;
        switch (energy_requirement)
        {
            case Energy_Requirement.OPTIONAL:
                if (isRequiredEnergyAvailable()) //activate effects in energy_effects
				{
					activate_energy_effects = true;
				}
                else  //activate normal effects
                {
					activate_energy_effects = false;
				}
                break;
            case Energy_Requirement.REQUIRED: //Activate effects in from energy_effects
				activate_energy_effects = true;
				break;
			case Energy_Requirement.NONE: //Activate all effects on target
            default:
				activate_energy_effects = false;
				break;
        }
        if (activate_energy_effects)
        {
			foreach (Effect e in energy_effects)
			{
				e.setTargets(targets);
                if (e.getUseAPCostForAmount())
                {
					e.setAmount(APCost);
                }
				e.activate();
			}
		} else
        {
			foreach (Effect e in effects)
			{
				e.setTargets(targets);
				if (e.getUseAPCostForAmount())
				{
					e.setAmount(APCost);
				}
				e.activate();
			}
		}
		showVisualEffectOnTargets(targets);
		if (sound_name != null)
		{
			AudioManager.Instance.Play(sound_name); // Plays named sound effect (Simon)
		}
	}

	/*
	 * Show Visual effect prefab at targets location
	 * @param: new_targets: array of targets to play effect on
	 * @author: Tanner Skomar
	 */
	public void showVisualEffectOnTargets(CombatCharacters[] new_targets)
    {
		if (visual_effect_prefab)
        {
			foreach (CombatCharacters t in new_targets)
			{
				t.showVisualEffect(visual_effect_prefab);
			}
		}
    }

	/*
	 * Used for setting the current state of the card. Handles disabling/enabling card interaction and visual components.
	 * @author: Lachlan Muir
	 */
	public void SetState(CardState newState)
	{
		// make sure necessary components/objects are set up properly
		if (faceDown != null && faceUp != null && dragScript != null)
		{ 
			switch (newState)
			{
				case CardState.IN_DECK:
					// enable the face down image
					faceDown.SetActive(true);
					// disable the face up image
					faceUp.SetActive(false);
					// disable card dragging
					dragScript.enabled = false;
					break;
				case CardState.IN_HAND:
					// enable the face up image
					faceUp.SetActive(true);
					// disable the face down image
					faceDown.SetActive(false);
					// enable card dragging
					dragScript.enabled = true;
					break;
				case CardState.IN_DISCARD:
					// enable the face down image
					faceUp.SetActive(true);
					// disable the face up image
					faceDown.SetActive(false);
					// disable card dragging
					dragScript.enabled = false;
					break;
				case CardState.HIDDEN:
					// disable the face down image
					faceDown.SetActive(false);
					// disable the face up image
					faceUp.SetActive(false);
					// disable the card dragging
					dragScript.enabled = false;
					break;
				case CardState.DISPLAYED:
					// disable the face down image
					faceDown.SetActive(false);
					// enable the face up image
					faceUp.SetActive(true);
					// disable the card dragging
					dragScript.enabled = false;
					//enable button
					break;
				default:
					Debug.LogError("Unhandled card state passed to SetState");
					break;
			}
		} 
		else
		{
			if (faceDown == null)
				Debug.LogError("Error in setting card state - faceDown not set properly.");
			if (faceUp == null)
				Debug.LogError("Error in setting card state - faceUp not set properly.");
			if (dragScript == null)
				Debug.LogError("Error in setting card state - dragScript not set properly.");
		}
	}

	//changes the AP cost of the card to a number between 0 and 3
	public void ChangeAPRandom()
    {
		cost = Random.Range(0, 3);
		UpdateCostText();
    }
	//Updates the display on the card to = it's current cost
	public void UpdateCostText()
    {
		if(costType == CardCostType.FLAT)
        {
			apDisplay.text = cost.ToString();
		} else //Remaing AP
        {
			apDisplay.text = "X";
        }
		
    }

	//Updates the display of the card name to the Card_name field
	public void UpdateNameText()
    {
		nameDisplay.text = Card_name;
	}

	/*
	 * Updates the target text of the card based on the selected type
	 * @author Tanner Skomar
	 */
	public void UpdateTargetText()
    {
        switch (targetType)
        {
			case CardTargetType.ALL:
				targetDisplay.text = "All";
				break;
			case CardTargetType.ALLIES:
				targetDisplay.text = "Allies";
				break;
			case CardTargetType.ENEMIES:
				targetDisplay.text = "Enemies";
				break;
			case Card.CardTargetType.SELF:
				targetDisplay.text = "Self";
				break;
			case Card.CardTargetType.TARGETED:
				targetDisplay.text = "Single Target";
				break;
			case Card.CardTargetType.SINGLE_PLAYER:
				targetDisplay.text = "Single Player";
				break;
			case Card.CardTargetType.SINGLE_ENEMY:
				targetDisplay.text = "Single Enemy";
				break;
			default:
				Debug.LogError("Card does not have a valid target type.");
				break;
		}
	}

	/*
     * Checks if the card is usable based on current energy
     * @return: true if card is playable, else false
     * @author Tanner Skomar
     */
	public bool isEnergyPlayable()
	{
		switch (energy_requirement)
		{
			case Energy_Requirement.REQUIRED:
				return EnergyManager.Instance.isEnergyAvailable(energy_type_required, energy_amount_consumed);//Energy must be available for card to be played
			case Energy_Requirement.OPTIONAL: //Optionals can always be used but will have different effects if energy is available
			case Energy_Requirement.NONE: //these can always be used because no energy is required
			default: //Energy_Requirement.None
				return true;
		}
	}

	/*
     * Checks if this action is playable based on energy requirement
     * and available energy
     * @return: true if energy is available to play the action
     * @author Tanner Skomar
     */
	public bool isRequiredEnergyAvailable()
	{
		switch (energy_requirement)
		{
			case Energy_Requirement.REQUIRED:
			case Energy_Requirement.OPTIONAL:
				return EnergyManager.Instance.isEnergyAvailable(energy_type_required, energy_amount_consumed);
			case Energy_Requirement.NONE:
			default: //Energy_Requirement.None
				return true;
		}
	}

	public void HighlightCard()
	{
		transform.Find("FaceUp").transform.Find("Card Highlight").GetComponent<Image>().enabled = true;
	}
	public void UnHighlightCard()
	{
		transform.Find("FaceUp").transform.Find("Card Highlight").GetComponent<Image>().enabled = false;
	}
}
