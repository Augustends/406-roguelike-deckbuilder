using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class RewardScreenController : LootDisplay
{
    [SerializeField] private GameObject allRewardsDisplay;
    [SerializeField] private GameObject cardRewardDisplay;
    [SerializeField] private TextMeshProUGUI moneyLootDisplay;
    [SerializeField] private Card[] selectedCards = new Card[3];

    private int money;
    private enum RewardScreenState
    {
        allRewards,
        cardSelection,
    }
    private RewardScreenState state;

    private void Start()
    {
        GenerateMoney();
    }
    /*
     * Generates and displays an int that represents the money acquired from this fight
     */
    public void GenerateMoney()
    {
        money = 100 + 5 * Random.Range(0, 10);
        moneyLootDisplay.text = "$" + money;
        AudioManager.Instance.Play("ShopPurchase"); // Plays named sound effect (Simon)
    }

    /*
     * Tied to the Next button in the UI
     * Goes to the next screen to show rewards
     * @author: Alex Shepherd
     */
    public void NextDisplay()
    {
        AudioManager.Instance.Play("Button"); // Plays named sound effect (Simon)
        switch (state)
        {
            //switch to card selection
            case RewardScreenState.allRewards:
                allRewardsDisplay.SetActive(false);
                playerData.AddMoney(money);
                GenerateCardRewardDisplay(cardRewardDisplay, playerData.party_members.Count, 3, new Vector2(-200, 150), new Vector2(200, 135));
                foreach(Card c in cardLoot)
                {
                    if(c != null)
                    {
                        c.GetComponent<RectTransform>().localScale = new Vector3(0.225f, 0.225f, 1);
                    }
                }
                InstantiatePortraits();
                cardRewardDisplay.SetActive(true);
                state = RewardScreenState.cardSelection;
                break;
            //add cards to decks, move on to overworld scene
            case RewardScreenState.cardSelection:
                //loop through all party members
                foreach(Card card in selectedCards)
                {
                    if(card == null)
                    {
                        return;
                    }
                }
                for (int i = 0; i < playerData.party_members.Count; i++)
                {
                    //add card to deck, position i should be the same character with a card in position i of selectedCards
                    for(int j = 0; j < playerData.party_members[i].full_card_list.Count; j++)
                    {
                        if(selectedCards[i].Card_name == playerData.party_members[i].full_card_list[j].Card_name)
                        {
                            playerData.party_members[i].AddCard(playerData.party_members[i].full_card_list[j]);
                        }
                    }
                }
                //return to overworld
                GameObject.Find("GameManager").GetComponent<GameController>().ReturnToOverworldWithFade();
                break;
        }
    }
    //instantiates the portraits of the party members into the scene
    public void InstantiatePortraits()
    {
        for (int i = 0; i < playerData.party_members.Count; i++)
        {
            //Generate the portraits of the party members
            GameObject portrait = new GameObject();
            Image portraitImage = portrait.AddComponent<Image>();
            portrait.GetComponent<Image>().sprite = GameObject.Instantiate(playerData.party_members[i].tokenSprite);
            portrait.transform.SetParent(cardRewardDisplay.transform);
            portrait.GetComponent<RectTransform>().localPosition = new Vector3(-350, 150f - (i * 135), 0);
            portrait.GetComponent<RectTransform>().sizeDelta = new Vector2(200, 200);
        }
    }

    /*
     * Stores a card in the selectedCards array when clicked on
     * @author: Alex
     */
    public override void CardSelected()
    {
        Card choice = EventSystem.current.currentSelectedGameObject.GetComponentInParent<Card>();
        //these loops check to see which row the card is in so you can't get 2 cards from the same row
        //The card is placed into selectedCards[i] to ensure that each character get's one card each
        //loop a number of times equal to the size of the party
        for (int i = 0; i < playerData.party_members.Count; i++)
        {
            //loop 3 times
            for (int j = 0; j < 3; j++)
            {
                if (cardLoot[i, j].gameObject.Equals(choice.gameObject))
                {
                    //unhighlight selectedCards[i]
                    if(selectedCards[i]!= null)
                    {
                        selectedCards[i].UnHighlightCard();
                    }
                    Debug.Log(choice.name + " was selected");
                    selectedCards[i] = choice;
                    choice.HighlightCard();
                    //highlight this card
                    AudioManager.Instance.Play("CardSelect"); // Plays named sound effect (Simon)
                }
            }
        }
    }

}
