using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/*
 * Attach this script to an object that you want to display a tooltip for and 
 *  fill the tooltip_prefab field in the inspector.
 *  @author: Travis Baldwin
 */
public class TooltipObjectHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]private TooltipDisplay.Tooltip_Optional_Placement tooltip_placement;
    [SerializeField]private RectTransform rect_transform; // Can specify separate RectTransform than this gameobject's, if not assigned will default to this gameobject's
    [SerializeField]private GameObject[] tooltip_prefabs;
    
    private GameObject tooltip_group; // Keep track of created tooltipVertGroup so can destroy it when no longer hovering

    // Will trigger when attached to a non-ui gameobject (not inside a canvas) that has a collider
    private void OnMouseEnter()
    {
        if (tooltip_prefabs != null)
        {
            if (rect_transform)
            {
                tooltip_group = UIManager.Instance.getTooltipScript().displayDelayedTooltipFromWorldSpace(rect_transform, tooltip_prefabs, tooltip_placement);
            }
            else
            {
                Debug.LogWarning("RectTransform not attached to: " + gameObject.name);
            }
        }
    }

    // Will trigger when attached to a non-ui gameobject (not inside a canvas) that has a collider
    public void OnMouseExit()
    {
        if (tooltip_group != null)
        {
            UIManager.Instance.getTooltipScript().stopTooltipCoroutines();
            UIManager.Instance.getTooltipScript().destroyTooltip(tooltip_group);
            tooltip_group = null;
        }
    }

    // Will trigger when attached to a ui gameobject (inside a canvas)
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (tooltip_prefabs != null)
        {
            if (rect_transform)
            {
                tooltip_group = UIManager.Instance.getTooltipScript().displayDelayedTooltip(rect_transform, tooltip_prefabs, tooltip_placement);
                //tooltip_group = UIManager.Instance.getTooltipScript().displayTooltip(rect_transform,tooltip_prefabs, tooltip_placement);
            }
            else
            {
                Debug.LogWarning("RectTransform not attached to: " + gameObject.name);
            }
        }
    }

    // Will trigger when attached to a ui gameobject (inside a canvas)
    public void OnPointerExit(PointerEventData eventData)
    {
        if (tooltip_group != null)
        {
            UIManager.Instance.getTooltipScript().stopTooltipCoroutines();
            UIManager.Instance.getTooltipScript().destroyTooltip(tooltip_group);
            tooltip_group = null;
        }
    }
    private void Start()
    {
        // If field not assigned in inspector will default to this gameobject's RectTransform
        if (rect_transform == null)
        {
            rect_transform = gameObject.GetComponent<RectTransform>();
        }
    }

    public GameObject[] getTooltipPrefabs()
    {
        return tooltip_prefabs;
    }

    public void setTooltipPrefabs(GameObject[] new_tooltip_prefabs)
    {
        tooltip_prefabs = new_tooltip_prefabs;
    }
}
