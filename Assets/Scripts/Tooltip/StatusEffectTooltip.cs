using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class StatusEffectTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private int effect_strength;
    private int turns_remaining;
    private string effect_description;
    private GameObject tooltip_group;

    /*
     * Getter for the amount of turns remaining for the displayed effect
     * @returns: turns remaining for displayed effect
     * @author: Travis Baldwin
     */
    public int getTurnsRemaining()
    {
        return turns_remaining;
    }

    /*
     * Setter for the amount of turns remaining for the displayed effect
     * @param: new_turns_remaining - updated turns remaining
     * @author: Travis Baldwin
     */
    public void setTurnsRemaining(int new_turns_remaining)
    {
        turns_remaining = new_turns_remaining;
    }

    /*
     * Getter for the strength of the displayed effect
     * @returns: strength for displayed effect
     * @author: Travis Baldwin
     */
    public int getEffectStrength()
    {
        return effect_strength;
    }

    /*
     * Setter for the strength of the displayed effect
     * @param: new_effect_strength - updated effect strength
     * @author: Travis Baldwin
     */
    public void setEffectStrength(int new_effect_strength)
    {
        effect_strength = new_effect_strength;
    }

    /*
     * Getter for the description of the displayed effect
     * @returns: description for displayed effect
     * @author: Travis Baldwin
     */
    public string getEffectDescription()
    {
        return effect_description;
    }

    /*
     * Setter for the description of the displayed effect
     * @param: new_effect_description - updated effect description
     * @author: Travis Baldwin
     */
    public void setEffectDescription(string new_effect_description)
    {
        effect_description = new_effect_description;
    }

    /*
     * Activates when the mouse hovers over the gameobject this script is attached to
     * @param: eventData - used to determine pointer events
     * @author: Travis Baldwin
     */
    public void OnPointerEnter(PointerEventData eventData)
    {
        Effect[] status_effects = transform.root.GetComponents<Effect>();
        GameObject[] status_effect_tooltips = new GameObject[status_effects.Length];

        //List<System.Type> check_duplicate = new List<System.Type>();
        List<GameObject> check_duplicate_tooltip = new List<GameObject>();

        int i = 0;
        foreach (var item in status_effects)
        {
            if (item)
            {
                if (item.tooltip_prefab != null)
                {
                    if (!check_duplicate_tooltip.Contains(item.tooltip_prefab))
                    {
                        check_duplicate_tooltip.Add(item.tooltip_prefab);
                        status_effect_tooltips[i] = item.tooltip_prefab;
                        i++;
                    }
                }
            }
        }

        tooltip_group = UIManager.Instance.getTooltipScript().displayDelayedTooltip(transform.parent.GetComponent<RectTransform>(),
            status_effect_tooltips, TooltipDisplay.Tooltip_Optional_Placement.DEFAULT);
        //tooltip_group = UIManager.Instance.getTooltipScript().displayTooltip(transform.parent.GetComponent<RectTransform>(),
        //    status_effect_tooltips, TooltipDisplay.Tooltip_Optional_Placement.DEFAULT);
    }

    /*
     * Activates when the mouse stops hovering over the gameobject this script is attached to
     * @param: eventData - used to determine pointer events
     * @author: Travis Baldwin
     */
    public void OnPointerExit(PointerEventData eventData)
    {
        if (tooltip_group != null)
        {
            UIManager.Instance.getTooltipScript().stopTooltipCoroutines();
            UIManager.Instance.getTooltipScript().destroyTooltip(tooltip_group);
            tooltip_group = null;
        }
    }
}
