using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TooltipDisplay : MonoBehaviour
{
    private GameObject tooltipCanvas;
    [SerializeField] private GameObject tooltipVertGroupPrefab;
    private GameObject tooltipVertGroup;
    private bool delayed;

    public GameObject tempref;

    public enum Tooltip_Optional_Placement
    {
        DEFAULT,
        RIGHT,
        LEFT,
        TOP,
        TOP_RIGHT,
        TOP_LEFT,
        BOTTOM,
        BOTTOM_RIGHT,
        BOTTOM_LEFT
    }

    /*
     * Creates tooltipVertGroup with all tooltips as children and moves it's position to the correct location relative to source object based on specified placement parameter
     *   Used with OnPointerEnter() from source object (UI object), calculates from source object's canvas position to tooltipCanvas's position
     * @param: object_transform - source gameobjects RectTransform
     * @param: tooltips - Array of tooltip prefabs to be instantiated under tooltipVertGroup
     * @param: tooltip_placement - optional parameter for specifying the display position of the tooltipVertGroup
     * @return: reference to the created tooltipVertGroup so can destroy correct object later
     * @author: Travis Baldwin
     */
    public GameObject displayTooltip(RectTransform object_transform, GameObject[] tooltips, Tooltip_Optional_Placement tooltip_placement = Tooltip_Optional_Placement.DEFAULT)
    {
        if (!delayed)
        {
            tooltipVertGroup = Instantiate(tooltipVertGroupPrefab, tooltipCanvas.transform);
        }
        else
        {
            tooltipVertGroup.SetActive(true);
        }

        foreach (var item in tooltips)
        {
            if (item != null)
            {
                Instantiate(item, tooltipVertGroup.transform);
            }
        }

        RectTransform rt = tooltipVertGroup.transform.GetComponent<RectTransform>();
        RectTransform plane = tooltipCanvas.GetComponent<RectTransform>();
        Vector3 input = object_transform.position;

        Camera cam;
        switch (tooltipCanvas.GetComponent<Canvas>().renderMode)
        {
            case RenderMode.ScreenSpaceOverlay:
                cam = null;
                break;
            case RenderMode.ScreenSpaceCamera:
            case RenderMode.WorldSpace:
                cam = tooltipCanvas.GetComponent<Canvas>().worldCamera;
                break;
            default:
                throw new System.NotImplementedException();
        }

        Vector3 output;

        switch (tooltip_placement)
        {
            case Tooltip_Optional_Placement.RIGHT:
                input += object_transform.TransformVector(object_transform.rect.width * object_transform.pivot.x, 0, 0);
                if (RectTransformUtility.ScreenPointToWorldPointInRectangle(plane, input, cam, out output))
                {
                    rt.pivot = new Vector3(0f, 0.2f);
                }
                rt.position = output;
                break;

            case Tooltip_Optional_Placement.LEFT:
                input += object_transform.TransformVector(-(object_transform.rect.width * (1f - object_transform.pivot.x)), 0, 0);
                if (RectTransformUtility.ScreenPointToWorldPointInRectangle(plane, input, cam, out output))
                {
                    rt.pivot = new Vector3(1f, 0.2f);
                }
                rt.position = output;
                break;

            case Tooltip_Optional_Placement.TOP:
                input += object_transform.TransformVector(-(object_transform.rect.width * (1f - object_transform.pivot.x)),
                                                           object_transform.rect.height * object_transform.pivot.y, 0);
                if (RectTransformUtility.ScreenPointToWorldPointInRectangle(plane, input, cam, out output))
                {
                    rt.pivot = new Vector3(0f, 0f);
                }
                rt.position = output;
                break;

            case Tooltip_Optional_Placement.TOP_RIGHT:
                input += object_transform.TransformVector(object_transform.rect.width * object_transform.pivot.x,
                                                           object_transform.rect.height * object_transform.pivot.y, 0);
                if (RectTransformUtility.ScreenPointToWorldPointInRectangle(plane, input, cam, out output))
                {
                    rt.pivot = new Vector3(0f, 0f);
                }
                rt.position = output;
                break;

            case Tooltip_Optional_Placement.TOP_LEFT:
                input += object_transform.TransformVector(-(object_transform.rect.width * (1f - object_transform.pivot.x)),
                                                           object_transform.rect.height * object_transform.pivot.y, 0);
                if (RectTransformUtility.ScreenPointToWorldPointInRectangle(plane, input, cam, out output))
                {
                    rt.pivot = new Vector3(1f, 0f);
                }
                rt.position = output;
                break;

            case Tooltip_Optional_Placement.BOTTOM:
                input += object_transform.TransformVector(-(object_transform.rect.width * (1f - object_transform.pivot.x)),
                                                           -(object_transform.rect.height * (1f - object_transform.pivot.y)), 0);
                if (RectTransformUtility.ScreenPointToWorldPointInRectangle(plane, input, cam, out output))
                {
                    rt.pivot = new Vector3(0f, 1f);
                }
                rt.position = output;
                break;

            case Tooltip_Optional_Placement.BOTTOM_RIGHT:
                input += object_transform.TransformVector(object_transform.rect.width * object_transform.pivot.x,
                                                           -(object_transform.rect.height * (1f - object_transform.pivot.y)), 0);
                if (RectTransformUtility.ScreenPointToWorldPointInRectangle(plane, input, cam, out output))
                {
                    rt.pivot = new Vector3(0f, 1f);
                }
                rt.position = output;
                break;

            case Tooltip_Optional_Placement.BOTTOM_LEFT:
                input += object_transform.TransformVector(-(object_transform.rect.width * (1f - object_transform.pivot.x)), 
                                                           -(object_transform.rect.height * (1f - object_transform.pivot.y)), 0);
                if (RectTransformUtility.ScreenPointToWorldPointInRectangle(plane, input, cam, out output))
                {
                    rt.pivot = new Vector3(1f, 1f);
                }
                rt.position = output;
                break;

            case Tooltip_Optional_Placement.DEFAULT:
                if (input.x > plane.rect.width / 2) //right side screen, display to the left
                {
                    input += object_transform.TransformVector(-(object_transform.rect.width * (1f - object_transform.pivot.x)), 0, 0);
                    if (RectTransformUtility.ScreenPointToWorldPointInRectangle(plane, input, cam, out output))
                    {
                        rt.pivot = new Vector3(1f, 0.2f);
                    }
                }
                else // left side screen, display to the right
                {
                    input += object_transform.TransformVector(object_transform.rect.width * object_transform.pivot.x, 0, 0);
                    if (RectTransformUtility.ScreenPointToWorldPointInRectangle(plane, input, cam, out output))
                    {
                        rt.pivot = new Vector3(0f, 0.2f);
                    }
                }
                rt.position = output;
                break;
            default:
                break;
        }

        RectTransform canvasRect = tooltipCanvas.GetComponent<RectTransform>();
        LayoutRebuilder.ForceRebuildLayoutImmediate(rt);

        float y_offset_top = rt.position.y + (rt.rect.height * (1f - rt.pivot.y)) - canvasRect.rect.height;
        if (y_offset_top > 0)
        {
            rt.position = rt.position - new Vector3(0, y_offset_top, 0);
        }
        else
        {
            float y_offset_bottom = rt.position.y - (rt.rect.height * rt.pivot.y);
            if (y_offset_bottom < 0)
            {
                rt.position = rt.position + new Vector3(0, -y_offset_bottom, 0);
            }
        }

        float x_offset_right = rt.position.x + (rt.rect.width * (1f - rt.pivot.x)) - canvasRect.rect.width;
        if (x_offset_right > 0)
        {
            rt.position = rt.position - new Vector3(x_offset_right, 0, 0);
        }
        else
        {
            float x_offset_left = rt.position.x - (rt.rect.width * rt.pivot.x);
            if (x_offset_left < 0)
            {
                rt.position = rt.position + new Vector3(-x_offset_left, 0, 0);
            }
        }

        // Old implementation

        //if (RectTransformUtility.ScreenPointToWorldPointInRectangle(plane, input, cam, out output))
        //{
        //    if (output.x > plane.rect.width / 2) //right side screen
        //    {
        //        Debug.Log("1");
        //        Debug.Log(output.x + " > " + plane.rect.width / 2);
        //        rt.pivot = new Vector3(1f, 0.5f);
        //        //output += rt.TransformVector(-(rt.rect.width * (1f - rt.pivot.x)), 0, 0); // offset our position by the amount of pixels between the pivot and the right side

        //        // supposed to raise tooltip by half its height
        //        //output += rt.TransformVector(0, rt.rect.height * rt.pivot.y, 0);
        //    }
        //    else // left side screen
        //    {
        //        Debug.Log(output.x + " < " + plane.rect.width / 2);
        //        Debug.Log("2");
        //        rt.pivot = new Vector3(0f, 0.5f);
        //        //output += rt.TransformVector(rt.rect.width * rt.pivot.x, 0, 0); ; // offset our position by the amount of pixels between the pivot and the right side

        //        // supposed to raise tooltip by half its height
        //        //output += rt.TransformVector(0, rt.rect.height * rt.pivot.y, 0);
        //    }

        //    rt.position = output;
        //}




        // Old implementation

        ////Vector3 display_pos = object_transform.position;
        //Vector3 display_pos = object_transform.position -
        //    Vector3.Scale(new Vector3(object_transform.rect.width , object_transform.rect.height ), object_transform.localScale);
        ////Vector3.Scale(object_transform, intent_display_root.localScale);

        ////tooltipVertGroup.transform.position = Camera.main.WorldToScreenPoint(
        ////    object_transform.position + new Vector3(object_transform.rect.width / 2, object_transform.rect.height / 2));

        //tooltipVertGroup.transform.position = display_pos;
        ////tooltipVertGroup.transform.position = Camera.main.WorldToScreenPoint(display_pos);

        delayed = false;
        return tooltipVertGroup;
    }

    /*
     * Creates tooltipVertGroup with all tooltips as children and moves it's position to the correct location relative to source object based on specified placement parameter
     *   Used with OnMouseEnter() from source object (non UI object), calculates from source object's world space location into screen space
     * @param: object_transform - source gameobjects RectTransform
     * @param: tooltips - Array of tooltip prefabs to be instantiated under tooltipVertGroup
     * @param: tooltip_placement - optional parameter for specifying the display position of the tooltipVertGroup
     * @return: reference to the created tooltipVertGroup so can destroy correct object later
     * @author: Travis Baldwin
     */
    public GameObject displayTooltipFromWorldSpace(RectTransform object_transform, GameObject[] tooltips, Tooltip_Optional_Placement tooltip_placement = Tooltip_Optional_Placement.DEFAULT)
    {
        if (!delayed)
        {
            tooltipVertGroup = Instantiate(tooltipVertGroupPrefab, tooltipCanvas.transform);
        }
        else
        {
            tooltipVertGroup.SetActive(true);
        }

        foreach (var item in tooltips)
        {
            if (item != null)
            {
                Instantiate(item, tooltipVertGroup.transform);
            }
        }

        RectTransform rt = tooltipVertGroup.transform.GetComponent<RectTransform>();

        Vector3 input = object_transform.position;


        Vector3 output;

        switch (tooltip_placement)
        {
            case Tooltip_Optional_Placement.RIGHT:
                input += object_transform.TransformVector(object_transform.rect.width * object_transform.pivot.x, 0, 0);
                output = Camera.main.WorldToScreenPoint(input);
                rt.pivot = new Vector3(0f, 0.2f);

                rt.position = output;
                break;

            case Tooltip_Optional_Placement.LEFT:
                input += object_transform.TransformVector(-(object_transform.rect.width * (1f - object_transform.pivot.x)), 0, 0);
                output = Camera.main.WorldToScreenPoint(input);
                rt.pivot = new Vector3(1f, 0.2f);

                rt.position = output;
                break;

            case Tooltip_Optional_Placement.TOP:
                input += object_transform.TransformVector(-(object_transform.rect.width * (1f - object_transform.pivot.x)),
                                                           object_transform.rect.height * object_transform.pivot.y, 0);
                output = Camera.main.WorldToScreenPoint(input);
                rt.pivot = new Vector3(0f, 0f);

                rt.position = output;
                break;

            case Tooltip_Optional_Placement.TOP_RIGHT:
                input += object_transform.TransformVector(object_transform.rect.width * object_transform.pivot.x,
                                                           object_transform.rect.height * object_transform.pivot.y, 0);
                output = Camera.main.WorldToScreenPoint(input);
                rt.pivot = new Vector3(0f, 0f);
                rt.position = output;
                break;

            case Tooltip_Optional_Placement.TOP_LEFT:
                input += object_transform.TransformVector(-(object_transform.rect.width * (1f - object_transform.pivot.x)),
                                                           object_transform.rect.height * object_transform.pivot.y, 0);
                output = Camera.main.WorldToScreenPoint(input);
                rt.pivot = new Vector3(1f, 0f);
                
                rt.position = output;
                break;

            case Tooltip_Optional_Placement.BOTTOM:
                input += object_transform.TransformVector(-(object_transform.rect.width * (1f - object_transform.pivot.x)),
                                                           -(object_transform.rect.height * (1f - object_transform.pivot.y)), 0);
                output = Camera.main.WorldToScreenPoint(input);
                rt.pivot = new Vector3(0f, 1f);
                
                rt.position = output;
                break;

            case Tooltip_Optional_Placement.BOTTOM_RIGHT:
                input += object_transform.TransformVector(object_transform.rect.width * object_transform.pivot.x,
                                                           -(object_transform.rect.height * (1f - object_transform.pivot.y)), 0);
                output = Camera.main.WorldToScreenPoint(input);
                rt.pivot = new Vector3(0f, 1f);
                
                rt.position = output;
                break;

            case Tooltip_Optional_Placement.BOTTOM_LEFT:
                input += object_transform.TransformVector(-(object_transform.rect.width * (1f - object_transform.pivot.x)),
                                                           -(object_transform.rect.height * (1f - object_transform.pivot.y)), 0);
                output = Camera.main.WorldToScreenPoint(input);
                rt.pivot = new Vector3(1f, 1f);
                
                rt.position = output;
                break;

            case Tooltip_Optional_Placement.DEFAULT:
                
                if (Camera.main.WorldToScreenPoint(input).x > tooltipCanvas.GetComponent<RectTransform>().rect.width / 2) //right side screen, display to the left
                {
                    input += object_transform.TransformVector(-(object_transform.rect.width * (1f - object_transform.pivot.x)), 0, 0);
                    output = Camera.main.WorldToScreenPoint(input);
                    rt.pivot = new Vector3(1f, 0.2f);
                    
                }
                else // left side screen, display to the right
                {
                    input += object_transform.TransformVector(object_transform.rect.width * object_transform.pivot.x, 0, 0);
                    output = Camera.main.WorldToScreenPoint(input);
                    rt.pivot = new Vector3(0f, 0.2f);
                    
                }
                rt.position = output;
                break;
            default:
                break;
        }
        RectTransform canvasRect = tooltipCanvas.GetComponent<RectTransform>();
        LayoutRebuilder.ForceRebuildLayoutImmediate(rt);

        float y_offset_top = rt.position.y + (rt.rect.height * (1f - rt.pivot.y)) - canvasRect.rect.height;
        if (y_offset_top > 0)
        {
            rt.position = rt.position - new Vector3(0, y_offset_top, 0);
        }
        else
        {
            float y_offset_bottom = rt.position.y - (rt.rect.height * rt.pivot.y);
            if (y_offset_bottom < 0)
            {
                rt.position = rt.position + new Vector3(0, -y_offset_bottom, 0);
            }
        }

        float x_offset_right = rt.position.x + (rt.rect.width * (1f - rt.pivot.x)) - canvasRect.rect.width;
        if (x_offset_right > 0)
        {
            rt.position = rt.position - new Vector3(x_offset_right, 0, 0);
        }
        else
        {
            float x_offset_left = rt.position.x - (rt.rect.width * rt.pivot.x);
            if (x_offset_left < 0)
            {
                rt.position = rt.position + new Vector3(-x_offset_left, 0, 0);
            }
        }


        delayed = false;
        return tooltipVertGroup;
    }

    /*
     * Calls the coroutine to delay creation and display of the tooltip
     *   UI variant
     * @param: object_transform - source gameobjects RectTransform
     * @param: tooltips - Array of tooltip prefabs to be instantiated under tooltipVertGroup
     * @param: tooltip_placement - optional parameter for specifying the display position of the tooltipVertGroup
     * @return: reference to the created tooltipVertGroup so can destroy correct object later
     * @author: Travis Baldwin
     */
    public GameObject displayDelayedTooltip(RectTransform object_transform, GameObject[] tooltips, Tooltip_Optional_Placement tooltip_placement = Tooltip_Optional_Placement.DEFAULT)
    {
        GameObject tooltipVertGroup = Instantiate(tooltipVertGroupPrefab, tooltipCanvas.transform);
        tooltipVertGroup.SetActive(false);
        StartCoroutine(delayedTooltip(tooltipVertGroup, object_transform, tooltips, tooltip_placement));
        return tooltipVertGroup;
    }

    /*
     * Calls the coroutine to delay creation and display of the tooltip
     *   Non-UI variant
     * @param: object_transform - source gameobjects RectTransform
     * @param: tooltips - Array of tooltip prefabs to be instantiated under tooltipVertGroup
     * @param: tooltip_placement - optional parameter for specifying the display position of the tooltipVertGroup
     * @return: reference to the created tooltipVertGroup so can destroy correct object later
     * @author: Travis Baldwin
     */
    public GameObject displayDelayedTooltipFromWorldSpace(RectTransform object_transform, GameObject[] tooltips, Tooltip_Optional_Placement tooltip_placement = Tooltip_Optional_Placement.DEFAULT)
    {
        GameObject tooltipVertGroup = Instantiate(tooltipVertGroupPrefab, tooltipCanvas.transform);
        tooltipVertGroup.SetActive(false);
        StartCoroutine(delayedTooltipFromWorldSpace(tooltipVertGroup, object_transform, tooltips, tooltip_placement));
        return tooltipVertGroup;
    }

    /*
     * Delays the creation and display of the tooltip by a certain amount of seconds
     *   UI variant
     * @param: tooltipVertGroup_delay - A gameobject with a Vertical display group for displaying all the tooltips from the source object
     * @param: object_transform - source gameobjects RectTransform
     * @param: tooltips - Array of tooltip prefabs to be instantiated under tooltipVertGroup
     * @param: tooltip_placement - optional parameter for specifying the display position of the tooltipVertGroup
     * @author: Travis Baldwin
     */
    IEnumerator delayedTooltip(GameObject tooltipVertGroup_delay, RectTransform object_transform, GameObject[] tooltips, Tooltip_Optional_Placement tooltip_placement = Tooltip_Optional_Placement.DEFAULT)
    {
        yield return new WaitForSeconds(0.35f);
        delayed = true;
        tooltipVertGroup = tooltipVertGroup_delay;
        displayTooltip(object_transform, tooltips, tooltip_placement);
    }

    /*
     * Delays the creation and display of the tooltip by a certain amount of seconds
     *   Non-UI variant
     * @param: tooltipVertGroup_delay - A gameobject with a Vertical display group for displaying all the tooltips from the source object
     * @param: object_transform - source gameobjects RectTransform
     * @param: tooltips - Array of tooltip prefabs to be instantiated under tooltipVertGroup
     * @param: tooltip_placement - optional parameter for specifying the display position of the tooltipVertGroup
     * @author: Travis Baldwin
     */
    IEnumerator delayedTooltipFromWorldSpace(GameObject tooltipVertGroup_delay, RectTransform object_transform, GameObject[] tooltips, Tooltip_Optional_Placement tooltip_placement = Tooltip_Optional_Placement.DEFAULT)
    {
        yield return new WaitForSeconds(0.35f);
        delayed = true;
        tooltipVertGroup = tooltipVertGroup_delay;
        displayTooltipFromWorldSpace(object_transform, tooltips, tooltip_placement);
    }

    /*
     * Stops any running coroutines from this script, 
     *   called when mouse leaves source object's bounds if it called for a delayed tooltip
     * @author: Travis Baldwin
     */
    public void stopTooltipCoroutines()
    {
        StopAllCoroutines();
    }

    /*
     * Destroys the Tooltip
     * @param: old_tooltip - object to destroy
     * @author: Travis Baldwin
     */
    public void destroyTooltip(GameObject old_tooltip)
    {
        Destroy(old_tooltip);
    }

    public void onLevelLoadEvent()
    {
        StopAllCoroutines();
        foreach (Transform child in tooltipCanvas.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        tooltipCanvas = gameObject;
        delayed = false;
    }
}