using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TooltipCardHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private TooltipDisplay.Tooltip_Optional_Placement tooltip_placement;
    [SerializeField] private RectTransform rect_transform; // Can specify separate RectTransform than this gameobject's, if not assigned will default to this gameobject's

    private GameObject tooltip_group; // Keep track of created tooltipVertGroup so can destroy it when no longer hovering

    /*
     * Activates when the mouse hovers over the gameobject this script is attached to
     * @param: eventData - used to determine pointer events
     * @author: Travis Baldwin
     */
    public void OnPointerEnter(PointerEventData eventData)
    {
        Effect[] effects = transform.GetComponents<Effect>();
        GameObject[] effect_tooltips = new GameObject[effects.Length];

        //List<System.Type> check_duplicate_effects = new List<System.Type>();
        List<GameObject> check_duplicate_tooltip = new List<GameObject>();

        int i = 0;
        foreach (var item in effects)
        {
            if (item)
            {
                if (item.tooltip_prefab != null)
                {
                    if (!check_duplicate_tooltip.Contains(item.tooltip_prefab))
                    {
                        check_duplicate_tooltip.Add(item.tooltip_prefab);
                        effect_tooltips[i] = item.tooltip_prefab;
                        i++;
                    }
                }
            }
        }

        tooltip_group = UIManager.Instance.getTooltipScript().displayDelayedTooltip(rect_transform,
            effect_tooltips, TooltipDisplay.Tooltip_Optional_Placement.TOP_RIGHT);
    }

    /*
     * Activates when the mouse stops hovering over the gameobject this script is attached to
     * @param: eventData - used to determine pointer events
     * @author: Travis Baldwin
     */
    public void OnPointerExit(PointerEventData eventData)
    {
        destroyTooltip();
    }

    public void destroyTooltip()
    {
        if (tooltip_group != null)
        {
            UIManager.Instance.getTooltipScript().stopTooltipCoroutines();
            UIManager.Instance.getTooltipScript().destroyTooltip(tooltip_group);
            tooltip_group = null;
        }
    }

    private void Start()
    {
        // If field not assigned in inspector will default to this gameobject's RectTransform
        if (rect_transform == null)
        {
            rect_transform = gameObject.GetComponent<RectTransform>();
        }
    }
}
