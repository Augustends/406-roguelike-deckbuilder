using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEngine.UI;

enum turn_state { START, PLAYERTURN, ENEMYTURN}

/// <summary>
/// Keeps track of whose turn it is
/// </summary>
public class TurnMechanic : Singleton<TurnMechanic>
{
    [SerializeField] //So the state can be seen in unity
    turn_state state;


    List<CombatCharacters> doneTurn = new List<CombatCharacters>();
    public List<CombatCharacters> round_current = new List<CombatCharacters>();
    List<CombatCharacters> round_after = new List<CombatCharacters>();

    int round_index;
    CombatCharacters activeCharacter;

    [SerializeField] TurnDisplay turnDisplay;

	public CombatCharacters ActiveCharacter { get => activeCharacter; }

    // Haydn: I'm using this in place of a proper "onTurnStart" event.
    public List<CombatCharacters> getDoneTurn() { return doneTurn; }

    public GameObject endTurnButton;

	private void Awake()
	{
		base.Awake();
	}

	// Start is called before the first frame update
	void Start()
    {
        state = turn_state.START;
        endTurnButton = GameObject.Find("EndTurnButton");
        StartCoroutine(setupCombat()); // StartCoroutine is necessary when used with an IEnumerator method
    }


    /* Setup of Combat and turn orders
     * @return: utilized to yield execution of code temporarily
     * @author: Travis Baldwin, Tanner Skomar
     */
    IEnumerator setupCombat()
    {
		//Debug.Log(CombatController.Instance.Party[1].name + "'s deck: " + CombatController.Instance.Party[1].PlayerDeck);
		// Wait for CombatController to give the go-ahead to start the round
		while (CombatController.Instance.State != CombatController.CombatState.INCOMBAT)
		{
			Debug.Log("TurnMechanic waiting for CombatController");
			yield return new WaitForSeconds(0.5f);
		}

		Debug.Log("TurnMechanic beginning round execution");
        // Make turn orders
        round_index = 0;
        NewRound();

        // Player character
        if (activeCharacter.getCharacter_Type() == Character_Type.PLAYER)
        {
            state = turn_state.PLAYERTURN;
            activeCharacter.takeTurn();
        }
        // Enemy character
        else if (activeCharacter.getCharacter_Type() == Character_Type.ENEMY)
        {
            state = turn_state.ENEMYTURN;
            activeCharacter.takeTurn();
        }
    }
	
	/*
      * Ends the players turn when the end turn button is pressed
      * @return: void
      * @author: Alex
      */
    public void EndPlayerTurn()
    {
        if (state == turn_state.PLAYERTURN)
        {
            activeCharacter.endTurn();
            AudioManager.Instance.Play("Button"); // Plays named sound effect (Simon)
        }
    }


    /* 
     * @return: void
     * @author: Travis Baldwin
     */
    public void HandleEndOfTurn()
    {
        // proceed to next turn
        nextTurn();
    }

    /* Determines which characters turn is next and also if 
     *   the end of the round has been reached
     * @return: void
     * @author: Travis Baldwin
     */
    public void nextTurn()
    {
        doneTurn.Add(ActiveCharacter);
        round_current.RemoveAt(0);
        if (round_current.Count == 0)
        {
            // Next round
            NewRound();
        }
        else
        {
            activeCharacter = round_current[0];
            Refresh();
        }

        if (activeCharacter.character_type == Character_Type.PLAYER)
        {
            state = turn_state.PLAYERTURN;
            ((PlayerCharacter)activeCharacter).takeTurn();
            endTurnButton.GetComponent<Button>().enabled = true;
        }
        else
        {
            endTurnButton.GetComponent<Button>().enabled = false;
            state = turn_state.ENEMYTURN;
            ((EnemyCharacter)activeCharacter).takeTurn();
        }
    }




    float tick = 0;
    private void Update()
    {
        tick += Time.deltaTime;
        if (tick >= 0.2f)
        {
            Refresh();
            tick = 0;
        }
    }


    /// <summary>
    /// Refreshes the turn order in case any characters have died or speeds have changed
    /// </summary>
    private void Refresh()
    {
		if (activeCharacter == null)
			return;
        // Order characters by speed, then only keep those that have not gone this turn. Move active player to top
        round_current = CombatController.Instance.AllCharacters.OrderByDescending(o => o.getCurrentSpeed()).Except(doneTurn).ToList();
        round_current.Remove(activeCharacter);
        round_current.Insert(0,activeCharacter);

        round_after = CombatController.Instance.AllCharacters.OrderByDescending(o => o.getCurrentSpeed()).ToList();
        if (turnDisplay==null)
        {
            Debug.LogError("Must have a turn order display set by inspector or code!");
        }

        turnDisplay.Display(round_current.Concat(round_after).ToList());

    }

    private void NewRound()
    {
        round_current = CombatController.Instance.AllCharacters.OrderByDescending(o => o.getCurrentSpeed()).ToList();
        round_after = CombatController.Instance.AllCharacters.OrderByDescending(o => o.getCurrentSpeed()).ToList(); //TODO REPLACE WITH NEXT TURN SPEED OF CHAR
        round_index += 1;
        activeCharacter = round_current[0];
		//Debug.Log(CombatController.Instance.Party[1].name + "'s deck: " + CombatController.Instance.Party[1].PlayerDeck);
        doneTurn.Clear();
        turnDisplay.NewTurn();
    }
}
