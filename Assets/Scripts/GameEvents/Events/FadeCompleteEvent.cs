using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "New Fade Complete Event")]
public class FadeCompleteEvent: BaseGameEvent<Events.FadeType>
{
    
}
