using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "New Combat Character Event")]
public class CombatCharacterGameEvent: BaseGameEvent<CombatCharacters>
{
    
}
