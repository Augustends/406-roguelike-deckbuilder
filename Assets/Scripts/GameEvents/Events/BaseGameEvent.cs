using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class BaseGameEvent<T> : ScriptableObject
{
    private readonly List<IGameEventListener<T>> eventListeners = new List<IGameEventListener<T>>();

    public void Raise(T item)
    {
        for (int i = eventListeners.Count- 1; i >= 0; i--)
        {
            eventListeners[i].OnEventRaised(item);
        }
    }

    public void RegisterUser(IGameEventListener<T> user)
    {
        if (eventListeners.Contains(user))
        {
            Debug.Log("Warning: Already have user");
        }
        else
        {
            eventListeners.Add(user);
        }
    }

    public void UnregisterUser(IGameEventListener<T> user)
    {
        if (eventListeners.Contains(user))
        {
            eventListeners.Add(user);
        }
        else
        {
            Debug.Log("Warning: Cant unregister this user");
        }
    }
}

public abstract class BaseGameEvent<T1,T2> : ScriptableObject
{
    private readonly List<IGameEventListener<T1,T2>> eventListeners = new List<IGameEventListener<T1,T2>>();

    public void Raise(T1 item, T2 item2)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
        {
            eventListeners[i].OnEventRaised(item,item2);
        }
    }

    public void RegisterUser(IGameEventListener<T1,T2> user)
    {
        if (eventListeners.Contains(user))
        {
            Debug.Log("Warning: Already have user");
        }
        else
        {
            eventListeners.Add(user);
        }
    }

    public void UnregisterUser(IGameEventListener<T1,T2> user)
    {
        if (eventListeners.Contains(user))
        {
            eventListeners.Add(user);
        }
        else
        {
            Debug.Log("Warning: Cant unregister this user");
        }
    }
}
