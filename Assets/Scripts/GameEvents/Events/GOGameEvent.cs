using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "New GameObject Event")]
public class GOGameEvent: BaseGameEvent<GameObject>
{
    
}
