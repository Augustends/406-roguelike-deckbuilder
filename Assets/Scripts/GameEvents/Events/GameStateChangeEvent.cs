using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="New Game State Event")]
public class GameStateChangeEvent : BaseGameEvent<GameState,GameState>
{

}
