using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// 
/// </summary>
/// <typeparam name="T"></Param Type>
/// <typeparam name="E"></GameEvent>
/// <typeparam name="UER"></Unity Event Response>
public abstract class BaseGameEventListener<T, E, UER> : MonoBehaviour,
    IGameEventListener<T> where E: BaseGameEvent<T> where UER: UnityEvent<T>
{
    [SerializeField] private E gameEvent;
    public E GameEvent { get { return gameEvent; } set { gameEvent = value; } }

    [SerializeField] private UER unityEventResponse;


    private void OnEnable()
    {
        GameEvent.RegisterUser(this);
    }

    private void OnDisable()
    {
        GameEvent.UnregisterUser(this);
    }

    public void OnEventRaised(T item)
    {
        if (unityEventResponse!=null)
        {
            unityEventResponse.Invoke(item);
        }
    }
}

/// <summary>
/// 
/// </summary>
/// <typeparam name="T"></Param Type>
/// <typeparam name="T2"></Param Type>
/// <typeparam name="E"></GameEvent>
/// <typeparam name="UER"></Unity Event Response>
public abstract class BaseGameEventListener<T, T2, E, UER> : MonoBehaviour,
    IGameEventListener<T,T2> where E : BaseGameEvent<T,T2> where UER : UnityEvent<T,T2>
{
    [SerializeField] private E gameEvent;
    public E GameEvent { get { return gameEvent; } set { gameEvent = value; } }

    [SerializeField] private UER unityEventResponse;


    private void OnEnable()
    {
        GameEvent.RegisterUser(this);
    }

    private void OnDisable()
    {
        GameEvent.UnregisterUser(this);
    }

    public void OnEventRaised(T item, T2 item2)
    {
        if (unityEventResponse != null)
        {
            unityEventResponse.Invoke(item,item2);
        }
    }
}

