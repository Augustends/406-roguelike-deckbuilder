using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameEventListener<T>
{
    void OnEventRaised(T item);
}
public interface IGameEventListener<T,T2>
{
    void OnEventRaised(T item, T2 item2);
}
