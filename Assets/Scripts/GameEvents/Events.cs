
using UnityEngine.Events;

public class Events
{
    public enum FadeType
    {
        IN,
        OUT
    }

    [System.Serializable] public class RestartEvent : UnityEvent { };
}

