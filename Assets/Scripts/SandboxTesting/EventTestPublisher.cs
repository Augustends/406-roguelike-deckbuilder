using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventTestPublisher : MonoBehaviour
{
    public GOGameEvent Event;
    // Start is called before the first frame update
    public void Start()
    {
        newTurn();
    }

    public void newTurn()
    {
        Event.Raise(gameObject);
    }
    public void Update()
    {
        if (Input.anyKeyDown)
        {
            newTurn();
        }
    }
}
