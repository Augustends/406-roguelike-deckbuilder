using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class OverworldLayout : MonoBehaviour , ISavable
{
    GameObject elite;
    GameObject rand;
    GameObject enemy;
    GameObject camp;
    GameObject shop;
    bool loaded;

    [SerializeField] GOGameEvent VertexMadeEvent;

    private void Awake()
    {
        loaded = false;
        elite = Resources.Load<GameObject>("overworld/EncElite");
        rand = Resources.Load<GameObject>("overworld/EncRandom");
        enemy = Resources.Load<GameObject>("overworld/EncEnemy");
        camp = Resources.Load<GameObject>("overworld/EncCamp");
        shop = Resources.Load<GameObject>("overworld/EncShop");
    }

    public Graph levelLayout;
    // Start is called before the first frame update
    void Start()
    {
        if (!loaded)
        {
            Debug.Log("Generate New Overworld Layout");
            Generate();
        }
        levelLayout.GraphVisualsUpdate();
        // Else we have loaded the previos level layout
    }

    void Generate()
    {
        // Level tiers
        List<GameObject> round1 = new List<GameObject>();
        round1.Add(enemy);
        round1.Add(enemy);
        round1.Add(enemy);
        round1.Add(enemy);
        round1.Add(rand);

        List<GameObject> round2 = new List<GameObject>();
        round2.Add(shop);
        round2.Add(rand);
        round2.Add(enemy);
        round2.Add(enemy);
        round2.Add(enemy);

        List<GameObject> round3 = new List<GameObject>();
        round3.Add(elite);
        round3.Add(elite);
        round3.Add(elite);
        round3.Add(elite);

        List<GameObject> round4 = new List<GameObject>();
        round4.Add(camp);
        round4.Add(camp);

        List<GameObject> round5 = new List<GameObject>();
        round5.Add(shop);
        round5.Add(rand);
        round5.Add(camp);


        List<List<GameObject>> rounds = new List<List<GameObject>>(new List<GameObject>[] { round1, round2, round5, round1, round2, round4 , round3 });

        // 2D point grid
        float[] hPts = { -6, -3, 0, 3, 7, 11, 15,19,23,27,31,35,39,44};
        float[] vPts = { -3, -0.5f, 2 };
        Vector3[,] ptsGrid = new Vector3[,]
        {
            {new Vector3(hPts[0],vPts[0]),new Vector3(hPts[0], vPts[1]),new Vector3(hPts[0], vPts[2])},
            {new Vector3(hPts[1],vPts[0]),new Vector3(hPts[1], vPts[1]),new Vector3(hPts[1], vPts[2])},
            {new Vector3(hPts[2],vPts[0]),new Vector3(hPts[2], vPts[1]),new Vector3(hPts[2], vPts[2])},
            {new Vector3(hPts[3],vPts[0]),new Vector3(hPts[3], vPts[1]),new Vector3(hPts[3], vPts[2])},
            {new Vector3(hPts[4],vPts[0]),new Vector3(hPts[4], vPts[1]),new Vector3(hPts[4], vPts[2])},
            {new Vector3(hPts[5],vPts[0]),new Vector3(hPts[5], vPts[1]),new Vector3(hPts[5], vPts[2])},
            {new Vector3(hPts[6],vPts[0]),new Vector3(hPts[6], vPts[1]),new Vector3(hPts[6], vPts[2])}
        };
        ptsGrid = new Vector3[hPts.Length,vPts.Length];
        for (int i = 0; i < hPts.Length; i++)
        {
            for (int j = 0; j < vPts.Length; j++)
            {
                ptsGrid[i, j] = new Vector3(hPts[i], vPts[j]);
            }
        }

        List<GameObject> levelTier = new List<GameObject>();
        List<GameObject> levelHardCopy = new List<GameObject>();
        List<GameObject> prevLevelHardCopy = new List<GameObject>();



        GameObject newNode;
        for (int i = 0; i < hPts.Length; i++)
        {
            levelTier.Clear();
            levelHardCopy.Clear();
            for (int j = 0; j < vPts.Length; j++)
            {
                levelTier.Add(rounds[i%rounds.Count][Random.Range(0, rounds[i % rounds.Count].Count)]); // Modulo is used to cycle over rounds list
                if (j == 0)
                {
                    newNode = Instantiate(levelTier[0]);
                    newNode.transform.position = ptsGrid[i, j];
                    newNode.transform.SetParent(levelLayout.transform);
                    levelHardCopy.Add(newNode);
                    levelLayout.AddVertex(newNode.GetComponent<Vertex>(),i);
                    if (i != 0) // Not first tier
                    {
                        levelLayout.AddEdge(newNode.GetComponent<Vertex>(), prevLevelHardCopy[0].GetComponent<Vertex>());
                    }
                }
                else
                {
                    if (levelTier[j] == levelTier[j - 1] && i%rounds.Count != 0)
                    {
                        levelHardCopy.Add(levelHardCopy[j - 1]);
                        // Offset multiply linked nodes upwards
                        levelHardCopy[j].transform.position += Vector3.up * Random.Range(0, 1.5f);
                    }
                    else
                    {
                        newNode = Instantiate(levelTier[j]);
                        newNode.transform.position = ptsGrid[i, j];
                        newNode.transform.SetParent(levelLayout.transform);
                        levelHardCopy.Add(newNode);
                        levelLayout.AddVertex(newNode.GetComponent<Vertex>(),i);
                    }
                    if (i == 0)
                        continue; // Done work for first tier. No edges to add
                    try
                    {
                        levelLayout.AddEdge(levelHardCopy[j].GetComponent<Vertex>(), prevLevelHardCopy[j].GetComponent<Vertex>());
                    }
                    catch (System.ArgumentException e)
                    {

                    }

                    levelHardCopy[j].transform.position += Vector3.right * Random.Range(-0.75f, 0.65f);  // Not needed, but adds some immediate position variance to keep it from looking scripted
                }
                //Debug.Log(i + "i, j:" +j);
                // Add Collapse here
            }

            prevLevelHardCopy = new List<GameObject>(levelHardCopy);
        }

        foreach (var vert in levelLayout.Vertices)
        {
            VertexMadeEvent.Raise(vert.gameObject);
        }
    }

    protected void OnDestroy()
    {
        SaveState();
    }

    public void SaveState()
    {
        OverworldStateData data = Resources.Load<OverworldStateData>("OverworldData");
        data.zoneLayout = new GraphData(levelLayout);
        data.level_name = "TODOIMPLEMENT";
    }

    public void LoadState()
    {
        Debug.Log("Load State");
        OverworldStateData data = Resources.Load<OverworldStateData>("OverworldData");
        if (data.level_name.Equals("fresh"))
        {
            return; // Can't load from fresh file
        }
        levelLayout.Clear();
        GenerateFromData(data.zoneLayout);
        // Set current vertex. Should really move this into the layout
        try
        {
            OverworldController.Instance.CurrentVertex = levelLayout.GetVertByID(data.current_vertex_id.id);
            OverworldController.Instance.CurrentVertex.ColorNode();
        }
        catch (System.ArgumentException e)
        {
            Debug.Log(e.Message + ": Propably cause by exiting play mode in Overworld scene, not letting the current node be saved. Unlikely to be game breaking");
        }
        loaded = true;
    }

    public void GenerateFromData(GraphData zoneLayout)
    {
        foreach (var vdata in zoneLayout.vertices)
        {
            levelLayout.AddVertex(CreateVertexFromData(vdata),vdata.level);
        }
        foreach (var edata in zoneLayout.edges)
        {
            levelLayout.AddEdge(levelLayout.GetVertByID(edata.idFrom), levelLayout.GetVertByID(edata.idTo));
        }
    }
    public Vertex CreateVertexFromData(VertexData data)
    {
        GameObject newNode = null;
        switch (data.type)
        {
            case EncounterType.NORMAL:
                newNode = Instantiate(enemy);
                break;
            case EncounterType.ELITE:
                newNode = Instantiate(elite);
                break;
            case EncounterType.SHOP:
                newNode = Instantiate(shop);
                break;
            case EncounterType.CAMP:
                newNode = Instantiate(camp);
                break;
            case EncounterType.RANDOM:
                newNode = Instantiate(rand);
                break;
        }
        Vertex newVert = newNode.GetComponent<Vertex>();
        newVert.id = data.id;
        newVert.type = data.type;
        newNode.transform.position = data.position;
        newVert.level = data.level;
        newVert.encounterData = data.encounterDataSO;
        newNode.transform.SetParent(this.transform);
        newVert.isColored = data.isColored;
        return newVert;
    }
}
