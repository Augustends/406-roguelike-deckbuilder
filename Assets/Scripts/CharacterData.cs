using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CharacterSO/Character")]
[SerializeField]
public class CharacterData : ScriptableObject
{
    public string character_name = "Default";
    public GameObject prefab;
    public GameObject profile_prefab;
    public int max_health, cur_health;
    public Sprite idleSprite;
    public Sprite tokenSprite;
    //The deck this character starts with
    public List<Card> default_deck = new List<Card>();
    //temporary structure to hold all the cards
    public List<Card> full_card_list = new List<Card>();
    //temporary structure to hold the character's acquired deck
    public List<Card> character_deck = new List<Card>();
    

    public void AddCard(Card card)
    {
        character_deck.Add(card);
        Debug.Log(card.name + " was added to " + character_name + "'s deck!");
    }

    public void RemoveCard(Card card)
    {
        character_deck.Remove(card);
        Debug.Log(card.name + " was removed from " + character_name + "'s deck!");
    }

    public void Reset()
    {
        Debug.Log(character_deck);
        cur_health = max_health;
        character_deck = new List<Card>(default_deck);
    }
}
