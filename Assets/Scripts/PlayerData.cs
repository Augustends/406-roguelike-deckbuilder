using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CharacterSO/PlayerSO")]
public class PlayerData : ScriptableObject
{
    public List<CharacterData> party_members = new List<CharacterData>();
    public List<CharacterData> character_list = new List<CharacterData>();
    //The player's current money
    public int money;
    public void Recruit(CharacterData character)
    {
        if (party_members.Contains(character))
        {
            throw new System.ArgumentException("Character is in party already");
        }
        if(party_members.Count >= 3)
        {
            throw new System.ArgumentException("Party is full");
        }
        party_members.Add(character);
    }
    public void Dismiss(CharacterData character)
    {
        if (!party_members.Contains(character))
        {
            throw new System.ArgumentException("Character is not in party");
        }
        party_members.Remove(character);
    }
    public void Clear()
    {
        party_members = new List<CharacterData>();
    }

    //Changes the current amount of money
    //Input
    //amount: an integer that will alter the amount of money the player currently has
    public void AddMoney(int amount)
    {
        money += amount;
        if(money < 0)
        {
            money = 0;
        }
        if(money > 99999)
        {
            money = 99999;
        }
    }
    public void Reset()
    {
        Clear();
        money = 100;
        foreach (var p in character_list)
        {
            p.Reset();
        }
    }
}
