using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


/// <summary>
/// Loads combat prefab systems
/// Responsible for loading/unloading combats
/// Loads/Unloads Combatants. Including party and enemies
/// Keeps track of combat state
/// </summary>
public class CombatController : Singleton<CombatController>
{
    public GameObject[] systemPrefabs;
    private List<GameObject> instancedSystemPrefabs;
    void InstantiateSystemPrefabs()
    {
        instancedSystemPrefabs = new List<GameObject>();
        for (int i = 0; i < systemPrefabs.Length; i++)
        {
            instancedSystemPrefabs.Add(Instantiate(systemPrefabs[i],transform)); // Instance the public inspector list of desired game systems and track them
        }
    }


    [SerializeField] EncounterDataSO combatEncounter;
    [SerializeField] PlayerData playerData;
    [SerializeField] GameObject rewardScreen;


    [SerializeField]List<PlayerCharacter> party; // Active party members
    [SerializeField]List<EnemyCharacter> enemies; // Active enemies
    public List<PlayerCharacter> Party { get => party;}
    public List<EnemyCharacter> Enemies { get => enemies;}
    public List<CombatCharacters> AllCharacters { get {
            var result = new List<CombatCharacters>(enemies);
            result.AddRange(new List<CombatCharacters>(party));
            return result ; } }


    public enum CombatState
    {
        PRECOMBAT,
        INCOMBAT,
        WON,
        LOST
    }
    protected CombatState state = CombatState.PRECOMBAT;
    public CombatState State { get => state;}


    private float groundPosition = -1.2f;

    protected override void Awake()
    {
        base.Awake();
        party = new List<PlayerCharacter>();
        enemies = new List<EnemyCharacter>();
    }


    void Start()
    {
        //UpdateCharacterListsBySceneHierarchy();
        LoadCombat();
        AudioManager.Instance.StopPlaying(AudioManager.Instance.GetCurrentSong()); //Stops playing the song playing while the stage was loading.(Simon)
        AudioManager.Instance.PlaySong("CombatTheme"); //Forest theme is played as that is the current area avaliable. (Simon)
    }

    void LoadEnemies(EncounterDataSO encounterData)
    {
        for (int i = 0; i < encounterData.enemyPrefabs.Length; i++)
        {
            var enemy_prefab = encounterData.enemyPrefabs[i];
            if (enemy_prefab == null)
            {
                Debug.LogError("Prefab list of encounter SO has a null element. Dounble check the encounter SO");
                continue;
            }
            AddEnemyToCombat(enemy_prefab, GetEnemyPositionByData(encounterData,i));
        }
    }

    public void AddEnemyToCombat(EnemyCharacter e_prefab, Vector3 position)
    {
        //TODO add check to ensure prefab has component type needed
        EnemyCharacter instancedEnemy = Instantiate(e_prefab).GetComponent<EnemyCharacter>();
        enemies.Add(instancedEnemy);
        instancedEnemy.transform.position = position;
        //PositionEnemyAuto(instancedEnemy);
    }

    public void RemoveEnemyFromCombat(EnemyCharacter e)
    {
        enemies.Remove(e);
    }

    public void RemovePlayerFromCombat(PlayerCharacter e)
    {
        party.Remove(e);
        e.CharacterData.cur_health = (int)(e.health.getMaxHealth()*0.25f);
    }

    /// <summary>
    /// Positions enemies alternating left or right based on collider size
    /// </summary>
    /// <param name="enemyCharacter"></param>
    void PositionEnemyAuto(EnemyCharacter enemyCharacter)
    {
        Vector3 pos = new Vector3(3, -2, 0); // First enemy position
        int i = Enemies.IndexOf(enemyCharacter);
        if (i!=0)
        {
            int l_or_r = ((i % 2) * 2 - 1); // will be -1  or 1
            int neighbour_index = Mathf.Clamp(i - 2, 0, i);
            pos = Enemies[neighbour_index].transform.position;
            pos += Enemies[neighbour_index].GetComponent<BoxCollider2D>().size.x * Vector3.right * l_or_r;
            pos += enemyCharacter.GetComponent<BoxCollider2D>().size.x * Vector3.right * l_or_r;
        }
        Debug.Log(i);
        enemyCharacter.transform.position = pos;
    }

    Vector3 GetEnemyPositionByData(EncounterDataSO enc_data, int enemyIndex)
    {
        try
        {
            return enc_data.enemyXPosition[enemyIndex] * Vector3.right + groundPosition * Vector3.up;
        }
        catch (System.IndexOutOfRangeException e)
        {
            Debug.LogError("Index of enemy is out of range in encounter data. Encounter Scriptable Object not compliant. Ask Duncan");
            throw;
        }
        catch (System.NullReferenceException e)
        {
            Debug.LogError("Encounter Scriptable Object X Position list is not made. Not compliant. Ask Duncan");
            throw;
        }
    }

    void LoadParty(PlayerData d)
    {
        foreach (var member in d.party_members)
        {
            if (member == null)
            {
                Debug.LogError("Prefab list of Player Data has a null element. Incomplete Party Prefab list");
                continue;
            }
            PlayerCharacter partymember = Instantiate(member.prefab).GetComponent<PlayerCharacter>();
            partymember.GetComponent<Health>().setCurrentHealth(member.cur_health);
            party.Add(partymember);
        }
        for (int i = 0; i < party.Count; i++)
        {
            party[i].Init(d.party_members[i]);
            party[i].transform.position = (2*i - 6) * Vector3.right + groundPosition * Vector3.up ;
        }
    }


    /// <summary>
    /// Clears party and enemy list and repopulates based on scene Hierarchy and Active gameobjects
    /// Author Duncan
    /// </summary>
    void UpdateCharacterListsBySceneHierarchy()
    {

        var partyGO = new List<GameObject>(GameObject.FindGameObjectsWithTag("Player"));
        var enemyGO = new List<GameObject>(GameObject.FindGameObjectsWithTag("Enemy"));

        party.Clear();
        enemies.Clear();

        //Refill party
        foreach (var go in partyGO)
        {
            var p = go.GetComponent<PlayerCharacter>();
            if (p == null)
            {
                Debug.LogError("Error: Gameobject is tagged as Player yet has no PlayerCharacter component: " + go.name);
            }
            else { party.Add(go.GetComponent<PlayerCharacter>()); }
        }

        //Refill enemies
        foreach (var go in enemyGO)
        {
            var e = go.GetComponent<EnemyCharacter>();
            if (e == null)
            {
                Debug.LogError("Error: Gameobject is tagged as Enemy yet has no EnemyCharacter component: " + go.name);
            }
            else { enemies.Add(go.GetComponent<EnemyCharacter>()); }
        }
    }

    public void HandleDeath(CombatCharacters combatCharacter)
    {
        if (combatCharacter is EnemyCharacter)
        {
            if (Enemies.Contains((EnemyCharacter)combatCharacter))
            {
                if (combatCharacter.transform.GetComponent<EnemyCharacter>() != null && combatCharacter.transform.GetComponent<EnemyCharacter>().EnemyName == "Dragon")
                {
                    GameController.Instance.Victory();
                }
                RemoveEnemyFromCombat((EnemyCharacter)combatCharacter);
                // If the dead character dies is the active character, end it's turn
                if (TurnMechanic.Instance.ActiveCharacter == combatCharacter)
                {
                    combatCharacter.endTurn();
                }
                return;
            }
        }
        if (combatCharacter is PlayerCharacter)
        {
            if (Party.Contains((PlayerCharacter)combatCharacter))
            {
               RemovePlayerFromCombat((PlayerCharacter)combatCharacter);
                // If the dead character dies is the active character, end it's turn
                if (TurnMechanic.Instance.ActiveCharacter == combatCharacter)
                {
                    CardController.Instance.handlePlayerEndOfTurn(combatCharacter);//Hide all their cards
                    TurnMechanic.Instance.EndPlayerTurn();//end their turn
                }
                return;
            }
        }
        Debug.LogError("Tried to remove character that is not being tracked by the combat controller");
    }

    void LoadCombat()
    {
        InstantiateSystemPrefabs();
		if (playerData != null)
			LoadParty(playerData);
		else
			Debug.LogError("Error while loading combat - missing player data. May need to insert in editor.");
		if (combatEncounter != null )
			LoadEnemies(combatEncounter);
		else
			Debug.LogError("Error while loading combat - missing encounter data. May need to insert in editor.");
        Debug.Log("Combatants loaded into scene");

        Invoke("BeginCombat", 0.75f);
		Debug.Log("CombatController finished loading combat");
		//Debug.Log(party[1].name + "'s deck: " + party[1].PlayerDeck);
    }

    void BeginCombat()
    {
        state = CombatState.INCOMBAT;
    }

    void EndCombat()
    {
        StartCoroutine(EndCombatRoutine());
    }

    /* When battle ends, loads out of scene
 * @return: utilized to yield execution of code temporarily
 * @author: Travis Baldwin
 */
    IEnumerator EndCombatRoutine()
    {

        // Need to assign when player has won and lost
        if (Enemies.Count <= 0)
        {
            state = CombatState.WON;
        }
        if (Party.Count <= 0)
        {
            state = CombatState.LOST;
        }


        if (state == CombatState.WON)
        {
            //save party member's health
            foreach(PlayerCharacter pc in party)
            {
                pc.CharacterData.cur_health = pc.health.getCurrentHealth();
                CardController.Instance.hideCardsAtEndOfCombat(pc);//Hide all their cards
            }
            // Placeholder output
            yield return new WaitForSeconds(0.2f);
            //Debug.Log("!You won the battle!");
            // Load out animation
            yield return new WaitForSeconds(2f);
            GameObject rewards = GameObject.Instantiate(rewardScreen);
            yield break;
        }
        else if (state == CombatState.LOST)
        {
            foreach (PlayerCharacter pc in party)
            {
                pc.removeActiveEffects(Active_Effect_Type.NONE);//Remove all active effects from players
                pc.CharacterData.cur_health = pc.health.getCurrentHealth();
                CardController.Instance.hideCardsAtEndOfCombat(pc);//Hide all their cards
            }
            // Placeholder output
            yield return new WaitForSeconds(0.4f);
            Debug.Log(":( You lost the battle :(");
            FadesManager.Instance.FadeOutThen(delegate { GoGameOver(); });
            yield break;
        }
        Debug.LogWarning("Should not try to end the combat when there are still characters on both sides!");
    }

    public void GoGameOver()
    {
        GameController.Instance.GameOver();
    }

    public void SaveState()
    {
        throw new System.NotImplementedException();
    }

    private void Update()
    {
        if ( state == CombatState.INCOMBAT && (Enemies.Count == 0 || Party.Count == 0))
        {
            EndCombat();
        }
    }


}

