using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public enum Energy_Type {NONE, WATER, WIND, FIRE, EARTH };
/// <summary>
/// Manages increases and decreases to energy values and their display on the UI
/// </summary>
public class EnergyManager : Singleton<EnergyManager>
{
    TextMeshProUGUI water_ui_value;
    TextMeshProUGUI wind_ui_value;
    TextMeshProUGUI fire_ui_value;
    TextMeshProUGUI earth_ui_value;
    [SerializeField]Animation water_animation;
    [SerializeField] Animation wind_animation;
    [SerializeField] Animation fire_animation;
    [SerializeField] Animation earth_animation;

    [SerializeField]
    int water_int;
    [SerializeField]
    int wind_int;
    [SerializeField]
    int fire_int;
    [SerializeField]
    int earth_int;


    // Start is called before the first frame update
    void Start()
    {
        // Assign TextMeshProUGUI references
        water_ui_value = transform.Find("Water").GetChild(0).GetComponent<TextMeshProUGUI>();
        wind_ui_value = transform.Find("Wind").GetChild(0).GetComponent<TextMeshProUGUI>();
        fire_ui_value = transform.Find("Fire").GetChild(0).GetComponent<TextMeshProUGUI>();
        earth_ui_value = transform.Find("Earth").GetChild(0).GetComponent<TextMeshProUGUI>();

        // might not need to assign here, could just assign from the editor which value to start with

        water_int = 0;
        wind_int = 0;
        fire_int = 0;
        earth_int = 0;

        updateUiText();
    }

    // Update is called once per frame
    void Update()
    {
        //// Manual Testing functionality*** Can remove when not needed***
        //if (Input.GetKeyDown(KeyCode.Q))
        //{
        //    increaseEnergy(Energy_Type.WATER, 3);
        //}
        //if (Input.GetKeyDown(KeyCode.W))
        //{
        //    decreaseEnergy(Energy_Type.WATER, 1);
        //}
        //if (Input.GetKeyDown(KeyCode.E))
        //{
        //    decreaseEnergyEndOfRound();
        //}
        //if (Input.GetKeyDown(KeyCode.R))
        //{
        //    decreaseAllEnergy(2);
        //}
        //if (Input.GetKeyDown(KeyCode.T))
        //{
        //    increaseAllEnergy(5);
        //}
    }

    /*
     * Getter for the amounts of each energy type
     * @param: energy_type - type of energy to get amount of
     * @return: int amount of given energy type
     * @author: Tanner Skomar
     */
    public int getEnergyAmount(Energy_Type energy_type)
    {
        switch (energy_type)
        {
            case Energy_Type.WATER:
                return water_int;
            case Energy_Type.WIND:
                return wind_int;
            case Energy_Type.FIRE:
                return fire_int;
            case Energy_Type.EARTH:
                return earth_int;
            default: //Energy_Type.None
                return 0;
        }
    }

    /*
     * Used to check if the given amount of the given Energy_Type is available
     * @param: energy_type - type of energy check 
     * @param: amount_required - amount of energy needed
     * @return: true if sufficient energy is available, else false
     * @author: Tanner Skomar
     */
    public bool isEnergyAvailable(Energy_Type energy_type, int amount_required)
    {
        return getEnergyAmount(energy_type) >= amount_required;
    }

    /// <summary>
    /// Increases the value of the specified Energy_Type
    /// </summary>
    /* @param: energy_type - the specified energy type from enum Energy_Type
     * @param: amount - the amount to increase energy by
     * @return: void
     * @author: Travis Baldwin
     */
    public void increaseEnergy(Energy_Type energy_type, int amount)
    {
        switch (energy_type)
        {
            case Energy_Type.WATER:
                water_int += amount;
                break;
            case Energy_Type.WIND:
                wind_int += amount;
                break;
            case Energy_Type.FIRE:
                fire_int += amount;
                break;
            case Energy_Type.EARTH:
                earth_int += amount;
                break;
            default:
                break;
        }
        updateUiText();
    }

    /// <summary>
    /// Increases the value of all energy types
    /// </summary>
    /* @param: amount - the amount to increase energy by
     * @return: void
     * @author: Travis Baldwin
     */
    public void increaseAllEnergy(int amount)
    {
        water_int += amount;
        wind_int += amount;
        fire_int += amount;
        earth_int += amount;

        updateUiText();
    }

    /// <summary>
    /// Decreases the value of the specified Energy_Type
    /// </summary>
    /* @param: energy_type - the specified energy type from enum Energy_Type
     * @param: amount - the amount to decrease energy by
     * @return: false if not enough energy available to consume, true if successfully consumed energy
     * @author: Travis Baldwin
     */
    public bool decreaseEnergy(Energy_Type energy_type, int amount)
    {
        switch (energy_type)
        {
            case Energy_Type.WATER:
                if (water_int - amount < 0)
                {
                    return false;
                }
                water_int -= amount;
                break;
            case Energy_Type.WIND:
                if (wind_int - amount < 0)
                {
                    return false;
                }
                wind_int -= amount;
                break;
            case Energy_Type.FIRE:
                if (fire_int - amount < 0)
                {
                    return false;
                }
                fire_int -= amount;
                break;
            case Energy_Type.EARTH:
                if (earth_int - amount < 0)
                {
                    return false;
                }
                earth_int -= amount;
                break;
            default:
                // Warning log message incase code somehow executes this default
                Debug.LogWarning("Defaulted switch statement source: decreaseEnergy()" +
                    " -- Values passed: energy_type = " + energy_type + " amount = " + amount);
                return false;
        }
        updateUiText();
        return true;
    }

    /// <summary>
    /// Decreases the value of all energies by 1, to be called at the end of a round
    /// </summary>
    /* @return: void
     * @author: Travis Baldwin
     */
    public void decreaseEnergyEndOfRound()
    {
        if (!(water_int <= 0))
        {
            water_int -= 1;
        }
        if (!(wind_int <= 0))
        {
            wind_int -= 1;
        }
        if (!(fire_int <= 0))
        {
            fire_int -= 1;
        }
        if (!(earth_int <= 0))
        {
            earth_int -= 1;
        }
        updateUiText();
    }

    /// <summary>
    /// Decreases the value of all energies by specified amount
    /// </summary>
    /* @param: amount - the amount to decrease energy by
     * @return: false if not enough energy available to consume, true if successfully consumed energy
     * @author: Travis Baldwin
     */
    public bool decreaseAllEnergy(int amount)
    {
        if ((water_int - amount) < 0)
        {
            return false;
        }
        if ((wind_int - amount) < 0)
        {
            return false;
        }
        if ((fire_int - amount) < 0)
        {
            return false;
        }
        if ((earth_int - amount) < 0)
        {
            return false;
        }

        water_int -= amount;
        wind_int -= amount;
        fire_int -= amount;
        earth_int -= amount;

        updateUiText();
        return true;
    }

    /// <summary>
    /// Sets the specified Energy_Type to a value
    /// </summary>
    /* @param: energy_type - the specified energy type from enum Energy_Type
     * @param: set_to_value - the value to set the energy type to
     * @return: void
     * @author: Travis Baldwin
     */
    public void setEnergy(Energy_Type energy_type, int set_to_value)
    {
        switch (energy_type)
        {
            case Energy_Type.WATER:
                water_int = set_to_value;
                break;
            case Energy_Type.WIND:
                wind_int = set_to_value;
                break;
            case Energy_Type.FIRE:
                fire_int = set_to_value;
                break;
            case Energy_Type.EARTH:
                earth_int = set_to_value;
                break;
            default:
                break;
        }
        updateUiText();
    }

    /*Checks if the UI was found in the scene, The text should not be updated if it is not there
     * @return: true if requred TextMeshProUGUI could be found else false
     * @author: Tanner Skomar
     */
    private bool uiFound()
    {
        if (water_ui_value != null && wind_ui_value != null && fire_ui_value != null && earth_ui_value != null)
        {
            return true;
        }
        Debug.LogWarning("At least one of the Required Energy TextMeshProUGUI Could not be found. UI Will not be updated");
        return false;
    }

    /* Use this to update the ui text instead of doing each one manually
     * Will only update if the text is in the scene
     * evertime their is a change
     * @return: void
     * @author: Tanner Skomar
     */
    private void updateUiText()
    {
        if (uiFound())
        {
            if (!water_ui_value.text.Equals(water_int.ToString()))
            {
                water_ui_value.text = water_int.ToString();
                water_animation.Play("WaterTokenAnim");
            }

            if (!wind_ui_value.text.Equals(wind_int.ToString()))
            {
                wind_ui_value.text = wind_int.ToString();
                wind_animation.Play("WindTokenAnim");
            }
            if (!fire_ui_value.text.Equals(fire_int.ToString()))
            {
                fire_ui_value.text = fire_int.ToString();
                fire_animation.Play("FireTokenAnim");
            }
            if (!earth_ui_value.text.Equals(earth_int.ToString()))
            {
                earth_ui_value.text = earth_int.ToString();
                earth_animation.Play("EarthTokenAnim");
            }
        }
    }


    /* Increases the value of random energy by amount
     * @param: amount - the amount to increase energy by
     * @return: void
     * @author: Tanner Skomar
     */
    public void increaseRandomEnergy(int amount)
    {
        switch (Random.Range(0, 4)) //pick random number from (0,1,2,3)
        {
            case 0:
                water_int += amount;
                break;
            case 1:
                wind_int += amount; ;
                break;
            case 2:
                fire_int += amount;
                break;
            case 3:
                earth_int += amount;
                break;
            default:
                break;
        }

        updateUiText();
    }

}
