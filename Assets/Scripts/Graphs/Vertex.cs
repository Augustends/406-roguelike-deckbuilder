using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class Vertex : MonoBehaviour
{
    public GOGameEvent EncounterSelectedEvent;

    [SerializeField] public EncounterType type;
    [SerializeField] public int level;
    [SerializeField] public string id;
    [SerializeField] public EncounterDataSO encounterData;
    [SerializeField] public bool isColored;


    // Start is called before the first frame update
    void Start()
    {
        id = System.Guid.NewGuid().ToString();
        if (isColored)
        {
            ColorNode();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    Coroutine pulsate;

    void OnMouseEnter()
    {
        if (pulsate==null)
        {
            pulsate = StartCoroutine(Pulsate(5,6,1));
        }
    }

    void OnMouseDown()
    {
        if (IsSelectable())
        {
            StopAllCoroutines();
            pulsate =  StartCoroutine(Pulsate(3, 8, 1));
            // Broadcast Selection
            EncounterSelectedEvent.Raise(gameObject);
        }
    }

    private bool IsSelectable()
    {
        var graph = GetComponentInParent<Graph>();
        var selectables = graph.SelectableVertices();
        return selectables.Contains(this);
    }

    void OnTriggerExit2D(Collider2D collision)
    {
    }


    public void ColorNode()
    {
        isColored = true;
        transform.Find("X")?.gameObject.SetActive(true);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="amp"></param> Bigger number is smaller amplitude. Counter intuitive
    /// <param name="freq"></param>
    /// <returns></returns>
    private IEnumerator Pulsate(float amp, float freq ,float time)
    {
        float curTime = 0;
        while (curTime < time)
        {
            transform.localScale = Vector3.Lerp(transform.localScale,
                Vector3.one * (Mathf.Cos(freq*curTime - 3.14f/2)* Mathf.Cos(freq*curTime - 3.14f / 2) /amp +1),
                0.3f);
            curTime += Time.deltaTime;
            yield return null;
        }
        transform.localScale = Vector3.one;
        pulsate = null;
    }

    public void PulsateCommand(float amp, float freq, float time)
    {
        StartCoroutine(Pulsate(amp, freq, time));
    }

    //bool isSelectable()
    //{
     //   return OverworldController.Instance.IsSelectable(this);
    //}
}
[System.Serializable]
public class VertexData
{
    [SerializeField] public Vector3 position;
    [SerializeField] public EncounterType type;
    [SerializeField] public int level;
    [SerializeField] public string id;
    [SerializeField] public EncounterDataSO encounterDataSO;
    [SerializeField] public bool isColored;

    public VertexData(Vertex v)
    {
        position = v.transform.position;
        type = v.type;
        level = v.level;
        id = v.id;
        encounterDataSO = v.encounterData;
        isColored = v.isColored;
    }

}
