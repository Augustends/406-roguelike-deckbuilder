using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Edge : MonoBehaviour
{
    public Vertex From;
    public Vertex To;
    LineRenderer line;

    public static Edge newEdge(Vertex from, Vertex to)
    {
        var go = new GameObject();
        go.name = "E:" + from.transform.name + "-" + to.transform.name;
        go.transform.position = (from.transform.position + to.transform.position) / 2;
        var e = go.AddComponent<Edge>();
        e.Init(from, to);
        return e;
    }

    public void Init(Vertex from, Vertex to)
    {
        From = from;
        To = to;
    }

    

    // Start is called before the first frame update
    void Start()
    {
        var pr = Instantiate(Resources.Load<GameObject>("overworld/PathRenderer"));
        line = pr.GetComponent<LineRenderer>();
        line.SetPosition(0, From.transform.position);
        line.SetPosition(1, To.transform.position);
        pr.transform.SetParent(transform);
    }

    // Update is called once per frame
    void Update()
    {
        line.SetPosition(0, From.transform.position);
        line.SetPosition(1, To.transform.position);
    }
}

[System.Serializable]
public class EdgeData
{
    [SerializeField] public string idFrom;
    [SerializeField] public string idTo;

    public EdgeData(Edge e)
    {
        idFrom = e.From.id;
        idTo = e.To.id;
    }
}
