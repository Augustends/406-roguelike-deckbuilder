using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Graph : MonoBehaviour
{
    [SerializeField] public Dictionary<Vertex, List<Vertex>> adjacencyList;
    [SerializeField] public List<Edge> edgeList;
    [SerializeField] public int verticesCount;

    public IEnumerable<Vertex> Vertices => adjacencyList.Keys;

    private void Awake()
    {
        adjacencyList = new Dictionary<Vertex, List<Vertex>>();
        edgeList = new List<Edge>();
        verticesCount = 0;
    }

    private void Start()
    {
        // Update graph visuals
        
    }

    public void AddVertex(Vertex v,int level)
    {
        if (adjacencyList.ContainsKey(v))
        {
            throw new System.ArgumentException("Vertex exists already " + v.transform.name);
        }
        adjacencyList.Add(v, new List<Vertex>());
        v.level = level*2;
    }
    
    public void AddEdge(Vertex from, Vertex to)
    {
        // We record the edge in two places: Adj List and in the Scene with Gameobjects
        // Update adjacency list
        if (!adjacencyList.TryGetValue(from,out var fromList) || !adjacencyList.TryGetValue(to, out var toList))
            throw new System.ArgumentException("One of the vertices does not exist");
        if (fromList.Contains(to))
            throw new System.ArgumentException("The edge already exist");

        fromList.Add(to);
        toList.Add(from);

        // Add Gameobject representation of edge to scene
        Edge e = Edge.newEdge(from, to);
        edgeList.Add(e);
        e.transform.SetParent(transform);
    }

    public bool HasEdge(Vertex from, Vertex to)
    {
        return adjacencyList[from].Contains(to);
    }

    IEnumerable<Vertex> GetNeighbours(Vertex v)
    {
        if (!adjacencyList.TryGetValue(v, out var neighbours))
            throw new System.ArgumentException("Vertex does not exist:" + v.transform.name);
        return neighbours;
    }

    /// <summary>
    /// Forward neighbours on relative to the x axis
    /// </summary>
    /// <param name="v"></param>
    /// <returns></returns>
    public IEnumerable<Vertex> GetForwardNeighbours(Vertex v)
    {
        IEnumerable<Vertex> fneighbours = GetNeighbours(v).Where(neighbour => neighbour.transform.position.x >= v.transform.position.x);
        return fneighbours;
    }

    private IEnumerable<Edge> GetEdges()
    {
        return edgeList;
    }

    public Vertex GetVertByID(string id)
    {
        foreach (var v in Vertices)
        {
            if (v.id.Equals(id))
            {
                return v;
            }
        }
        throw new System.ArgumentException("Vertice ID not in graph");
    }

    private GraphData Save()
    {
        return new GraphData(this);
    }

    public void Clear()
    {
        foreach (var v in Vertices)
        {
            Destroy(v.gameObject);
        }
        foreach (var edge in edgeList)
        {
            Destroy(edge.gameObject);
        }
        adjacencyList = new Dictionary<Vertex, List<Vertex>>();
        edgeList = new List<Edge>();
        verticesCount = 0;
    }

    public void GraphVisualsUpdate()
    {
        /*
         * Pulsate Selectable nodes 
         */
        foreach (var next in SelectableVertices())
        {
            next.PulsateCommand(4,4,160);
        }

    }

    /// <summary>
    /// Get a list of all selecatble nodes from the current player overworld node
    /// Duncan
    /// </summary>
    /// <returns></returns>
    public Vertex[] SelectableVertices()
    {
        Vertex playerVertex = OverworldController.Instance.CurrentVertex;
        Vertex[] nextVertices = null;
        if (playerVertex == null)
        {
            nextVertices = Vertices.Take(3).ToArray(); // Get first 3 vertices
        }
        // Pulsate nodes neighbouring current vertex that are further on x. VERY HACKY
        else
        {
            nextVertices = GetForwardNeighbours(playerVertex).ToArray();
        }
        return nextVertices;
    }

}

//For saving to JSON
[Serializable]
public class GraphData
{
    public List<VertexData> vertices;
    public List<EdgeData> edges;
    public GraphData(Graph g)
    {
        vertices = new List<VertexData>();
        edges = new List<EdgeData>();
        foreach (var v in g.Vertices)
        {
            vertices.Add(new VertexData(v));
        }
        foreach (var e in g.edgeList)
        {
            edges.Add(new EdgeData(e));
        }
    }
}
