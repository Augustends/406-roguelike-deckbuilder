using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public enum Character_Type { PLAYER, ENEMY }

public class CombatCharacters : MonoBehaviour
{
    public Character_Type character_type;
    public Health health;
    [SerializeField]
    protected int base_speed;
    protected int speed_modifer = 0; //this gets added to base_speed for current speed

    protected int defense_modifer = 0; //this should be subtracted from damage recieved
    protected int attack_modifer = 0; //this should be added to damage dealt
    protected int poison_modifier = 0; //the number of turns remaining for poison, if ==0 then you can heal again

    //NOTE: these 2 should never be set on rustbot, he will get stuck in a loop trying to funnel damage to himself
    protected bool defense_matrix_active = false; //if true damage will be funneled to rustbot if he is alive
    protected CombatCharacters rustbot_ref; //Put a rustbot ref here when defense matrix is activated for easy reference

    [SerializeField]
    protected int starting_armour = 0; //Armour default when starting combat

    public SwarmManager swarm_manager; //Used by to keep track of bees applied by queen bee


    protected bool active = false;
    protected string turnDescription = "";
    [SerializeField] protected CombatCharacterGameEvent StartTurnEvent;
    public Sprite UITurnToken;

    // used while displaying the status effect UI elements
    public GameObject status_effect_display_prefab;
    private Transform status_effect_char_root;
    private Transform status_effect_display_root1;
    private Transform status_effect_display_root2;

    private int status_effect_display_count;


    [SerializeField]
    private GameObject FloatingTextEffectPrefab;

    /* Getter for character_type
     * @return: enum of the characters type, player or enemy 
     * @author: Travis Baldwin
     */
    public Character_Type getCharacter_Type()
    {
        return character_type;
    }

    /* Getter for current speed
     * SHould be used for turn order
     * @return: int representing base_speed + speed_modifer 
     * @author: Tanner Skomar
     */
    public int getCurrentSpeed()
    {
        return base_speed + speed_modifer;
    }

    /*
     * Add the given amount to the speed modifier
     * @param: amount - the amount to add to the modifier.
     * @author: Tanner Skomar
     */
    public void modifySpeed(int amount)
    {
        speed_modifer += amount;
    }

    /* Getter for attack_modifer
     * @return: int representing attack_modifer
     * @author: Tanner Skomar
     */
    public int getAttackMod()
    {
        return attack_modifer;
    }

    /*
     * Add the given amount to the attack modifier
     * @param: amount - the amount to add to the modifier.
     * @author: Tanner Skomar
     */
    public void modifyAttack(int amount)
    {
        attack_modifer += amount;
    }

    /* Getter for defense_modifer
     * @return: int representing defense_modifer
     * @author: Tanner Skomar
     */
    public int getDefenseMod()
    {
        return defense_modifer;
    }

    /*
    * Add the given amount to the defense modifier
    * @param: amount - the amount to add to the modifier.
    * @author: Tanner Skomar
    */
    public void modifyDefense(int amount)
    {
        defense_modifer += amount;
    }

    /*
     * Setter for defense_matrix_active
     * @param: is_active - bool true if dense matrix should be set on
     * @param: new_target_ref - a reference to the target of the damage
     * @author Tanner Skomar
     */
    public void modifyDefenseMatrix(bool is_active, CombatCharacters new_target_ref = null)
    {
        defense_matrix_active = is_active;
        rustbot_ref = new_target_ref;
        if (is_active)
        {
            showEffectFloatText("Matrix Enabled");
        }
        else
        {
            showEffectFloatText("Matrix Disabled");
        }
    }

    /*
     * Damages character given amount - defense_modifer
     * 
     * if rustbot has used his defense matrix effect and is still alive send damage to him instead
     * @param: amount - the amount of damage the character should take
     * @return: void
     * @author: Tanner Skomar
     */
    public void takeDamage(int amount)
    {
        if(defense_matrix_active)
        {
            if (rustbot_ref)
            {
                health.damage(0); //this character takes 0 damage
                rustbot_ref.takeDamage(amount);
                return;
            } else //rustbot is dead so deactivate defense Matrix
            {
                modifyDefenseMatrix(false);//Deactivate defense matrix
            }
        }
        

        //behave normally
        int new_damage = amount - defense_modifer;
        if (new_damage < 0) //prevents negative damage (accidental healing)
        {
            new_damage = 0;
        }
        health.damage(new_damage);
    }

    /*
     * Increases armour by given amount
     * @param: amount - the amount of armour to add
     * @return: void
     * @author: Tanner Skomar
     */
    public void increaseArmour(int amount)
    {
        health.addArmour(amount);
    }

    /*
     * Increases health by the given amount
     * @param: amount - the amount of healing to be applied to the character
     * @return: void
     * @author: Tanner Skomar
     */
    public void recoverHealth(int amount)
    {
        if(poison_modifier > 0) //if poisoned, don't heal
        {
            health.heal(0);
            return;
        }
        health.heal(amount);
    }

    public void modifyPoison(int amount)
    {
        poison_modifier += amount;
    }

    /*
     * Used to check if the character is still alive
     * @return: true if player is alive else false
     * @author: Tanner Skomar
     */
    public bool isAlive()
    {
        return health.getCurrentHealth() > 0;
    }

    /*
     * Used to remove Active effects on target
     * @param: type_to_remove 
     *              - BUFF if buffs should be removed
     *              - DEBUFF if debuffs should be removed
     *              - NONE if both types should be removed
     */
    public void removeActiveEffects(Active_Effect_Type type_to_remove)
    {
        switch (type_to_remove)
        {
            case Active_Effect_Type.BUFF://Remove Buffs
                showEffectFloatText("Blighted");
                foreach (Effect e in getActiveEffects())
                {
                    if (e.getActiveType() == Active_Effect_Type.BUFF)
                    {
                        e.removeActiveEffect(e);
                    }
                }
                break;
            case Active_Effect_Type.DEBUFF://Remove Debuffs
                showEffectFloatText("Cleansed");
                foreach (Effect e in getActiveEffects())
                {
                    if(e.getActiveType() == Active_Effect_Type.DEBUFF)
                    {
                        e.removeActiveEffect(e);
                    }
                }
                break;
            case Active_Effect_Type.NONE://Not specified so remove all
            default:
                showEffectFloatText("Status Cleared");
                foreach (Effect e in getActiveEffects())
                {
                    e.removeActiveEffect(e);
                }
                break;
        }
    }

    /*
    * Get the list of active effects on the character
    * @author: Tanner Skomar
    */
    public Effect[] getActiveEffects()
    {
        Effect[] active_effects = GetComponents<Effect>();

        return active_effects;
    }

    /*
     * Placeholder Function to take a turn
     * Should be overrid
     * @author: Tanner Skomar
     */
    public virtual void takeTurn()
    {
        turnDescription += name +"'s turn description: ";
        active = true;
        StartTurnEvent.Raise(this);
        Effect[] start_turn_effects = getActiveEffects().Where(x => !x.isEndTurnEffect).ToArray();
        foreach (Effect e in start_turn_effects)
        {
            //Debug.Log(e);
            e.setTargets(new CombatCharacters[] { this }); //For testing while applying effects by hand the target should already have been set when applied through code.
            e.activateContinuous();
        }
        swarm_manager.activateContinuous(); //Activate continuous effects of swarm
    }

    /*
     * Should be called at the end of a combat characters turn
     * Will go through and activate any continuous effects on the character
     */
    public virtual void endTurn()
    {
        // Local Handling of end of turn. Apply effects. Show turn log. Deactivate character
        Effect[] endturn_effects = getActiveEffects().Where(x=>x.isEndTurnEffect).ToArray();
        foreach (Effect e in endturn_effects)
        {
            //Debug.Log(e);
            e.setTargets(new CombatCharacters[] {this}); //For testing while applying effects by hand the target should already have been set when applied through code.
            e.activateContinuous();
        }



        turnDescription += ", End " + name + "'s Turn";
        //Debug.Log(turnDescription);
        turnDescription = "";
        active = false;

        // Tell turn mechanic that the turn is over
        TurnMechanic.Instance.HandleEndOfTurn();
    }

    public void DeathThrows()
    {
        SpriteRenderer[] spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
        foreach (var s in spriteRenderers)
        {
            // TODO Not working with burn away properly. Have to add one to every sprite renderer in rig
            var burn = s.gameObject.AddComponent<BurnAway>();
            burn.OverrideDestroy((GameObject x) => {});
        }
        Invoke("Die", 2.1f);
    }
    private void Die()
    {
        Destroy(gameObject);
    }

    bool deathLatch = false;
    protected virtual void Update()
    {
        if (!deathLatch && !isAlive())
        {
            CombatController.Instance.HandleDeath(this);
            
            DeathThrows();

            active = false;
            deathLatch = true;
        }
    }


    protected virtual void Start()
    {
        health = GetComponent<Health>(); //Get the health component
        health.setCurrentArmour(starting_armour);
        speed_modifer = 0;

        // find the status effect roots and move the display to correct screen point
        status_effect_char_root = transform.Find("StatusEffect Root");
        Transform status_effect_display_roots = transform.Find("StatusEffect Canvas").GetChild(0);
        status_effect_display_root1 = status_effect_display_roots.GetChild(0);
        status_effect_display_root2 = status_effect_display_roots.GetChild(1);

        Vector3 root_pos = Camera.main.WorldToScreenPoint(status_effect_char_root.position);
        status_effect_display_roots.position = root_pos;
        status_effect_display_count = 0;
    }

    /*
     * Instantiates the prefab status effect and assigns the arguments to it's relevant values
     * @param effect_icon: icon associated with the applied effect
     * @param effect_strength: strength of the applied effect
     * @returns: gameobject of the UI display prefab for later reference
     * @author: Travis Baldwin
     */
    public GameObject displayStatus(Sprite effect_icon, int effect_strength)//@Tanner Skomar modifed when duration and amount were unified
    {
        Transform effect_display_row;
        if (status_effect_display_count >= 4)
        {
            effect_display_row = status_effect_display_root2;
        }
        else
        {
            effect_display_row = status_effect_display_root1;
        }

        GameObject status_effect = Instantiate(status_effect_display_prefab,effect_display_row);
        status_effect.transform.GetChild(0).GetComponent<Image>().sprite = effect_icon;
        status_effect.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = effect_strength.ToString();

        // assigns values for the script to display when hovered over
        StatusEffectTooltip tooltip = status_effect.GetComponent<StatusEffectTooltip>();
        tooltip.setEffectStrength(effect_strength);

        // parameterize effect_description from the effect script
        //tooltip.setEffectDescription(effect_description);

        status_effect_display_count += 1;

        return status_effect;
    }

    /*
     * Updates the turns_remaining fields on the gameobject's TMPro and in it's script
     * @param status_effect_object: reference to the gameobject of the instantiated display
     * @param turns_remaining: turns remaining on the applied effect
     * @author: Travis Baldwin
     */
    public void updateStatusDisplayTurn(GameObject status_effect_object, int turns_remaining)//@Tanner Skomar modifed when duration and amount were unified
    {
        status_effect_object.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = turns_remaining.ToString();
        status_effect_object.GetComponent<StatusEffectTooltip>().setTurnsRemaining(turns_remaining);
    }

    /*
     * Removes the gameobject of the instantiated display.  Called from effect script when destroys itself.
     * @param status_effect_object: reference to the gameobject of the instantiated display
     * @author: Travis Baldwin
     */
    public void removeStatusDisplay(GameObject status_effect_object)
    {
        Destroy(status_effect_object);
        status_effect_display_count -= 1;

        if (status_effect_display_count >= 4)
        {
            if (status_effect_object.transform.parent.gameObject.name == status_effect_display_root1.gameObject.name)
            {
                status_effect_display_root2.GetChild(0).SetParent(status_effect_display_root1);
            }
        }
    }



    /*
     * Spawns floating effect text
     * @param: text - string to show in text
     * @author: Tanner Skomar
     */
    public void showEffectFloatText(string new_text)
    {
        if (FloatingTextEffectPrefab && new_text != "")
        {
            showFloatingText(new_text, FloatingTextEffectPrefab);
        }
    }


    /* Spawns floating text with given prefab
     * @param: text - string to show in text
     * @param: text_prefab - floating text prefab type to spawn text as
     * @author: Tanner Skomar
     */
    protected void showFloatingText(string new_text, GameObject text_prefab)
    {
        var float_text = Instantiate(text_prefab, new Vector4(transform.position.x, transform.position.y, -3), Quaternion.identity, gameObject.transform);
        var text_mesh_comp = float_text.transform.GetChild(0).GetComponent<TextMeshPro>();
        text_mesh_comp.text = new_text;
    }

    /* Spawns visual effect with given prefab
     * @param: text_prefab - floating text prefab type to spawn text as
     * @author: Tanner Skomar
     */
    public void showVisualEffect(GameObject visual_effect_prefab)
    {
        Instantiate(visual_effect_prefab, new Vector4(transform.position.x, transform.position.y+1, -3), Quaternion.identity, gameObject.transform);
    }
}
