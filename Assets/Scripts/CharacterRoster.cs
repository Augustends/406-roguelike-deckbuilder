using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CharacterSO/CharacterRoster")]
public class CharacterRoster : ScriptableObject
{
    [SerializeField] List<CharacterData> Roster;
    public CharacterData GetCharacter(string name)
    {
        foreach (var character in Roster)
        {
            if (character.name.Equals(name))
            {
                return character;
            }
        }
        throw new System.ArgumentException("Character not in roster: " + name);
    }
}
