using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// A reusable singlton pattern. Simply have a class extend this and BAM, its a singleton
/// Author: Duncan Boyes
/// </summary>
/// <typeparam name="T"></typeparam>
public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    private static T instance;


    /// <summary>
    /// Public accessor for the single instance
    /// </summary>
    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new GameObject(typeof(T).Name).AddComponent<T>();
                Debug.Log("Had to create Singleton of: " + typeof(T).Name);
            }
            return instance;
        }
    }


    /// <summary>
    /// Useful to know if a singleton exists
    /// </summary>
    public static bool IsIntialized
    {
        get { return instance == null; }
    }


    protected virtual void Awake()
    {
        if (instance!=null)
        {
            Debug.LogError("[Singleton] Trying to instantiate a second instance of a singleton class." + gameObject.name);
            Destroy(gameObject);
        }
        else
        {
            instance = (T)this;
        }
    }


    /// <summary>
    /// Frees up the static instance upon the object being destroyed
    /// </summary>
    protected virtual void OnDestroy()
    {
        if (instance == this)
        {
            instance = null;
        }
    }
}

