using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToogleOnOff : MonoBehaviour
{
    SpriteRenderer sr;


    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }
    public void Toogle()
    {
        if (sr.enabled)
        {
            sr.enabled = false;
        }
        else
        {
            sr.enabled = true;
        }
    }

    
}
