using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
/**
 * Script used for testing the UI
 */
public class UITestScript : MonoBehaviour
{
    [SerializeField]
    GameObject testCard;
    [SerializeField]
    Canvas canvas;

    [SerializeField]
    GameObject deck;
    [SerializeField]
    GameObject hand;
    [SerializeField]
    GameObject discard;


    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 10; i++)
        {
            GameObject newCard = Instantiate(testCard, this.transform.GetChild(1));
            newCard.transform.GetChild(1).GetChild(0).GetComponent<Image>().color = new Color(Random.Range(0,1f), Random.Range(0,1f), Random.Range(0,1f));
            newCard.GetComponent<DragNDrop>().SetCanvas(canvas);
            newCard.transform.position = this.transform.GetChild(1).position;
        }
        

    }

    // Update is called once per frame
    /*void Update()
    {
        
    }*/

    /**
     * Attempts to draw a card, if deck is empty, puts cards from discard into deck, then attempt to draw a card
     */
    public void DrawCard()
    {
        if (deck.transform.childCount > 0)
        {
            GameObject tempCardDeck = deck.transform.GetChild(0).gameObject;
            //tempCardDeck.transform.SetParent(hand.transform);
            StartCoroutine(Lerp(tempCardDeck.transform, hand.transform));
            tempCardDeck.GetComponent<Card>().InHand();
        } else
        {
            foreach (Transform child in discard.transform.Cast<Transform>().ToList()) // Have to cast to list or you will miss half the gameobjects
            {
                child.transform.SetParent(deck.transform);
                child.position = deck.transform.position;
                child.GetComponent<Card>().InDeck();

            }
            if (deck.transform.childCount > 0)
            {
                GameObject tempCardDeck = deck.transform.GetChild(0).gameObject;
                tempCardDeck.transform.SetParent(hand.transform);
                tempCardDeck.GetComponent<Card>().InHand();
            }
            
        }
    }

    /**
     * Discard the first gameobject in your hand
     */
    public void DiscardCard()
    {
        if (hand.transform.childCount > 0)
        {
            GameObject tempCardHand = hand.transform.GetChild(0).gameObject;
            //tempCardHand.transform.SetParent(discard.transform);
            //tempCardHand.transform.position = discard.transform.position;
            tempCardHand.GetComponent<Card>().InDiscard();
            StartCoroutine(Lerp(tempCardHand.transform, discard.transform));
        }
    }

    /**
     * Simulate ending a turn by discarding all cards in hand
     */
    public void EndTurn()
    {
        foreach (Transform child in hand.transform.Cast<Transform>().ToList())
        {
            //child.transform.SetParent(discard.transform);
            //child.position = discard.transform.position;
            child.GetComponent<Card>().InDiscard();
            StartCoroutine(Lerp(child.transform, discard.transform));
        }
    }

    /**
     * Called Lerp but actually uses Smoothdamp as it more accurately achieves the goal of moving cards
     */
    IEnumerator Lerp(Transform transformToLerp, Transform end)
    {
        float timeElapsed = 0;
        Vector3 velocity = Vector3.zero;
        float smoothTime = 0.7f;

        while (timeElapsed < 0.7f)
        {
            transformToLerp.position = Vector3.SmoothDamp(transformToLerp.position, end.position, ref velocity, smoothTime);
            timeElapsed += Time.deltaTime;
            smoothTime -= Time.deltaTime;
            yield return null;
            //Debug.Log(velocity.ToString());
        }
        transformToLerp.SetParent(end);
        transformToLerp.position = end.position;
    }
}
