using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="SceneStates/Overworld")]
public class OverworldStateData : ScriptableObject
{
    [SerializeField] public VertexData current_vertex_id;
    [SerializeField] public string level_name;
    [SerializeField] public GraphData zoneLayout;

    public void Clear()
    {
        current_vertex_id = null;
        level_name = "fresh";
        zoneLayout = null;
    }
}
