using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CampController : MonoBehaviour
{
    [SerializeField] PlayerData playerData;
    [SerializeField] GameObject[] campSupplies = new GameObject[4];
    [SerializeField] Vector3[] partyPositions = new Vector3[3];
    [SerializeField] GameObject[] party = new GameObject[3];
    [SerializeField] int restHealth = 20;
    [SerializeField] GameObject bgDay;
    [SerializeField] GameObject bgNight;
    [SerializeField] GameObject shadows;
    [SerializeField] GameObject sparks;
    [SerializeField] GameObject fire;
    [SerializeField] GameObject[] buttons = new GameObject[2];

    private void Start()
    {
        MakeCamp();
        PlaceParty();
    }

    private void MakeCamp()
    {
        //Camp layout is determined by # of party members
       int partySize = playerData.party_members.Count;
       if(partySize > 1)
        {
            campSupplies[0].SetActive(true);
            campSupplies[1].SetActive(true);
        }
       if(partySize > 2)
        {
            campSupplies[2].SetActive(true);
            campSupplies[3].SetActive(true);
        }
    }

    private void PlaceParty()
    {
        for(int i = 0; i < playerData.party_members.Count; i++)
        {
            party[i] = GameObject.Instantiate(playerData.party_members[i].prefab);
            party[i].GetComponent<PlayerCharacter>().health.setMaxHealth(party[i].GetComponent<PlayerCharacter>().CharacterData.max_health);
            party[i].GetComponent<PlayerCharacter>().health.setCurrentHealth(party[i].GetComponent<PlayerCharacter>().CharacterData.cur_health);
            party[i].transform.position = partyPositions[i];
            if(i == 2)
            {
                Vector3 scale = party[i].transform.Find(playerData.party_members[i].character_name + "Rigged").transform.localScale;
                party[i].transform.Find(playerData.party_members[i].character_name + "Rigged").transform.localScale = new Vector3(scale.x * -1f, scale.y * 1f, 1f);
            }
            //party member's animator should have a trigger called "Rest" that puts them into a rest animation
            party[i].transform.Find(playerData.party_members[i].character_name + "Rigged").GetComponent<Animator>().SetTrigger("Rest");
            party[i].transform.Find("HealthBar Canvas").transform.Find("Armor").gameObject.SetActive(true);
        }
    }

    public void Rest()
    {
        //removes the rest button
        Destroy(buttons[0]);
        Destroy(buttons[1]);
        for (int i = 0; i < playerData.party_members.Count; i++)
        {
            party[i].GetComponent<PlayerCharacter>().health.heal(20);
            party[i].GetComponent<PlayerCharacter>().CharacterData.cur_health = party[i].GetComponent<PlayerCharacter>().health.getCurrentHealth();
        }
        //changes from night to day
        bgNight.SetActive(false);
        bgDay.SetActive(true);
        shadows.SetActive(false);
        sparks.SetActive(false);
        fire.SetActive(false);

    }
    public void ChangeParty()
    {
        GameController.Instance.UnloadLevel("Camp");
        GameController.Instance.LoadLevel("CharacterSelect");
    }
}
