using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fishing : MonoBehaviour
{
    [SerializeField] 
    private PlayerData playerData;
    [SerializeField]
    private CharacterData fisher;
    [SerializeField]
    private Transform canvas;
    [SerializeField]
    private GameObject biteIcon;
    [SerializeField]
    private GameObject encounterFish;
    [SerializeField]
    private GameObject trash;
    //temporary text telling you that you are now fishing
    [SerializeField]
    private GameObject nowFishingText;

    enum FISH_STATE { NEUTRAL, FISHING, BITE, CAUGHT, MISS}
    FISH_STATE state = FISH_STATE.NEUTRAL;
    private void Awake()
    {
        /*
        fisher = playerData.party_members[0];
        GameObject fisherInstance = Instantiate(fisher.prefab);
        fisherInstance.transform.position = new Vector3(-5, -3, 0);
        fisherInstance.transform.localScale = new Vector3(2, 2, 1);
        */
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            Debug.Log(state);
            switch (state)
            {
                case FISH_STATE.NEUTRAL:
                    CastRod();
                    nowFishingText.SetActive(true);
                    break;
                case FISH_STATE.FISHING:
                    ReelIn();
                    break;
                case FISH_STATE.BITE:
                    StartCoroutine(Caught());
                    break;
                case FISH_STATE.MISS:
                    break;
                case FISH_STATE.CAUGHT:
                    break;  
            }

        }
    }
    //Start Fishing
    private void CastRod()
    {
        state = FISH_STATE.FISHING;
        Debug.Log("Rod is cast");
        StartCoroutine(WaitForFish());
    }
    //wait for a random amount of time for a fish to bite
    IEnumerator WaitForFish()
    {
        yield return new WaitForSeconds(3f);
        //maxLength is in place to prevent the fish from taking an insane amount of time to bite
        yield return new WaitForSeconds(Random.Range(0, 5));
        Bite();
        yield return new WaitForSeconds(0.75f);
        //missed the catch
        if(state == FISH_STATE.BITE)
        {
            biteIcon.SetActive(false);
            state = FISH_STATE.MISS;
        }

    }

    //Reel in Rod, change state based on if a fish was on the line
    private void ReelIn()
    {
        if(state == FISH_STATE.BITE)
        {
            StartCoroutine(Caught());
        }
        else
        {
            state = FISH_STATE.MISS;
        }
    }
    //State if a fish is on the line
    private void Bite()
    {
        state = FISH_STATE.BITE;
        Debug.Log(state);
        biteIcon.SetActive(true);
    }
    //For when a fish is on the line
    IEnumerator Caught()
    {
        biteIcon.SetActive(false);
        state = FISH_STATE.CAUGHT;
        Debug.Log("Catch!");
        int caughtFish = Random.Range(0, 10);
        Debug.Log(caughtFish);
        if(caughtFish == 1)
        {
            StartCoroutine(TrashCatch());
        }
        else if(caughtFish < 5)
        {
            StartCoroutine(EncounterCatch());
        }
        else
        {
            StartCoroutine(CardCatch());
        }

      yield return new WaitForSeconds(1f);
    }
    IEnumerator TrashCatch()
    {
        Debug.Log("Caught garbage");
        trash.SetActive(true);
        for (float i = 0.1f; i < 100; i++)
        {
            trash.GetComponent<RectTransform>().localPosition = Vector3.Lerp(new Vector3(200, -500, 0), new Vector3(0, 125, 0), i / 100);
            trash.GetComponent<RectTransform>().localScale = Vector3.Lerp(new Vector3(0, 0, 0), new Vector3(1, 1, 1), i / 100);
            yield return null;
        }
    }
    //Function called when an encounter is caught
    IEnumerator EncounterCatch()
    {
        encounterFish.SetActive(true);
        encounterFish.GetComponent<RectTransform>().localPosition = new Vector3(200, -500, 0);
        for (float i = 0.1f; i < 100; i++)
        {
            encounterFish.GetComponent<RectTransform>().localPosition = Vector3.Lerp(new Vector3(200, -500, 0), new Vector3(0, 125, 0), i / 100);
            yield return null;
        }
        GameController.Instance.UnloadLevel("Fish");
        GameController.Instance.LoadLevel("CombatSceneDuncan");
    }
    /* Retrieves a given amount of cards from a character's full cardlist 
    * @character: The character whose card list you want to draw from
    * @return: an card retrieved from the full card list from the given character 
    * @author: Alex Shepherd
     */
    IEnumerator CardCatch()
    {
        Card fish = fisher.full_card_list[Random.Range(0, fisher.full_card_list.Count)];
        Card cardInstance = GameObject.Instantiate(fish, canvas);
        for (float i = 0.1f; i < 100; i++)
        {
            cardInstance.GetComponent<RectTransform>().localPosition = Vector3.Lerp(new Vector3(200, -500, 0), new Vector3(0, 125, 0), i / 100);
            cardInstance.GetComponent<RectTransform>().localScale = Vector3.Lerp(new Vector3(0, 0, 0), new Vector3(1, 1, 1), i / 100);
            yield return null;
        }
        fisher.AddCard(fish);
    }
    //TODO
    IEnumerator MoneyCatch()
    {
        yield return null;
    }
}
