using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISavable
{
    void SaveState();
    void LoadState();
}
