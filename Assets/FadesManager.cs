using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadesManager : Singleton<FadesManager>
{


    [SerializeField] Animation animation;
    [SerializeField] AnimationClip fadeInAnim;
    [SerializeField] AnimationClip fadeOutAnim;
    public FadeCompleteEvent onFadeEvent;
    public delegate void FadeCompleteCallback();
    FadeCompleteCallback completeCallback;


    public void OnFadeInComplete()
    {
        Debug.Log("Fade in Complete");
        onFadeEvent.Raise(Events.FadeType.IN);
        UIManager.Instance.SetDummyCameraActive(true);
    }

    public void OnFadeOutComplete()
    {
        //Debug.Log("Fade out Complete");
        onFadeEvent.Raise(Events.FadeType.OUT);
        completeCallback();
    }

    public void FadeIn()
    {
        animation.Stop();
        animation.clip = fadeInAnim;
        animation.Play();
    }

    public void FadeOutThen(FadeCompleteCallback callback)
    {
        UIManager.Instance.SetDummyCameraActive(false);
        animation.Stop();
        animation.clip = fadeOutAnim;
        animation.Play();
        completeCallback = callback;
    }

    protected void Awake()
    {
        base.Awake();
    }
    protected void OnDestroy()
    {
        base.OnDestroy();
    }
}
