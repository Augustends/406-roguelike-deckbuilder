using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// Responsible for choosing the enounter
/// </summary>
public class CombatFactory : Singleton<CombatFactory>
{
    protected override void Awake()
    {
        base.Awake();
        normalEncounters = Resources.LoadAll("encounters_data",typeof(EncounterDataSO)).Cast<EncounterDataSO>().ToArray();
        eliteEncounters = Resources.LoadAll("elite_encounters_data",typeof(EncounterDataSO)).Cast<EncounterDataSO>().ToArray();
    }

    [SerializeField] EncounterDataSO[] normalEncounters;
    [SerializeField] EncounterDataSO[] eliteEncounters;

    // Level thresholds to encounter CRs
    Dictionary<int, int> LevelToNormalDifficultyCR = new Dictionary<int, int>() { { 1, 100 }, { 2, 200 }, { 3, 300 }, { 4, 500 },{5, 700 } };
    Dictionary<int, int> LevelToEliteDifficultyCR = new Dictionary<int, int>() { { 1, 200 }, { 2, 400 }, { 3, 600 }, { 4, 1000 } , { 5, 1400 } };



    public void ChooseCombatEncounter(GameObject vertexGO)
    {
        // Chose a combat that matches this vertex
        Vertex V = vertexGO.GetComponent<Vertex>();
        if (V.type == EncounterType.ELITE)//Elite type
        {
            int lowCR = V.level - 5;
            int highCR = V.level + 5;
            var validCREncounters = eliteEncounters.Where(x => x.CR < highCR && x.CR > lowCR).ToList();
            //take random from validEncounters;
            if (validCREncounters.Count == 0) Debug.LogError("Could not find an encounter with the CR within the needed range: " + lowCR + " and " + highCR + " at vertex: " + vertexGO.name);
            var enc = validCREncounters[Random.Range(0, validCREncounters.Count())];
            if (enc == null) { V.encounterData = eliteEncounters[0]; }
            else { V.encounterData = enc; }
        }
        else
        {
            int lowCR = V.level-3;
            int highCR = V.level+3;
            var validCREncounters = normalEncounters.Where(x => x.CR < highCR && x.CR > lowCR).ToList();
            //take random from validEncounters;
            if (validCREncounters.Count == 0) Debug.LogError("Could not find an encounter with the CR within the needed range: " + lowCR + " and " + highCR + " at vertex: " + vertexGO.name);
            var enc = validCREncounters[Random.Range(0,validCREncounters.Count())];
            if (enc == null) { V.encounterData = normalEncounters[0]; }
            else { V.encounterData = enc; }
        }

    }
}





