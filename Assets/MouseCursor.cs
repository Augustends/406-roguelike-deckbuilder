using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseCursor : MonoBehaviour
{
    [SerializeField]Sprite Idle;
    [SerializeField]Sprite Grab;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Cursor.visible == true)
        {
            Cursor.visible = false;
        }

        Vector2 cursorPos = Input.mousePosition;
        transform.position = cursorPos;


        GetComponent<Image>().sprite = Idle;
        if (Input.GetMouseButton(0))
        {
            GetComponent<Image>().sprite = Grab;
        }
    }
}
