using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class EventSystemSanityCheck : MonoBehaviour
{
    private void Awake()
    {
       var es = GameObject.FindObjectsOfType<EventSystem>();
        if (es.Length>1)
        {
            Destroy(this.gameObject);
        }
    }
}
