using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ChooseCardFromDeckDisplay : DeckDisplay
{
    public Card currentCardSelected;
    public bool confirmedSelection = false;
    [SerializeField] GameObject cardButton;
    private void Awake()
    {
        base.Awake();
        RefreshDisplay();
        this.enabled = true;

        Card[] cards = transform.GetComponentsInChildren<Card>();
        foreach (var card in cards)
        {
            GameObject button = Instantiate(cardButton, card.transform);
            button.GetComponent<Button>().onClick.AddListener(delegate { CardSelected(); });
        }
        DisplayDeckB();

    }

    public void Confirm()
    {
        if (currentCardSelected!=null)
        {
            confirmedSelection = true;
        }
    }

    private void CardSelected()
    {
        if (currentCardSelected != null)
        {
            currentCardSelected.UnHighlightCard();
        }
        
        currentCardSelected = EventSystem.current.currentSelectedGameObject.GetComponentInParent<Card>();
        currentCardSelected.HighlightCard();
    }

    // Update is called once per frame
    void Update()
    {
    }
}
